open Plebeia.Internal
open Test_utils
open Info

let check info =
  match decode (encode info) with
  | None -> assert false
  | Some info' -> assert (Info.equal info info')

let test () =
  check { tribune= None; copies= [] };
  check { tribune= None; copies= [ [ from_Some @@ Segment.of_string "L" ]] };
  check { tribune= None; copies= [ [ from_Some @@ Segment.of_string "LRLRLR" ]] };
  check { tribune= None; copies= [ [ from_Some @@ Segment.of_string "LRLRLR"
                                   ; from_Some @@ Segment.of_string "RLRLRL" ]] };
  with_random @@ fun st ->
  let copies =
    let open Gen in
    (list (int_range (0,10))
     @@ list (int_range (1, 10))
     @@ segment @@ int_range (1, Segment.max_length)) st
  in
  let tribune =
    let open Gen in
    choice [return None; (index >>| fun x -> Some x)] st
  in
  check { copies ; tribune }

let () =
  let open Alcotest in
  run "info_encoding"
    ["info_enconding", ["test", `Quick, test]]
