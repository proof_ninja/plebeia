(* node storage test *)
open Plebeia.Internal
open Test_utils
    
module RS = Random.State

let test () =
  with_random @@ fun st ->
    for _ = 1 to 10000 do
      test_with_context 1000000 @@ fun c ->
      Storage.Chunk.test_write_read st c.Context.storage
    done

let () =
  let open Alcotest in
  run "chunk"
    ["chunk", ["test", `Quick, test]]
