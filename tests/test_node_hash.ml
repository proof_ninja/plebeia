open Plebeia.Internal
open Test_utils

module RS = Random.State

let test () =
  with_random @@ fun st ->
  test_with_context 1000000 @@ fun ctxt ->
  for _ = 0 to 100 do
    let n = Gen.bud 10 st in
    let _, nh = Node_hash.compute ctxt n in
    let _, nh' =
      Node_hash.compute' (fun n ->
          match n with
          | Node.Disk (index, wit) -> `View (Node.load_node ctxt index wit)
          | View v -> `View v) n
    in
    assert (nh = nh');
  done

let () =
  let open Alcotest in
  run "node_hash"
    ["node_hash", ["test", `Quick, test]]
