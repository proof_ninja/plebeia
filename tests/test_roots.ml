open Plebeia.Internal
open Commit
open Test_utils

module RS = Random.State

let test () =
  with_random @@ fun st ->
  test_with_context 100000 @@ fun c ->

  (* Create context and roots files *)
  let storage_context = c.Context.storage in
  let storage_roots = Storage.create (Storage.filename storage_context ^ ".roots") in

  (* Create the roots *)
  let t = create ~storage_context ~storage_roots in

  let rec loop acc = function
    | 0 -> ()
    | n ->
        (* insert a leaf then commit *)
        let v = Value.of_string @@ string_of_int n in
        let node = Node.new_leaf v in
        let _node, i, hp = Node_storage.commit_node c node in
        (* half of them are genesis *)
        let parent = match acc with
          | [] -> None
          | _ ->
              if RS.int st 2 = 0 then None
              else Some (Rand.one_of st (Array.of_list acc))
        in
        ignore (Commit.add t ~parent ~info:None (Commit.Hash.of_plebeia_hash_prefix hp) i);
        loop (i::acc) (n-1)
  in
  loop [] 1000;

  prerr_endline "Added 1000 roots";

  (* reload *)
  let t' = create ~storage_context ~storage_roots in
  assert (List.sort compare @@ Commit.to_list t
          = List.sort compare @@ Commit.to_list t');

  prerr_endline "Rebuild test with empty roots file...";

  (* create an empty roots *)
  let storage_roots' = Storage.create (Storage.filename storage_context ^ ".roots'") in
  let t'' = (* hope storage_roots' are filled by recovered roots from storage_context *)
    create
      ~storage_context
      ~storage_roots:storage_roots'
  in
  assert (Commit.to_list t = Commit.to_list t'')

let () =
  let open Alcotest in
  run "roots"
    ["roots", ["test", `Quick, test]]
