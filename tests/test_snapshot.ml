open Plebeia.Internal
open Test_utils

module RS = Random.State

let test () =
  with_random @@ fun st ->
  test_with_context 1000000 @@ fun ctxt ->
  let fn = Filename.temp_file "plebeia" ".dump" in
  Format.eprintf "written to %s@." fn;

  for _ = 0 to 100 do
    let n = Gen.bud 20 st in
    let v, nh = Node_hash.compute ctxt n in
    let n = Node.View v in
    save_node_to_dot "node.dot" n;
    let fd = Unix.openfile fn [O_CREAT; O_TRUNC; O_WRONLY] 0o644 in
    Snapshot.save fd ctxt n;
    Unix.close fd;

    let fd = Unix.openfile fn [O_RDONLY] 0o644 in
    let reader = Data_encoding_tools.make_reader fd in
    let n', disk_nhs = Snapshot.load reader in
    Unix.close fd;
    assert (disk_nhs = []);
    assert (match n' with
        | Node.View v -> Node_hash.of_view v = Some nh
        | _ -> assert false);
  done;

  let test n =
    let st = RS.make [|n|] in
    let n = Gen.bud 5 st in
    let v, nh = Node_hash.compute ctxt n in
    let n = Node.View v in
    let n, _, _ = Node_storage.commit_node ctxt n in
    let n = forget_random_nodes st 0.2 n in
    save_node_to_dot "node.dot" n;
    let fd = Unix.openfile fn [O_CREAT; O_TRUNC; O_WRONLY] 0o644 in
    Snapshot.save_keeping_disk_nodes fd ctxt n;
    Unix.close fd;

    let fd = Unix.openfile fn [O_RDONLY] 0o644 in
    let reader = Data_encoding_tools.make_reader fd in
    let n', disk_nhs = Snapshot.load reader in
    Unix.close fd;
    assert (match n' with
        | Node.View v -> Node_hash.of_view v = Some nh
        | Disk (i, _) -> List.assoc i disk_nhs = nh);
  in

  for _ = 0 to 10000 do
    let seed = RS.int st 1_000_000 in
    try test seed with e ->
      Format.eprintf "error at seed %d@." seed;
      raise e
  done

let () =
  let open Alcotest in
  run "snapshot"
    ["snapshot", ["test", `Quick, test]]
