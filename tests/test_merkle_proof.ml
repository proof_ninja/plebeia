open Plebeia.Internal
open Result
open Test_utils
open Cursor

module MP = Merkle_proof

module RS = Random.State

let root_hash cursor =
  let Cursor (_, node, context, _) = from_Ok @@ Cursor.go_top cursor in
  Node_hash.compute context node
  |> snd

let show_hash lh =
  Hash.prefix lh |> Hash.Prefix.to_hex_string

let show_path path =
  String.concat " / " @@ List.map Segment.to_string path


let check_found_or_not_found keys expected_nodes nodes =
  if List.length keys = List.length expected_nodes &&
       List.length keys = List.length nodes then
    let list = List.combine keys (List.combine expected_nodes nodes) in
    let check (key, (expected, node)) =
      match expected, node with
      | None, None -> ()
      | Some expected, Some node ->
         let node_hash node =
           try
             let (_, nh) = Node_hash.compute_with_disk_node_hashes [] node in
             nh
           with
           | _ ->
              failwith ("node_hash cannot computed: " ^ string_of_node node 2)
         in
         let nh1 = node_hash expected in
         let nh2 = node_hash node in
         if nh1 = nh2 then ()
         else
           let _ =
             Printf.printf "hash is not equals: (%s <> %s) '%s'\n"
               (Hash.to_hex_string nh1)
               (Hash.to_hex_string nh2)
               (show_path key);
             Printf.printf "--- expected:\n%s\n--- actual:\n%s\n"
               (Debug.string_of_node expected 2)
               (Debug.string_of_node node 2);
           in
           failwith ("hash is not equals: " ^ show_path key)
      | Some _, None ->
         failwith ("The indicated node of path by proof should be found, but not found: " ^ show_path key)
      | None, Some _ ->
         failwith ("The indicated node of path by proof should be not found, but found: " ^ show_path key)
    in
    List.iter check list
  else
    failwith "length are not equals"


let check_soundness keys cursor expected_nodes =
  match
    MP.generate cursor keys >>= fun mproof ->
    MP.validate keys mproof >>= fun (hash, node_opts) ->
    Ok (hash, node_opts)
  with
  | Ok (hash, node_opts) ->
     check_found_or_not_found keys expected_nodes node_opts;
     let expected = root_hash cursor in
     if expected <> hash then begin
         let Cursor (_, node, _, _) = cursor in
         (Format.sprintf "!!! Merkle Proof Test Failed !!!\n Expected hash: %s\n but Exact: %s\nnode:\n%s" (show_hash expected) (show_hash hash) (Debug.string_of_node node 2))
         |> prerr_endline;
         assert false
       end
  | Error err ->
     let show_cur (Cursor (_, node, _, _)) = Debug.string_of_node node 2 in
     Printf.printf "Merkleproof error: \n%s\n(%s)" (show_cur cursor) (Error.show err);
     Error.raise err

let check_encoding keys cursor =
  let open Data_encoding in
  match MP.generate cursor keys with
  | Ok mproof ->
     let bson = Bson.construct MP.encoding mproof in
     let mproof' = Bson.destruct MP.encoding bson in
     if mproof' <> mproof then
       Format.printf "Merkle Proof Encoding Test Failed.\nThe Bson\n"
  | Error err ->
     failwith (Error.show err)

let check segs cursor expected_node_opt =
  check_soundness [segs] cursor [expected_node_opt];
  check_encoding [segs] cursor

let segment s = from_Some @@ Segment.of_string s

let commit cur =
  let Cursor (trail, node, ctx, info) = cur in
  let (node', _, _) = Node_storage.commit_node ctx node in
  _Cursor (trail, node', ctx, info)

let test_encoding ctx =
  let open Data_encoding in
  let expected =
    `A [
      `String "budsome";
      `O [("segment", `String "20")];
      `O [("value", `String "484f4745")];
    ]
  in
  let leaf = Node.new_leaf (Value.of_string "HOGE") in
  let seg = segment "LL" in
  let ext = Node.new_extender seg leaf in
  let node = Node.new_bud (Some ext) in
  let cursor = _Cursor (_Top, node, ctx, Info.empty) |> commit in
  let keys = [[seg]] in
  match MP.generate cursor keys with
  | Ok mproof ->
     let json = Data_encoding.Json.construct MP.encoding mproof in
  if expected <> json then begin
      Format.printf "== expected: %a\n" Json.pp expected;
      Format.printf "== actual: %a\n" Json.pp json;
      failwith "json encoding error"
    end
  | Error e ->
     Error.raise e


let test1 ctx =
  let leaf = Node.new_leaf (Value.of_string "HOGE") in
  let seg = segment "LL" in
  let ext = Node.new_extender seg leaf in
  let node = Node.new_bud (Some ext) in
  let cursor = _Cursor (_Top, node, ctx, Info.empty) |> commit in
  check [seg] cursor (Some leaf);
  check [] cursor (Some node);
  (* Non existence proofs *)
  check [segment "LRLRLR"] cursor None;
  check [seg; seg] cursor None;
  ()

let test2 ctx =
  let left = Node.new_leaf (Value.of_string "LEFT") in
  let right = Node.new_leaf (Value.of_string "RIGHT") in
  let internal = Node.new_internal left right in
  let prefix = segment "LL" in
  let ext = Node.new_extender prefix internal in
  let node = Node.new_bud (Some ext) in
  let cursor = _Cursor (_Top, node, ctx, Info.empty) |> commit in
  let seg = Segment.append prefix (segment "R") in
  check [seg] cursor (Some right);
  check [] cursor (Some node);
  (* Non existence proofs *)
  check [prefix] cursor None;
  check [segment "LLRR"] cursor None;
  ()

let test3 ctx =
  let seg1 = segment "RRRRR" in
  let seg1' = segment "RRRLR" in
  let seg2 = segment "LLLLL" in
  let seg3 = segment "LLRL" in
  let value1 = Value.of_string "HOGE1" in
  let value2 = Value.of_string "HOGE2" in
  let cursor =
    Cursor.empty ctx
    |> (fun c -> Cursor.create_subtree c seg1)   (* mkdir "RRRRR" *)
    >>= (fun c -> Cursor.create_subtree c seg1') (* mkdir "RRRLR" *)
    >>= (fun c -> Cursor.subtree c seg1)         (* cd "RRRRR" *)
    >>= (fun c -> Cursor.insert c seg2 value1)
    >>= (fun c -> Cursor.insert c seg3 value2)
    >>= Cursor.go_top
    |> from_Ok
    |> commit
  in
  check [seg1; seg2] cursor (Some (Node.new_leaf value1));
  check [seg1; seg3] cursor (Some (Node.new_leaf value2));
  let expected_bud =
    Node.(new_bud (Some
                     (new_extender (segment"LL")
                        (new_internal
                           (new_extender (segment"LL") (new_leaf value1))
                           (new_extender (segment"L")  (new_leaf value2))))))
  in
  check [seg1] cursor (Some expected_bud);
  (* Non existence proofs *)
  check [seg1; seg2; segment "RRR"] cursor None; (* file not a directory *)
  ()

let test4 ctx =
  let rec create store node depth =
    if depth = 0 then
      (List.rev store, node)
    else
      let seg = segment "RR" in
      let (node, _, _) =
        Node.new_extender seg node
        |> fun node -> Node.new_bud (Some node)
        |> Node_storage.commit_node ctx
      in
      create (seg :: store) node (depth - 1)
  in
  let value = Value.of_string "HOGE" in
  let (path, node) = create [] (Node.new_leaf value) 10000 in
  let cursor = _Cursor(Cursor._Top, node, ctx, Info.empty) in
  check path cursor (Some (Node.new_leaf value));
  (* Non existence proofs *)
  check [segment "RRR"] cursor None;
  ()

let get_node (Cursor (_, node, _, _)) = node

let go_down cur seg =
  Cursor.access_gen cur seg >>= function
  | Cursor.Reached (c, (Node.Bud _)) -> return c
  | Reached (c, (Node.Leaf _)) -> return c
  | res -> Cursor.error_access res

let find path cur =
  Result.fold_leftM (fun cur seg ->
      go_down cur seg
      |> Result.map_error (fun e ->
             Printf.printf "[Go Down ERROR]: '%s' '%s'\n%s\n" (Segment.to_string seg)
               (Error.show e)
               (Debug.string_of_node (get_node cur) 2);
             e)
    ) cur path
  |> Result.map_error (fun e ->
         Printf.printf "[FIND ERROR]: %s\n%s\n" (show_path path) (Debug.string_of_node (get_node cur) 2);
         e)

let random_check st =
  let len = 10000 in
  test_with_context len (fun context ->
      let max_depth = 5 in
      let node = Gen.bud max_depth st in
      let cursor = _Cursor(_Top, node, context, Info.empty) |> commit in
      let pathes = leaves context node in
      if pathes <> [] then
        let path = Gen.(one_of @@ return pathes) st in
        let leaf = find path cursor |> Result.from_Ok |> get_node in
        check path cursor (Some leaf))

let test () =
  let len = 10000 in
  test_with_context len (fun ctx ->
      test_encoding ctx;
      test1 ctx;
      test2 ctx;
      test3 ctx;
      test4 ctx
    );
  with_random @@ fun st ->
  for _ = 0 to 100 do
    random_check st
  done


let () =
  let open Alcotest in
  run "merkle_proof"
    ["merkle_proof", ["test", `Quick, test]]
