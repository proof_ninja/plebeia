open Plebeia.Internal
open Test_utils

module RS = Random.State

let test () =
  with_random @@ fun st ->

  (* XXX prefix.context, prefix.lock, prefix.roots are not assured not existing *)
  let prefix = Filename.temp_file "plebeia" "" in

  let vc = Vc.create prefix in

  let rec loop parent = function
    | 0 -> ()
    | n ->
        let parent =
          if Gen.bool st then parent
          else
            (* Change the parent to make dummy parents *)
          if Gen.bool st then None (* force genesis *)
          else Some (Gen.hash_prefix st)
        in
        let parent = Option.map Commit.Hash.of_plebeia_hash_prefix parent in

        let node = Gen.bud 5 st in
        let c = Cursor.(_Cursor (_Top, node, Vc.context vc, Info.empty)) in
        let c, _hp, _commit =
          Vc.commit
            ~override:true
            ~allow_missing_parent:true
            vc ~parent ~hash_override: None c in
        let _, nh = Cursor_hash.compute c in
        loop (Some (Hash.prefix nh)) (n-1)
  in
  loop None 100;
  Vc.close vc;
  Format.eprintf "done : %s.*@." prefix

let () =
  let open Alcotest in
  run "roots_with_dummy"
    ["roots_with_dummy", ["test", `Quick, test]]
