(* node storage test *)
open Plebeia.Internal

let () =
  let open Alcotest in
  run "blake2b28"
       ["blake2b28", ["test", `Quick, Hash.Prefix.test]]
