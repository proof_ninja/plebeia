open Plebeia.Internal
(* open Result *)
open Test_utils
(* open Cursor *)
module RS = Random.State

(* No branching *)
let build_file st d i =
  with_vc (d ^/ "src") @@ fun vc ->
  let cur = Vc.empty_cursor vc in
  let rec loop parents (Cursor.Cursor (_, n, ctxt, _info) as _cur) = function
    | 0 -> ()
    | i ->
        let parent, parents =
          (* genesis at 1/100 *)
          if Gen.int 100 st = 0 then None, parents
          else
            match Gen.shuffle parents st with
            | p::ps ->
                begin match Gen.int 20 st with
                  | 0 | 1 -> Some p, p::ps (* branch at 1/10 *)
                  | 2 ->
                      (* kill branch at 1/20 *)
                      begin match ps with
                        | _::ps ->
                            Some p, ps
                        | [] -> Some p, []
                      end
                  | _ -> Some p, ps
                end
            | [] -> None, []
        in
        let n' = make_resemble st ctxt n in
        let cur' = Cursor.(_Cursor (_Top, n', ctxt, Info.empty)) in
        let cur', hp, _commit =
          Vc.commit
            ~override: true
            vc
            ~parent
            ~hash_override: None
            cur'
        in
        let parents = Commit.Hash.of_plebeia_hash_prefix hp :: parents in
        loop parents cur' (i-1)
  in
  loop [] cur i

let copy_file st d =
  let vc_src = Vc.open_ ~mode:Storage.Reader (d ^/ "src") in
  with_vc (d ^/ "dst") @@ fun vc_dst ->
  let commits = Commit.to_list @@ Vc.roots vc_src in

  (* intentinally remove away some (5%) commits *)
  let commits =
    List.filter (fun _ -> Gen.int 20 st <> 0) commits
  in

  match Copy.copy vc_src commits vc_dst with
  | Error e ->
      Format.eprintf "ERROR: %a@." Error.pp e;
      Error.raise e
  | Ok _ -> ()

let test () =
  with_random @@ fun st ->
  with_tempdir @@ fun d ->
  build_file st d 1000;
  prerr_endline "Now copying";
  copy_file st d

let () =
  let open Alcotest in
  run "copy_file"
    ["copy_file",
     ["test", `Quick, test]]
