Require Import Program Omega.
Require Import Base Hash.
Open Scope list_scope.

Notation hash_zero := (Prefix.zero, string_empty).

Definition string_concat (ss : list ocaml_string) : ocaml_string :=
  List.fold_right string_append string_empty ss.

Lemma string_concat_app : forall xs ys,
    string_concat (xs ++ ys) = string_concat xs ^ string_concat ys.
Proof.
  induction xs.
  - simpl. intros ys. now rewrite string_append_nil_l.
  - simpl. intros ys. rewrite IHxs. now rewrite string_append_assoc.
Qed.

Axiom make_h_injective : forall last_2bits1 n1 ss1 last_2bits2 n2 ss2,
    make_h last_2bits1 n1 ss1 = make_h last_2bits2 n2 ss2 ->
    (last_2bits1, n1, string_concat ss1) = (last_2bits2, n2, string_concat ss2).

Lemma make_h_injective_1 : forall last_2bits1 n1 s1 last_2bits2 n2 s2,
    make_h last_2bits1 n1 [s1] = make_h last_2bits2 n2 [s2] ->
    (last_2bits1, n1, s1) = (last_2bits2, n2, s2).
Proof.
  intros. injection (make_h_injective _ _ _ _ _ _ H). intros eq1 eq2 eq3.
  do 2 rewrite string_append_nil_r in eq1. now subst.
Qed.

Axiom merge_spec : forall h1 h2, h1 ^^ h2 = Hash.to_strings h1 ++ Hash.to_strings h2 ++ [string_make1 (char_of_nat (string_length (string_concat (Hash.to_strings h2))))].

Lemma string_append_make1_injective : forall xs1 xs2 c1 c2,
    xs1 ^ string_make1 c1 = xs2 ^ string_make1 c2 -> (xs1, c1) = (xs2, c2).
Proof.
  intros xs1 xs2 c1 c2 eq.
  cut (string_length (string_make1 c1) = string_length (string_make1 c2)).
  - intros eqlen. injection (string_append_injective_with_length _ _ _ _ eq eqlen). intros eqc eqxs.
    apply string_make1_injective in eqc. now subst.
  - now do 2 rewrite string_length_make1.
Qed.

Parameter prefix_to_string : Prefix.t -> ocaml_string.
Axiom length_prefix : forall prefix, string_length (prefix_to_string prefix) = 28%nat.
Axiom prefix_to_string_injective: forall p1 p2,
    prefix_to_string p1 = prefix_to_string p2 -> p1 = p2.

Axiom hash_to_strings_spec : forall prefix var_part,
    Hash.to_strings (prefix, var_part) = [prefix_to_string prefix; var_part].

Lemma string_concat_hash_to_strings_injective : forall h1 h2,
    string_concat (Hash.to_strings h1) = string_concat (Hash.to_strings h2) -> h1 = h2.
Proof.
  destruct h1 as [prefix1 varpart1], h2 as [prefix2 varpart2].
  do 2 rewrite hash_to_strings_spec. simpl. do 2 rewrite string_append_nil_r. intro eq.
  set (f_equal string_length eq) as eqlen.
  do 2 rewrite string_length_append, length_prefix in eqlen. apply plus_reg_l in eqlen.
  injection (string_append_injective_with_length _ _ _ _ eq eqlen). intros eq1 eq2.
  apply prefix_to_string_injective in eq2. now subst.
Qed.

Lemma make_h_to_strings_injective : forall last_2bits1 n1 t1 last_2bits2 n2 t2,
    make_h last_2bits1 n1 (to_strings t1) = make_h last_2bits2 n2 (to_strings t2) -> (last_2bits1, n1, t1) = (last_2bits2, n2, t2).
Proof.
  intros. injection (make_h_injective _ _ _ _ _ _ H). intros eq1 eq2 eq3.
  apply string_concat_hash_to_strings_injective in eq1. now subst.
Qed.


Lemma concat_merge_injective : forall l1 r1 l2 r2,
    string_concat (l1 ^^ r1) = string_concat (l2 ^^ r2) -> (l1, r1) = (l2, r2).
Proof.
  intros l1 r1 l2 r2. do 2 (rewrite merge_spec; do 2 rewrite string_concat_app; rewrite <- string_append_assoc).
  simpl. do 2 rewrite string_append_nil_r. intros eq.
  injection (string_append_make1_injective _ _ _ _ eq). intros eqlen eq'.
  apply char_of_nat_injective in eqlen.
  injection (string_append_injective_with_length _ _ _ _ eq' eqlen).
  intros eqr eql.
  rewrite (string_concat_hash_to_strings_injective _ _ eqr).
  rewrite (string_concat_hash_to_strings_injective _ _ eql).
  reflexivity.
Qed.

Lemma make_h_merge_injective : forall last_2bits1 n1 l1 r1 last_2bits2 n2 l2 r2,
    make_h last_2bits1 n1 (l1 ^^ r1) = make_h last_2bits2 n2 (l2 ^^ r2) ->
    (last_2bits1, n1, l1, r1) = (last_2bits2, n2, l2, r2).
Proof.
  intros. injection (make_h_injective _ _ _ _ _ _ H). intros eq1 eq2 eq3.
  injection (concat_merge_injective _ _ _ _ eq1). intros eqr eql. now subst.
Qed.

Axiom make_h_not_eq_zero : forall bits n ss, make_h bits n ss <> Prefix.zero.

(** * injectives *)

Lemma of_bud_injective : forall x y, of_bud x = of_bud y -> x = y.
Proof.
  destruct x, y; simpl.
  - injection 1. intros eq. injection (make_h_to_strings_injective _ _ _ _ _ _ eq). intro eq1.
    now rewrite eq1.
  - injection 1. intro eq.
    now edestruct (make_h_not_eq_zero (Some 3) 2 (to_strings t)).
  - injection 1. intro eq.
    now edestruct (make_h_not_eq_zero (Some 3) 2 (to_strings t)).
  - reflexivity.
Qed.

Lemma of_leaf_injective : forall x y, of_leaf x = of_leaf y -> x = y.
Proof.
  intros x y. injection 1. intros eq.
  injection (make_h_injective_1 _ _ _ _ _ _ eq).
  now apply Value.to_string_inj.
Qed.

Lemma of_internal_injective : forall l1 r1 l2 r2,
    of_internal l1 r1 = of_internal l2 r2 -> (l1, r1) = (l2, r2).
Proof.
  unfold of_internal. intros l1 r1 l2 r2. injection 1. intros eq.
  injection (make_h_merge_injective _ _ _ _ _ _ _ _ eq). intros. now subst.
Qed.

Lemma of_extender_injective : forall seg1 seg2 h1 h2,
    snd h1 = snd h2 ->
    of_extender seg1 h1 = of_extender seg2 h2 -> (seg1, h1) = (seg2, h2).
Proof.
  unfold of_extender. intros seg1 seg2 h1 h2 eq_snd. injection 1. intros eq_seg eq_fst.
  rewrite <- (Segment.encode_injective _ _ eq_seg).
  destruct h1, h2. simpl in eq_snd, eq_fst. now subst o0 t0.
Qed.

(** * independences *)

Lemma not_eq_of_bud_Some_None : forall h1, of_bud (Some h1) <> of_bud None.
Proof.
  unfold of_bud. intros h. injection 1.
  now apply make_h_not_eq_zero.
Qed.

Lemma not_eq_of_bud_of_leaf : forall opt v, of_bud opt <> of_leaf v.
  unfold of_bud, of_leaf. destruct opt as [h|].
  - intros v. injection 1. intros eq.
    now injection (make_h_injective _ _ _ _ _ _ eq).
  - intros v. injection 1.
    intros eq. apply eq_sym in eq. now apply (make_h_not_eq_zero _ _ _ eq).
Qed.

Lemma not_eq_of_bud_of_internal : forall opt l r, of_bud opt <> of_internal l r.
Proof.
  unfold of_bud, of_internal. destruct opt as [h|].
  - injection 1. intros eq. injection (make_h_injective _ _ _ _ _ _ eq).
    intros. apply eq_sym in H1. now auto.
  - intros l r. injection 1. apply not_eq_sym.
    now apply make_h_not_eq_zero.
Qed.

Lemma not_eq_of_bud_of_extender : forall opt seg h, of_bud opt <> of_extender seg h.
Proof.
  unfold of_bud, of_extender. destruct opt as[h|].
  - intros seg h'. intro eq. apply (f_equal snd) in eq. simpl in eq.
    now apply (Segment.encode_non_empty seg).
  - intros seg h'. intro eq. apply (f_equal snd) in eq. simpl in eq.
    now apply (Segment.encode_non_empty seg).
Qed.

Lemma not_eq_of_leaf_of_internal : forall v l r, of_leaf v <> of_internal l r.
Proof.
  unfold of_leaf, of_internal. intros v l r. injection 1.
  intros eq. now injection (make_h_injective _ _ _ _ _ _ eq).
Qed.

Lemma not_eq_of_leaf_of_extender : forall v seg h, of_leaf v <> of_extender seg h.
Proof.
  unfold of_leaf, of_extender. intros v seg h eq. apply (f_equal snd) in eq. simpl in eq.
  now apply (Segment.encode_non_empty seg).
Qed.

Lemma not_eq_of_internal_of_extender : forall l r seg h, of_internal l r <> of_extender seg h.
  (* Use: (set_last_ (h ...)) <> _ ++ seg *)
(*  now edestruct not_eq_set_last_2bits_h_append; [inversion Inv2| exact hash_eq].*)
Proof.
  unfold of_internal, of_extender. intros l r seg h eq.
  apply (f_equal snd) in eq. simpl in eq.
  now apply (Segment.encode_non_empty seg).
Qed.
