(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)
(** { 1 Node hash } *)

val compute' :
  (Node.t -> [< `Hash of Hash.t | `View of Node.view ])
  -> Node.t
  -> Node.t * Hash.t
(** Compute the node hash of the given node.

    The function argument for short cutting, especially for [Disk _].  It can do either

      * Obtain the node hash of the node somehow from an external source
      * Retrieve the view of the node to let [compute'] calculate the node hash of it

    Tail recursive.
*)

val compute :  Context.t -> Node.node -> Node.view * Hash.t
(** Compute the node hash of the given node.  It may traverse the node
    loading subnodes from the disk to obtain or compute the node hashes of
    subnodes if necessary.

    Note: Index is reset to Not_Indexed!
*)

val compute_with_disk_node_hashes : (Index.t * Hash.t) list -> Node.t -> Node.t * Hash.t
(** Node hashes of [Disk (index,_)] are obtained from the association
    list of [Index.t] and [t].  If [index] is not found in the alist,
    it fails.
*)

val of_view : Node.view -> Hash.t option
(** Returns the node hash of view, if available *)

val of_node : Node.node -> (Node.view * Hash.t option) option
(** Returns the node hash of view, if available *)
