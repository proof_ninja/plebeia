(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

type 'a t = Random.State.t -> 'a
include Monad.S1 with type 'a t := 'a t

module RS = Random.State

val int : int -> int t
(** [0..n-1] *)

val int_range : (int * int) -> int t
(** Inclusive *)

val char : char t
val bool : bool t

val string : int t -> char t -> string t
val list : int t -> 'a t -> 'a list t

val one_of : 'a list t -> 'a t
val choice : 'a t list -> 'a t
val shuffle : 'a list -> 'a list t

val value : Value.t t
(** Random value up to 64 bytes *)

val segment : int t -> Segment.t t
(** [segment n] is a generator of segments upto length [n] *)

val segment_with_exact_length : int t -> Segment.t t
(** [segment_with_exact_length n] is a generator of segments with length [n] *)

val hash_prefix : Hash.Prefix.t t

val index : Index.t t

(** { 2 Node generation capped by segment depth *)

(** [leaf] is a generator of leaves whose values are upto 64 bytes *)
val leaf : Node.node t

val bud_none : Node.node t
val bud : int -> Node.node t
val internal : int -> Node.node t
val extender : int -> Node.node t
