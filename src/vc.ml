(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Utils
open Node
open Cursor

type t =
  { roots : Commit.t
  ; context : Context.t
  }

let roots { roots ; _ } = roots
let context { context ; _ } = context

let empty_cursor t = Cursor.empty t.context

let check_prefix s =
  if s = "" then failwith "Path prefix cannot be empty"
  else if s.[String.length s - 1] = '/' then failwith "Path prefix cannot be a directory"
  else ()

let create' ?context_pos ?context_length ?hashcons ?node_cache ?(roots_file=true) ?(lock=true) path_prefix =
  check_prefix path_prefix;
  if lock then ignore (Lock.lock @@ path_prefix ^ ".lock");
  let context =
    Context.create ?pos:context_pos ?length:context_length
      ?hashcons ?node_cache
      (path_prefix ^ ".context")
  in
  let storage_roots =
    if roots_file then Some (Storage.create @@ path_prefix ^ ".roots")
    else None
  in
  let roots =
    Commit.create'
      ~storage_context: context.Context.storage
      ~storage_roots
  in
  { roots ; context }

let create ?context_pos ?context_length ?hashcons ?node_cache path_prefix =
  create' ?context_pos ?context_length ?hashcons ?node_cache ~roots_file:true path_prefix

let open_ ~mode ?context_pos ?hashcons ?node_cache ?(roots_file=true) path_prefix =
  check_prefix path_prefix;
  if mode = Storage.Writer then ignore (Lock.lock @@ path_prefix ^ ".lock");
  let context =
    Context.open_ ?pos:context_pos ~mode
      ?hashcons ?node_cache
      (path_prefix ^ ".context")
  in
  let storage_roots =
    if roots_file then
      let fn = path_prefix ^ ".roots" in
      if Sys.file_exists fn then Some (Storage.open_ ~mode fn)
      else
        if mode = Storage.Writer then Some (Storage.create fn)
        else None
    else None
  in
  let roots =
    Commit.create'
      ~storage_context: context.Context.storage
      ~storage_roots
  in
  { roots ; context }

let close { context ; roots } =
  Context.close context;
  Commit.close roots (* Roots share the same storage with Context,
                       double close is prevented by Storage.close *)

let commit
    ?(allow_missing_parent=false)
    ?(override=false)
    { roots ; context ; _ }
    ~parent
    ~hash_override
    c =
  (* make sure contexts are the same *)
  let () =
    let Cursor (_, _, context', _) = c in
    assert (context == context'); (* XXX should not use pointer equality *)
  in

  let (c, i, hp) = Cursor_storage.commit_top_cursor c in
  let hash = match hash_override with
    | None -> Commit.Hash.of_plebeia_hash_prefix hp
    | Some hash -> hash
  in
  Log.notice "Vc.commit hash:%a  plebeia_hash:%s  parent:%a"
    Commit.Hash.pp hash
    (Hash.Prefix.to_hex_string hp)
    (Format.option Commit.Hash.pp) parent;

  begin match Commit.find roots hash with
    | Some _i' ->
        if override then
          Log.notice "Vc: Overriding hash %a" Commit.Hash.pp hash
        else
          failwithf "hash collision %s" (Hash.Prefix.to_hex_string hp)
    | None -> ()
  end;

  let parent = match parent with
    | None -> None
    | Some h ->
        match Commit.find roots h with
        | None when not allow_missing_parent ->
            failwithf "No such parent: %a@" Commit.Hash.pp h
        | None ->
            (* We need a dummy root entry for this parent *)
            (* XXX Wait what happens it is hashconsed ? *)
            Some (snd @@ Commit.add_dummy roots h)
        | Some { Commit.index= i ; _ } -> Some i
  in
  (* Before the commit itself, we record the info *)
  let Cursor.Cursor (_,_,_,info) = c in
  if List.length info.Info.copies <> 0 then
    Log.notice "Vc: COPIES=%d" (List.length info.Info.copies);
  let iinfo =
    if Info.empty = info then None
    else
      Some (Info.write context.Context.storage info)
  in

  let commit, i = Commit.add roots ~parent hash i ~info:iinfo in
  assert (match iinfo with
      | Some iinfo -> Index.(iinfo + Unsafe.of_int 2 = i)
      | None -> true);

  Storage.commit context.Context.storage; (* XXX Now this flushes the mmap modifications.  This is not always necessary *)
  (c, hp, commit)

let sync t =
  Log.notice "Vc: sync'ing storage and roots";
  let (), secs = with_time (fun () -> Commit.sync t.roots) in
  Log.notice "Vc: reloaded storage and roots in %.1f secs" secs

let load_info context i_info =
  Info.read context.Context.storage i_info

let checkout' ?(keep_info=false)  ({ roots ; context ; _ } as t) hash =
  (* XXX We need to get the index of commit to load the info *)
  match Commit.find roots hash with
  | Some ent when Commit.Entry.is_dummy ent ->
      None (* Dummy. Tree is not in the storage *)
  | Some ({ Commit.index; info; _ } as root) ->
      let info = match info with
        | Some i -> Result.from_Ok @@ load_info context i
        | None -> Info.empty
      in
      Some (root,
            _Cursor (_Top,
                     Disk (index, Not_Extender),
                     context,
                     if keep_info then info else Info.empty))
  | None when Context.mode context = Writer ->
      (* Writer knows everything.  hash really does not exist. *)
      None
  | None ->
      (* XXX if it is a reader, it may not know the new roots written by
         the writer.  The reader should update the roots and retry
         the checkout *)
      sync t;
      (* XXX dupe *)
      match Commit.find roots hash with
      | Some ent when Commit.Entry.is_dummy ent ->
          None (* Dummy. Tree is not in the storage *)
      | Some ({ Commit.index ; info ; _ } as root) ->
          let info = match info with
            | None -> Info.empty
            | Some info -> Result.from_Ok @@ load_info context info
          in
          Some (root,
                _Cursor (_Top,
                         Disk (index, Not_Extender),
                         context,
                         if keep_info then info else Info.empty))
      | None -> None (* Really not found *)

let checkout ?keep_info roots hash =
  let open Option.Op in
  checkout' ?keep_info roots hash >>| snd
