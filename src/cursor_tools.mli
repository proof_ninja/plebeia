val fold :
  init:'a ->
  Segment.Segs.t ->
  Cursor.t ->
  ('a ->
   Segment.Segs.t ->
   Cursor.t ->
   ([< `Continue | `Exit | `Up ] * 'a, Error.t) result) ->
  ('a * Cursor.t, Error.t) result

val ls :
  Cursor.t ->
  ((Segment.segment * Node.view) list * Cursor.t,
   Error.t)
  result

val ls_rec :
  Cursor.t ->
  ((Segment.segment list * Node.node) list *
   Cursor.t, Error.t)
  result

module GenTraverse : sig

  type ('acc, 'data) visitor
  
  type dir = [ `Bud | `Extender | `Side of Segment.side ]

  val prepare :
    'acc ->
    'data ->
    Cursor.t ->
    ('acc ->
     'data ->
     Cursor.t ->
     (Cursor.t * ('acc * ('data * dir) list option), Error.t) result) ->
    ('acc, 'data) visitor

  val step :
    ('acc, 'data) visitor ->
    ([ `Continue of ('acc, 'data) visitor
     | `Finished of Cursor.t * 'acc ], Error.t)
    result

  val fold :
    'acc ->
    'data ->
    Cursor.t ->
    ('acc ->
     'data ->
     Cursor.t ->
     (Cursor.t * ('acc * ('data * dir) list option),
      Error.t)
     result) ->
    (Cursor.t * 'acc, Error.t) result

  val ls :
    Cursor.t ->
    (Cursor.t * (Segment.t * Node.t) list,
     Error.t)
    result

  val ls_rec :
    Cursor.t ->
    (Cursor.t *
     (Segment.t list * Node.t) list, Error.t)
    result
end
