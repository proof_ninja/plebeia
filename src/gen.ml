(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

include Monad.Make1(struct
    type 'a t = Random.State.t -> 'a
    let bind (gen: 'a t) (f: 'a -> 'b t) st = f (gen st) st
    let return x = fun _st -> x
  end)

module RS = Random.State

let int sz : int t = fun st ->
  RS.int st sz

let int_range (min,max) : int t = fun st ->
  RS.int st (max - min + 1) + min

let string int char : string t = fun st ->
  let l = int st in
  String.init l (fun _ -> char st)

let list int n : 'a list t = fun st ->
  let l = int st in
  List.init l (fun _ -> n st)

let char : char t = int 256 >>| Char.chr

let bool : bool t = RS.bool

let one_of : 'a list t -> 'a t = fun ast ->
  ast >>= fun xs ->
  assert (xs <> []);
  int (List.length xs) >>| fun i ->
  List.nth xs i

let choice : 'a t list -> 'a t = fun xs ->
  assert (xs <> []);
  int (List.length xs) >>= fun i ->
  List.nth xs i

let shuffle xs : 'a list t = fun st ->
  let a = Array.of_list xs in
  let size = Array.length a in
  for i = 0 to size - 2 do
    let pos = Random.State.int st (size - i - 1) + i in
    let x = Array.unsafe_get a pos in
    Array.unsafe_set a pos @@ Array.unsafe_get a i;
    Array.unsafe_set a i x
  done;
  Array.to_list a

let value : Value.t t =
  string (int 64) char >>| Value.of_string

let hash_prefix : Hash.Prefix.t t =
  string (return 28) char >>| Hash.Prefix.of_string

let index : Index.t t =
  let open Stdint in
  fun st ->
    Index.of_uint32
    @@ Uint32.of_int64
    @@ RS.int64 st
    @@ Int64.(of_uint32 Uint32.max_int - 256L + 1L)

let segment int : Segment.t t =
  let int st = max 1 (int st) in (* length > 0 *)
  list int (bool >>| fun b -> if b then Segment.Left else Segment.Right) >>|
  Segment.of_sides

let segment_with_exact_length int : Segment.t t =
  list int (bool >>| fun b -> if b then Segment.Left else Segment.Right) >>|
  Segment.of_sides

(* Tricky.  In one Bud level, we cannot have more than [Segment.max_length]
   level of segments, otherwise removing nodes from it might create an Extender
   with very long segment over this limit.
*)
let rec leaf = value >>| Node.new_leaf

and bud_none = return (Node.new_bud None)

and bud depth =
  let depth0 = depth in
  if depth <= 1 then return (Node.new_bud None)
  else
    int depth >>= fun n ->
    if n = 0 then return (Node.new_bud None)
    else begin
      let depth = depth - 1 in
      choice [extender depth; internal depth] >>= fun n ->
      begin match n with
        | Node.View (Node.Leaf _) -> bud depth0
        | Node.View (Node.Bud _) -> bud depth0
        | _ -> return (Node.new_bud (Some n))
      end
    end

and internal depth =
  let f =
    if depth <= 1 then
      (* we cannot create an internal*)
      choice [bud_none; leaf]
    else
      let depth = depth - 1 in
      int depth >>= fun n ->
      if n = 0 then choice [bud_none; leaf]
      else choice [internal depth;
                   extender depth;
                   bud depth]
  in
  f >>= fun n1 ->
  f >>| fun n2 ->
  let n = Node.new_internal n1 n2 in
  match n with
  | View (Internal _) -> n
  | _ -> assert false

and extender depth =
  if depth <= 1 then
    (* we cannot create an extender *)
    choice [bud_none; leaf]
  else
    int depth >>= fun n ->
    if n = 0 then choice [bud_none; leaf]
    else
      let limit = max 1 (min Segment.max_length depth) in
      segment (int_range (1, limit)) >>= fun seg ->

      let depth = depth - Segment.length seg in
      choice [internal depth; bud depth] >>| fun n ->
      Node.new_extender seg n
