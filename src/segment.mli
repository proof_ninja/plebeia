(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Arthur Breitman <arthur.breitman+nospam@tezos.com>     *)
(* Copyright (c) 2019 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** A module encapsulating the concept of a path through the Patricia tree.
  A path is a sequence of n full segments. The n-1 first segments end in
  a bud and the nth ends in a leaf. Internal segments are bit of paths
  encoded in the internal nodes of the Patricia tree while tip segments
  represent the bits of path encoded close to the leaf. *)

(** { 2 Side } *)

type side = Left | Right
(** Binary tree, by convention when a bool or a bit is used, 0 = false = Left
  and 1 = true = Right *)

val string_of_side : side -> string
(** For human readability.  "L" or "R" *)

val string_of_sides : side list -> string
(** Human readable string representation of a side list, e.g. "LLLRLLRL" *)

(** { 2 Segment } *)

type segment
type t = segment

val normalize : t -> t
(** Normalize it using [Encoding] *)

val max_short_segment_length : int
(** Maximum length of sides which fits with one cell with an index: 215 *)

val max_length : int
(** Maximum length of sides for a segment: 1815 *)

val empty : t
(** The empty segment. *)

val is_empty : t -> bool

val cut : t -> (side * t) option
(** Cuts a path into a head and tail if not empty.  Also known as "snoc". *)

val take : int -> t -> (side list * t) option
(** [take n t] takes the first [n] sides of [t] and returns them with the rest *)

val cons : side -> t -> t

val equal : t -> t -> bool
(** Normal (=) does not work *)

val compare : t -> t -> int
(** Normal compare does not work *)

val common_prefix : t -> t -> t * t * t
(** Factors a common prefix between two segments. *)

val of_sides : side list -> t
(** Converts a list of side to a segment. *)

val to_sides : t -> side list

val to_string : t -> string
(** Human readable string representation of a segment, e.g. "LLLRLLRL" *)

val of_string : string -> t option
(** Parse the string representation of a segment, e.g. "LLRLLRL" *)

val string_of_segments : t list -> string
(** Human readable string representation of segments: "LLRLRLL/RRRLL/.."  *)

val pp : Format.formatter -> t -> unit

val pp_segments : Format.formatter -> t list -> unit

val length : t -> int

val append : t -> t -> t
val concat : t list -> t

(** { 3 Serialization } *)
module Serialization : sig

  val encode : t -> string
  (** <--- segment bits --->10{0,7}
      At most 7 zeros at tail.
  *)

  val decode : string -> t option
  (** <--- segment bits --->10{0,7}
      At most 7 zeros at tail.
  *)

  val decode_exn : string -> t
  (** <--- segment bits --->10{0,7}
      At most 7 zeros at tail.
  *)

  val decode_slice_exn : (string * int * int) -> t
  (** [decode_exn] for slice *)

  val decode_slice : (string * int * int) -> t option
  (** [decode] for slice *)

  val encode_list : t list -> string
  val decode_list : string -> t list option

  val decode_list_slice : (string * int) -> (t list * int) option
  (** slice version *)
end

val compare_list : t list -> t list -> int
(** comparison of t list *)

(** { 3 Data encoding } *)

val encoding : t Data_encoding.t
(** For encoding to JSON or binary using data-encoding *)

(** { 2 Segs: append friendly segment list } *)

module Segs : sig
  (** append friendly segment list *)

  type t

  val empty : t
  (** Empty segments XXX The name is confusing.  [empty] is [[Segment.empty]] *)

  val add_side : t -> side -> t
  (** Append a side to the last segment of [t] *)

  val append_seg : t -> segment -> t
  (** Append a segment to the last segment of [t] *)

  val append_sides : t -> side list -> t
  (** Append a list of [side]s to the last segment of [t] *)

  val append_rev_sides : t -> side list -> t
  (** Reverse-append a list of [side]s to the last segment of [t] *)

  val push_bud : t -> t
  (** Finalize the current last segment of [t]
      then append an empty semgent at the last of it *)

  val finalize : t -> segment list
  (** Get the segment list representation of [t] *)

  val to_string : t -> string
  (** String representation of [t] *)

  val of_segments : segment list -> t
  (** The last segment is appendable *)

  val last : t -> side list
  (** The last segment *)
end

module StringEnc : sig
  (** { 2 Encoding of binary strings to segment. Mainly for testing. }  *)
  val encode : string -> t
  (** Encode a binary string to a segment *)

  val decode : t -> string option
  (** Decode a segment to a binary string *)
end
