(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* Implementation of the root table.

   All the data should be in memory.
*)

open Utils

module C = Xcstruct

module Hash : sig
  type t

  val of_string : string -> t
  val of_plebeia_hash_prefix : Hash.Prefix.t -> t
  val to_string : t -> string
  val to_hex_string : t -> string
  val show_ref : (t -> string) ref
  val pp : Format.formatter -> t -> unit
end = struct

  type t = string

  let of_string s =
    assert (String.length s = 32);
    s

  let to_string x = x

  let of_plebeia_hash_prefix hp =
    let s = Hash.Prefix.to_string hp in
    s ^ "\000\000\000\000"

  let to_hex_string s =
    let `Hex h = Hex.of_string s in
    h

  let show_ref = ref to_hex_string

  let pp ppf h = Format.pp_print_string ppf @@ !show_ref h
end

module Entry : sig
  type t = private
    { parent : Index.t option
      (* Index of the parent root node in storage_context.

         [Some Index.zero] is illegal.
         [Some Index.one] if the entry is a dummy.
      *)
      (* XXX Not the index of the parent entry, but the index of the parent tree
         root. Since it is hard to find the parent entry index of storage *)

    ; index  : Index.t
      (* Index of the root node in storage_context.

         Index of the entry itself when it is a dummy.
      *)

    ; hash   : Hash.t      (* Context hash *)

    ; info : Index.t option
      (* Index of the info in storage_context. *)
    }

    val make : parent:Index.t option -> index:Index.t -> hash:Hash.t -> info:Index.t option -> t
    val is_dummy : t -> bool
    val is_genesis : t -> bool
    val compare : t -> t -> int
    val pp : Format.formatter -> t -> unit

    type tables =
      { by_hash    : (Hash.t, t) Hashtbl.t
      ; by_index   : (Index.t, t) Hashtbl.t
      ; children   : (Index.t, t list) Hashtbl.t
            (* This cannot be [(Hash.t, t list) Hashtbl.t]
               since children register themselves before their parents hashes
               are known *)
      }
    val empty_tables : unit -> tables
    val children : tables -> t -> t list
    type Error.t += Dummy_commit_cannot_have_parent_for_now
    val parent : tables -> t -> (t option, Error.t) result
    val to_list : tables -> t list
    val length : tables -> int
    val iter : (t -> unit) -> tables -> unit
    val fold : (t -> 'a -> 'a) -> tables -> 'a -> 'a
    val add : tables -> t -> unit

    val genesis : tables -> t list
    val dummies : tables -> t list

    module Store : sig
      val read : Storage.storage -> Index.t -> t * Index.t option
      val read_the_latest : Storage.storage -> t option

      val write_at : Storage.t -> at:Index.t -> prev:Index.t option -> t -> unit
      (** Write (or overwrite) [t] to the specified index [at] and its predecessor.
          [prev] is the index of the previous Entry.
      *)

      val write : Storage.storage -> t -> t * Index.t
    end

end = struct
  type t =
    { parent : Index.t option
    ; index  : Index.t
    ; hash   : Hash.t      (* Context hash *)
    ; info   : Index.t option
    }

  let make ~parent ~index ~hash ~info =
    assert (parent <> Some Index.zero);
    { parent; index; hash; info }

  let is_dummy ent = ent.parent = Some Index.one

  let is_genesis ent = ent.parent = None

  let compare e1 e2 = compare e1.index e2.index

  let pp ppf ({ index ; parent ; hash ; info } as ent) =
    let f fmt = Format.fprintf ppf fmt in
    f "%a %sat %a parent=%a info=%a"
      Hash.pp hash
      (if is_dummy ent then "DUMMY " else "")
      Index.pp index
      (fun ppf -> function
         | None -> f "none"
         | Some x -> Index.pp ppf x) parent
      (Format.option Index.pp) info

  (* All are in the memory.
     We avoid using Hash.t as possible to keep the size small as possible.
  *)
  type tables =
    { by_hash    : (Hash.t, t) Hashtbl.t
    ; by_index   : (Index.t, t) Hashtbl.t
    ; children   : (Index.t, t list) Hashtbl.t
          (* This cannot be [(Hash.t, t list) Hashtbl.t]
             since children register themselves before their parents hashes
             are known *)
    }

  let empty_tables () =
    { by_hash = Hashtbl.create 101
    ; by_index = Hashtbl.create 101
    ; children = Hashtbl.create 101
    }

  let children tables ent =
    match Hashtbl.find_opt tables.children ent.index with
    | None -> []
    | Some cs -> cs

  type Error.t += Dummy_commit_cannot_have_parent_for_now
  let () = Error.register_printer @@ function
    | Dummy_commit_cannot_have_parent_for_now -> Some "Dummy commit cannot have parent for now"
    | _ -> None

  let parent tables ent =
    if is_genesis ent then Ok None
    else if is_dummy ent then Error Dummy_commit_cannot_have_parent_for_now
    else
      match ent.parent with
      | None -> assert false (* checked by is_genesis *)
      | Some pi ->
          match Hashtbl.find_opt tables.by_index pi with
          | None -> assert false
          | Some ent -> Ok (Some ent)

  let list p t = (* XXX filter *)
    List.sort compare
    @@ Hashtbl.fold (fun _hash entry acc ->
        if p entry then entry::acc
        else acc) t.by_hash []

  let length roots = Hashtbl.length roots.by_hash

  let to_list = list (fun _ -> true)
  let genesis = list is_genesis
  let dummies = list is_dummy

  let iter f roots = List.iter f @@ to_list roots

  let fold f roots acc =
    List.fold_left (fun acc e -> f e acc) acc
    @@ to_list roots

  let add tables ent =
    (* collisions are overridden *)
    Hashtbl.replace tables.by_hash ent.hash ent;
    Hashtbl.replace tables.by_index ent.index ent;
    if is_dummy ent || is_genesis ent then ()
    else
      match ent.parent with
      | None -> ()
      | Some i when i = Index.one -> ()
      | Some pi ->
          let children =
            match Hashtbl.find_opt tables.children pi with
            | None -> []
            | Some cs -> cs
          in
          Hashtbl.replace tables.children pi (ent::children)

  module Store = struct
    (* commits

    |0        15|16      19|20      23|24        27|28      31|
    |<-  zero ->|<- info ->|<- prev ->|<- parent ->|<- idx  ->|

    Previous cell:
    |0                                                      31|
    |<-------------------- hash ----------------------------->|

    Hash value can be different from Plebeia's root Merkle hash

    if parent is 1, the entry is a dummy
    *)

    let read storage (i : Index.t) =
      let buf = Storage.get_cell storage i in
      let index  = C.get_index buf 28 in
      let info = Index.zero_then_none @@ C.get_index buf 16 in
      let parent = Index.zero_then_none @@ C.get_index buf 24 in
      let prev = Index.zero_then_none @@ C.get_index buf 20 in
      let j = Index.pred i in
      let buf = Storage.get_cell storage j in
      let hash = Hash.of_string @@ C.copy buf 0 32 in
      (* fix index when it is a dummy *)
      let index = if parent = Some Index.one then i else index in
      { index ; parent ; hash ; info }, prev

    let read_the_latest storage =
      match Storage.get_last_root_index storage with
      | None -> None
      | Some i -> Some (fst @@ read storage i)

    (* at : the latter cell *)
    let write_at storage ~at:i ~prev commit =

      let j = Index.pred i in
      (* debug *)
      assert (Option.default Index.zero prev < j);

      let buf = Storage.get_cell storage j in
      C.write_string (Hash.to_string commit.hash) buf 0 32;

      let buf = Storage.get_cell storage i in
      C.set_index buf 28 commit.index;
      C.set_index buf 24 (Option.default Index.zero commit.parent);
      C.set_index buf 20 (Option.default Index.zero prev);
      C.set_index buf 16 (Option.default Index.zero commit.info);
      (* reserved *)
      C.write_string (String.make 16 '\000') buf 0 16;

      (* debug *)
      let read_commit, read_prev = read storage i in
      if commit <> read_commit || prev <> read_prev then begin
        Format.eprintf "ERROR!!!!@.";
        Format.eprintf "commit: @[%a@] %a@." pp commit (Format.option Index.pp) prev;
        Format.eprintf "read  : @[%a@] %a@." pp read_commit (Format.option Index.pp) read_prev;
        assert false
      end

    let write storage commit =
      let with_last_root_index f =
        let prev = Storage.get_last_root_index storage in
        let a,i = f prev in
        Storage.set_last_root_index storage (Some i);
        a,i
      in
      with_last_root_index @@ fun prev ->
      let i = Storage.new_indices storage 2 in
      let i = Index.succ i in
      (* patch commit if it is a dummy *)
      let commit = if is_dummy commit then { commit with index= i } else commit in
      write_at storage ~at:i ~prev commit;
      commit, i
  end
end

type entry = Entry.t = private
  { parent : Index.t option
  ; index  : Index.t
  ; hash   : Hash.t
  ; info   : Index.t option
  }

(* storage *)

type t =
  { tables : Entry.tables
  ; storage_context : Storage.t
  ; storage_roots : Storage.t option (* separate file to store roots *)
  }

(* shared read_commits and read_additional_commits *)
let add' cntr tables entry =
  begin match Hashtbl.find_opt tables.Entry.by_hash entry.hash with
  | None -> Entry.add tables entry
  | Some entry' (* this is newer than entry *) ->
      (* We do not terminate the program but uses
         the newer commit which has been read already.
      *)
      Log.notice "Commit.read_commits: commits with the same hash: %a at indices %a and %a.  Use the one at %a"
        Hash.pp entry.hash
        Index.pp entry.index
        Index.pp entry'.index
        Index.pp entry'.index
  end;
  incr cntr;
  if !cntr mod 10000 = 0 then begin
    Log.notice "read %d new commits" !cntr;
  end

let read_commits storage =
  let tables = Entry.empty_tables () in
  let cntr = ref 0 in
  let rec aux = function
    | None -> ()
    | Some i ->
        let ent, prev = Entry.Store.read storage i in
        add' cntr tables ent;
        aux prev
  in
  aux (Storage.get_last_root_index storage);
  Log.notice "read %d commits (%d unique) from %s"
    !cntr
    (Entry.length tables)
    (Storage.filename storage);
  tables

type Error.t += Conflict
let () = Error.register_printer @@ function
  | Conflict -> Some "Conflict found in Plebeia commit table"
  | _ -> None

(* This is always read from storage_context *)
let read_additional_commits tables storage =
  let cntr = ref 0 in
  let rec aux = function
    | None -> Ok !cntr
    | Some i ->
        let entry, prev = Entry.Store.read storage i in
        let add_and_continue () =
          add' cntr tables entry;
          aux prev
        in
        (* We stop when we are sure that the entry is also in storage_root *)
        match Hashtbl.find_opt tables.Entry.by_index entry.index with
        | Some entry' ->
            if entry = entry' then Ok !cntr
            else begin
              (* Oops, conflict found *)
              Log.error "Conflict commit: storage: %a roots: %a"
                Entry.pp entry
                Entry.pp entry';
              Error Conflict
            end
        | None -> add_and_continue ()
  in
  match aux (Storage.get_last_root_index storage) with
  | Ok n ->
      Log.notice "read %d new commits from %s"
        !cntr
        (Storage.filename storage);
      Ok n
  | Error e -> Error e

let write_commit storage ~parent index ~hash ~info =
  let commit = Entry.make ~index ~parent ~hash ~info in
  let commit, i = Entry.Store.write storage commit in
  Storage.commit storage;
  commit, i

(* hash collisions are overridden *)
let add_gen t ~parent hash index ~info =
  let commit, i = write_commit t.storage_context ~parent index ~hash ~info in
  Entry.add t.tables commit;
  (* Small risk of program crash before it writes to the storage_roots *)
  begin match t.storage_roots with
    | None -> ()
    | Some s -> ignore (write_commit s ~parent index ~hash ~info)
  end;
  Log.notice "Added root %a" Entry.pp commit;
  commit, i

let add t ~parent hash index ~info = add_gen t ~parent hash index ~info

let add_dummy t hash =
  (* XXX needs comment for this Index.one *)
  add_gen t ~parent:(Some Index.one) hash Index.zero (* will be fixed *) ~info:None

let mem t = Hashtbl.mem t.tables.by_hash

let find t = Hashtbl.find_opt t.tables.by_hash

let find_by_index t = Hashtbl.find_opt t.tables.by_index

let create' ~storage_context ~storage_roots =
  (* Storage_roots:  a backup for fast loading.  If anything bad happens
     in it, we load everything from storage_context.

     Storage_context: the master data which is slow to load.
  *)
  let tables = match storage_roots with
    | None ->
        read_commits storage_context
    | Some storage_roots ->
        let tables, init_roots =
          match read_commits storage_roots with
          | exception _ ->
              Log.warning "Roots file has a problem. Reloading all the roots from context file";
              read_commits storage_context, true
          | tables ->
              (* Maybe the storage_roots is obsolete or conflicting. *)
              match read_additional_commits tables storage_context with
              | Error e ->
                  Log.warning "Roots file has a conflict: %a. Reloading all the roots from context file" Error.pp e;
                  read_commits storage_context, true
              | Ok 0 -> tables, false
              | Ok _ -> tables, true
        in
        if init_roots && Storage.mode storage_roots = Storage.Writer then begin
          Log.notice "Reinitializing roots file: %s" (Storage.filename storage_roots);
          let (), secs = with_time @@ fun () ->
            Storage.truncate storage_roots ;
            let f { index ; parent ; hash ; info } =
              ignore (write_commit storage_roots ~parent index ~hash ~info)
            in
            Entry.iter f tables
          in
          Log.notice "Reinitialized roots file with %d commits in %.1f secs: %s" (Entry.length tables) secs (Storage.filename storage_roots);
        end;
        tables
  in
  { tables
  ; storage_context
  ; storage_roots
  }

let close roots =
  Storage.close roots.storage_context;
  Option.iter Storage.close  roots.storage_roots

let create ~storage_context ~storage_roots =
  create' ~storage_context ~storage_roots:(Some storage_roots)

let read_additional_commits t =
  read_additional_commits t.tables t.storage_context

let sync t =
  Storage.sync t.storage_context;
  Option.iter Storage.sync t.storage_roots;
  match read_additional_commits t with
  | Ok n -> Log.notice "%d new commits found@." n
  | Error _ -> assert false (* XXX error, unrecoverable *)

let parent t = Entry.parent t.tables

let genesis t = Entry.genesis t.tables
let dummies t = Entry.dummies t.tables
let children t = Entry.children t.tables
let to_list t = Entry.to_list t.tables
let iter f roots = Entry.iter f roots.tables
let fold f roots acc = Entry.fold f roots.tables acc
let length roots = Entry.length roots.tables
let read_the_latest roots = Entry.Store.read_the_latest roots.storage_context

let decendants roots e =
  let rec f es frontiers = match frontiers with
    | [] -> List.sort Entry.compare es
    | fr::frontiers ->
        let ts = children roots fr in
        f (ts @ es) (ts @ frontiers)
  in
  f [] [e]

let ancestors roots ~while_ e =
  let rec f es e =
    if not @@ while_ e then Ok es
    else
      let es = e::es in
      match parent roots e with
      | Ok None -> Ok es
      | Ok (Some e) -> f es e
      | Error e -> Error e
  in
  f [] e
