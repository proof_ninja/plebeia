(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2020 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** Copy commits to one file to another *)

open Utils
open Result

let report_gc () =
  (* It takes 0.2 secs *)
  Stdlib.Gc.print_stat Stdlib.stderr

let copy vc1 cs vc2 =
  (* Since it loops without Lwt for long, we use non Lwt logger *)
  Log.(set default);
  let commits1 = Vc.roots vc1 in
  let commits2 = Vc.roots vc2 in
  Result.fold_leftM (fun (i, (psi,pdi), last_src, last_dst) c1 ->
      if i mod 100 = 0 then
        Format.eprintf "%d vc1: %a  vc2: %a@."
          i Context.pp_cache (Vc.context vc1) Context.pp_cache (Vc.context vc2);

      if i mod 1000 = 0 then begin
        (* XXX I guess not really required *)
        Format.eprintf "%d reopen vc1@." i;
        let (), sec = with_time @@ fun () -> Storage.reopen (Vc.context vc1).Context.storage in
        Format.eprintf "%d reopen vc1 in %.2f secs@." i sec;
        Format.eprintf "%d reopen vc2@." i;
        let (), sec = with_time @@ fun () -> Storage.reopen (Vc.context vc2).Context.storage in
        Format.eprintf "%d reopen vc2 in %.2f secs@." i sec;
      end;

      if i mod 1000 = 0 then begin
        (* This is VERY costy. *)
        Stdlib.Gc.compact ();
        let (), sec = with_time @@ report_gc in
        Format.eprintf "%d gc: %.2f@." i sec;
      end;

      if Commit.Entry.is_dummy c1 then begin
        ignore (Commit.add_dummy commits2 c1.hash);
        Ok (i + 1, (psi,pdi), last_src, last_dst)
      end else
        (* copy src with the info! *)
        let src = from_Some @@ Vc.checkout ~keep_info:true vc1 c1.Commit.hash in
        (match Commit.parent commits1 c1 with
         | Error e -> Error e
         | Ok None ->
             (* genesis *)
             Ok (None, (Vc.empty_cursor vc1, Vc.empty_cursor vc2))
         | Ok (Some p1) ->
             Ok (Some p1.Commit.hash,

                 (* Do we have copied it into [vc2] already? *)
                 match Vc.checkout' vc2 p1.Commit.hash with
                 | None ->
                     (* Lost.  We have to make a dummy entry for the parent *)
                     Log.warning "ADDING A DUMMY!";
                     let _, _ = Commit.add_dummy commits2 p1.hash in
                     (* We use [last_dst/src].  It is not the parent
                        but still likely share many parts with [c1]
                     *)
                     (last_src, last_dst)
                 | Some (_p2, cur2) ->
                     let pcur = from_Some @@ Vc.checkout vc1 p1.Commit.hash in
                     pcur, cur2)) >>= fun (parent, (psrc, pdst)) ->

        let _, nhpsrc = Cursor_hash.compute psrc in
        let _, nhpdst = Cursor_hash.compute pdst in
        assert (nhpsrc = nhpdst);

        (* Diff psrc and src *)
        let Cursor.Cursor (tr, pn, ctxt1,  _) = psrc in
        let Cursor.Cursor (tr', n, ctxt1', info) = src  in

        let Cursor(_, _, dctxt, _) = pdst in

        (* visit the copied segments and scan the nodes under them *)
        let copies = info.Info.copies in

        (* segments may not exist in psrc nor src *)

        let tbl = Hashtbl.create 1023 in

        let visit c segs =
          (* XXX go_up: true does not work because the internal access_gen moves the cursor elsewhere
             we move to the top at the end anyway
          *)
          Deep.deep ~go_up:false ~create_subtrees:false
            c
            segs
            (fun c seg ->
               (* should have Cursor.get_node? *)
               Cursor.access_gen c seg >>= function
               | Reached (c, _) ->
                   (* This traverse all the nodes and register the hashes.
                      XXX No point of rebuilding the tree.
                   *)
                   let node n =
                     let v, h = Node_hash.compute dctxt n in
                     let n = Node.View v in
                     if Hashtbl.mem tbl h then `Return (n, ())
                     else begin
                       let i = from_Some @@ Node.index n in
                       Hashtbl.add tbl h i;
                       `Continue v
                     end
                   in
                   let view = Node.Mapper.default_mkview in
                   let Cursor.Cursor (_, n, _, _) = c in
                   let n, () = Node.Mapper.map ~node ~view n in
                   let Cursor.Cursor (trail, _, ctxt, info) = c in
                   Ok (Cursor._Cursor (trail, n, ctxt, info), ())
               | _res ->
                   Ok (c, ()) (* not found *)
            ) >>= fun (c, res) ->
          Cursor.go_top c >>| fun c -> (c,res)
        in

        if List.length copies <> 0 then
          Format.eprintf "%d COPIES %d@." i (List.length copies);

        let (pdst, ()), sec =
          with_time @@ fun () ->
          from_Ok @@ Result.fold_leftM (fun (c,()) segs ->
              match visit c segs with
              | Ok res -> Ok res
              | Error _e ->
                  (* hmmm *)
                  Ok (c,())) (pdst,()) copies
        in

        if List.length copies <> 0 then begin
          Format.eprintf "%d COPIES %d HASHES %d visited in %.2f secs@."
            i
            (List.length copies)
            (Hashtbl.length tbl)
            sec;

          let (), sec = with_time @@ fun () ->
            Hashtbl.iter (fun h i ->
                (* This gives score 256, which should be too high *)
                Node_cache.add dctxt.node_cache h i ) tbl;
          in
          Format.eprintf "%d COPIES %d HASHES %d added in %.2f secs@."
            i
            (List.length copies)
            (Hashtbl.length tbl)
            sec;

          Format.eprintf "%d vc1: %a  vc2: %a@."
            i Context.pp_cache (Vc.context vc1) Context.pp_cache (Vc.context vc2);
        end;

        assert (tr = Cursor._Top && tr' = Cursor._Top);
        assert (ctxt1 == ctxt1');
        let diff, sec = with_time @@ fun () -> Diff.diff ctxt1 pn n in

        Format.eprintf "%d DIFFS %d in %.2f secs@." i (List.length diff) sec;

        match
          with_time @@ fun () ->
          Result.fold_leftM (fun c diff ->
(*
              (* XXX resetting may load the nodes fully without sharing, which may cost lots of memory *)
              let diff, sec = with_time @@ fun () -> Diff.reset_for_another_context ctxt1 diff in
*)
              let diff, sec = with_time @@ fun () -> Diff.reset_for_another_context' ~src:ctxt1 ~dst:dctxt diff in
              if sec > 1.0 then Format.eprintf "%d DIFF reset took long time: %.2f secs@." i sec;
              Diff.apply c diff) pdst diff
        with
        | Error e, _ -> Error e
        | Ok cur2, sec ->
            Format.eprintf "%d DIFFS %d applied in %.2f secs@." i (List.length diff) sec;

            (* Apply the diff to pdst *)
            let cur2, _hp, _commit =
              Vc.commit
                ~allow_missing_parent: true
                ~override: true
                vc2
                ~parent
                ~hash_override: (Some c1.Commit.hash)
                cur2
            in
            let _, nhsrc = Cursor_hash.compute src in
            let _, nhdst = Cursor_hash.compute cur2 in
            if nhsrc <> nhdst then begin
              Log.fatal "COPY IS BUGGY";
(* No point for huge trees
              let psrc = Cursor_storage.load_fully ~reset_index:false psrc in
              let src = Cursor_storage.load_fully ~reset_index:false src in
              let cur2 = Cursor_storage.load_fully ~reset_index:false cur2 in
              Debug.save_cursor_to_dot "src1.dot" psrc;
              Debug.save_cursor_to_dot "src2.dot" src;
              Debug.save_cursor_to_dot "copy.dot" cur2;
              Log.fatal "DIFF";
              List.iter (Log.fatal "%a" Diff.pp) diff;
              Format.eprintf "ODIFF@.";
              List.iter (Log.fatal "%a" Diff.pp) odiff;
*)
              assert (nhsrc = nhdst);
            end;

            (* Performance check *)
            let Cursor.Cursor (_, srcn, _, _) = src in
            let srci = from_Some @@ Node.index srcn in
            let Cursor.Cursor (_, cur2n, _, _) = cur2 in
            let cur2i = from_Some @@ Node.index cur2n in
            let diffsrc = Index.(to_float (srci - psi)) in
            let diffdst = Index.(to_float (cur2i - pdi)) in
            Log.notice "%d IDXs %a %a %.2f %.2f" i Index.pp srci Index.pp cur2i
              (diffdst /. diffsrc) (Index.to_float cur2i /. (max (Index.to_float srci) 1.0));

            Ok (i + 1, (srci,cur2i), src, cur2)
            ) (0, (Index.Unsafe.of_int 3, Index.Unsafe.of_int 3) (* the initial idx I guess *), Vc.empty_cursor vc1, Vc.empty_cursor vc2) cs
  >>| fun (i, (si,di), _, _) -> (i, si, di)
