(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Data_encoding_tools

(* Visit Plebeia tree in the fixed order
   to create a stream of node encodings *)
type t =
  | End (* End of snapshot *)
  | BudNone
  | BudSome
  | Internal
  | Extender of Segment.t (* Maybe this segments are big *)
  | Value of Value.t
  | Cached of int32 (* i-th non-cached node starting from 1 *)
  | HashOnly of Hash.t (* Contents unknown.  Used for Merkle proof *)

let encoding =
  let open Data_encoding in
  union [ case ~title: "End" (Tag 0) null
            (function End -> Some () | _ -> None)
            (fun () -> End)
        ; case ~title: "BudNone" (Tag 1) (constant "budnone")
            (function BudNone -> Some () | _ -> None)
            (fun () -> BudNone)
        ; case ~title: "BudSome" (Tag 2) (constant "budsome")
            (function BudSome -> Some () | _ -> None)
            (fun () -> BudSome)
        ; case ~title:"Internal" (Tag 3) (constant "internal")
            (function Internal -> Some () | _ -> None)
            (fun () -> Internal)
        ; case ~title:"Extender" (Tag 4) (obj1 (req "segment" Segment.encoding))
            (function Extender seg -> Some seg | _ -> None)
            (fun seg -> Extender seg)
        ; case ~title:"Value" (Tag 5) (obj1 (req "value" Value.encoding))
            (function Value v -> Some v | _ -> None)
            (fun v -> Value v)
        ; case ~title:"Cached" (Tag 6) (obj1 (req "cached" int32))
            (function Cached i -> Some i | _ -> None)
            (fun i -> Cached i)
        ; case ~title:"HashOnly" (Tag 7) (obj1 (req "hash" Hash.encoding))
            (function HashOnly nh -> Some nh | _ -> None)
            (fun nh -> HashOnly nh)
        ]

type command =
  | End
  | Read
  | BudSome of int32
  | Extender of int32 * Segment.t
  | Internal of int32

(* Beware!  Loaded all in memory! *)
let load' (ts : t Stream.t) =
  let open Node in
  let reader () = Stream.next ts in
  let tbl = Hashtbl.create 1000 in
  let hashonly_nodes = ref [] in
  let incr cntr = Int32.add cntr 1l in
  let register cntr n =
    (* If n's child is [Disk] created by [HashOnly], we get into trouble *)
    let f = function
      | View v -> `View v
      | Disk (i,_) ->
          match List.assoc i !hashonly_nodes with
          | exception Not_found -> assert false
          | nh -> `Hash nh
    in
    let n, _nh = Node_hash.compute' f n in
    Hashtbl.replace tbl cntr n;
    n
  in
  let register_hashonly cntr nh =
    let i = Index.Unsafe.of_int32 cntr in
    let n = Disk (i, if Hash.is_long nh then Is_Extender else Not_Extender) in
    Hashtbl.replace tbl cntr n;
    hashonly_nodes := (i, nh) :: !hashonly_nodes;
    n
  in
  let rec loop cntr jobs ns = match jobs, ns with
    | [], _ -> assert false
    | [(End : command)], [n] -> n
    | End::_, _ -> assert false
    | BudSome cntr' :: jobs, n::ns ->
        let n = register cntr' @@ new_bud @@ Some n in
        loop cntr jobs (n::ns)
    | Extender (cntr', seg) :: jobs, n::ns ->
        let n = register cntr' @@ new_extender seg n in
        loop cntr jobs (n::ns)
    | Internal cntr' :: jobs, n2::n1::ns ->
        let n = register cntr' @@ new_internal n1 n2 in
        loop cntr jobs (n::ns)
    | (BudSome _ | Extender _) :: _, [] -> assert false
    | Internal _ :: _, ([] | [_]) -> assert false
    | Read :: jobs, _ ->
        let t = reader () in
        match t with
        | End -> loop cntr (End :: jobs) ns
        | Cached i ->
            begin match Hashtbl.find tbl i with
            | exception Not_found -> assert false
            | n -> loop cntr jobs (n :: ns)
            end
        | Value v ->
            let n = register cntr @@ new_leaf v in
            loop (incr cntr) jobs (n :: ns)
        | HashOnly nh ->
            let n = register_hashonly cntr nh in
            loop (incr cntr) jobs (n :: ns)
        | BudNone ->
            let n = register cntr @@ new_bud None in
            loop (incr cntr) jobs (n :: ns)
        | BudSome ->
            loop (incr cntr) (Read :: BudSome cntr :: jobs) ns
        | Extender seg ->
            loop (incr cntr) (Read :: Extender (cntr, seg) :: jobs) ns
        | Internal ->
            loop (incr cntr) (Read :: Read :: Internal cntr :: jobs) ns
  in
  let n = loop 1l [Read; End] [] in
  n, !hashonly_nodes

let load { reader } =
  let read_nodes = ref 0 in
  let read_bytes = ref 0 in
  let report () =
    Format.eprintf "%dK plebeia nodes read %dMiB@."
      (!read_nodes / 1_000) (!read_bytes / 1_000_000)
  in
  let reader () =
    try
      let bytes, t = reader encoding in
      read_bytes := !read_bytes + bytes;
      incr read_nodes;
      if !read_nodes mod 1_000_000 = 0 then report ();
      Some t
    with
    | _ -> None
  in
  let ts = Stream.from (fun _ -> reader ()) in
  let res = load' ts in
  report ();
  res

let save'' write ctxt ~disk_expansion n =
  (* We cannot share the cache for non Disks and Disks.
     Non Disks and Disks may share the same hash, they will be read back
     differently!
  *)
  let tbl = Hashtbl.create 1000 in
  let tbl_disk = Hashtbl.create 1000 in
  let cntr = ref 0l in
  let node n =
    match n with
    | Node.Disk _ when not disk_expansion ->
        let _, nh = Node_hash.compute ctxt n in
        begin match Hashtbl.find_opt tbl_disk nh with
          | Some i ->
              write (Cached i);
              `Return (n, ())
          | None ->
              cntr := Int32.add !cntr 1l;
              write (HashOnly nh);
              `Return (n, ())
        end
    | _ ->
        let v, nh = Node_hash.compute ctxt n in
        match Hashtbl.find_opt tbl nh with
        | Some i ->
            write (Cached i);
            `Return (n, ())
        | None ->
            cntr := Int32.add !cntr 1l;
            Hashtbl.replace tbl nh !cntr;
            match n with
            | Disk _ when not disk_expansion ->
                write (HashOnly nh);
                `Return (n, ())
            | _ ->
                begin match v with
                | Bud (None, _, _) ->
                    write BudNone
                | Bud (Some _, _, _) ->
                    write BudSome;
                | Internal (_, _, _, _) ->
                    write Internal
                | Extender (seg, _, _, _) ->
                    write (Extender seg);
                | Leaf (v, _, _) ->
                    write (Value v)
                end;
                `Continue v
  in
  let view =
    let open Node in
    Node.Mapper.
      { leaf= (fun v i h -> View (_Leaf (v, i, h)), ())
      ; bud= (fun xop i h ->
            match xop with
            | None -> View (_Bud (None, i, h)), ()
            | Some (n, ()) -> View (_Bud (Some n, i, h)), ())
      ; extender= (fun seg (n,()) i h ->
            View (_Extender (seg, n, i, h)), ())
      ; internal= (fun (nl,()) (nr,()) i h ->
            View (_Internal (nl, nr, i, h)), ())
      }
  in
  ignore (Node.Mapper.map ~node ~view n)

let save' fd ctxt ~disk_expansion n =
  let written_nodes = ref 0 in
  let written_bytes = ref 0 in
  let report () =
    Format.eprintf "%dK plebeia nodes written %dMiB@."
      (!written_nodes / 1_000) (!written_bytes / 1_000_000)
  in
  let write v =
    let bytes = Data_encoding.Binary.to_bytes_exn ~buffer_size:1024 encoding v in
    let written = Unix.write fd bytes 0 (Bytes.length bytes) in
    assert (Bytes.length bytes = written);
    written_bytes := !written_bytes + written;
    incr written_nodes;
    if !written_nodes mod 1_000_000 = 0 then report ()
  in
  save'' write ctxt ~disk_expansion n;
  report ()

let save fd ctxt n = save' fd ctxt ~disk_expansion: true n
let save_keeping_disk_nodes fd ctxt n = save' fd ctxt ~disk_expansion: false n
