(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Node
open Hash

let compute' g n : (Node.t * t) =
  let node n = match g n with
    | `Hash nh -> `Return (n, nh)
    | `View v ->
        match v with
        | Leaf (_, _, Hashed hp)
        | Bud (_, _, Hashed hp)
        | Internal (_, _, _, Hashed hp)  -> `Return (View v, (hp, ""))
        | Extender (seg, _, _, Hashed hp) -> `Return (View v, of_extender seg (hp, ""))
        | _ -> `Continue v
  in
  let view =
    { Node.Mapper.leaf= ( fun v _ _ ->
          let h = of_leaf v in
          View (_Leaf (v, Not_Indexed, Hashed (prefix h))), h )
    ; bud= ( fun nop _ _ ->
          match nop with
          | None ->
              let h = of_bud None in
              View (_Bud (None, Not_Indexed, Hashed (prefix h))), h
          | Some (n, h) ->
              let h = of_bud (Some h) in
              View (_Bud (Some n, Not_Indexed, Hashed (prefix h))), h )
    ; extender= ( fun seg (n, h) _ _ ->
          let h = of_extender seg h in
          View (_Extender (seg, n, Not_Indexed, Hashed (prefix h))), h )
    ; internal= ( fun (nl,hl) (nr,hr) _ _ ->
          let h = of_internal hl hr in
          View (_Internal (nl, nr, Not_Indexed, Hashed (prefix h))), h )
    }
  in
  Node.Mapper.map ~node ~view n

let compute_with_context context n =
  let f n = `View (Node.view context n) in
  let n, h = compute' f n in
  let v = match n with
    | Disk _ -> assert false
    | View v -> v
  in
  v, h

let compute = compute_with_context

let compute_with_disk_node_hashes alist =
  let f n = match n with
    | Disk (i, _) -> `Hash (List.assoc i alist)
    | View v -> `View v
  in
  compute' f

let of_view =
  let open Node in
  function
  | ( Bud (_, _, Hashed hp)
    | Leaf (_, _, Hashed hp))
    | (Internal (_, _, _, Hashed hp)) -> Some (hp,"")
  | Extender (seg, _, _, Hashed hp) -> Some (hp, Segment.Serialization.encode seg)
  | _ -> None

let of_node node =
  match node with
  | Disk _ -> None
  | View v -> Some (v, of_view v)
