(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

val save : Unix.file_descr -> Context.t -> Node.t -> unit
(** Dump the snapshot of Plebeia tree pointed by [Cursor.t] to [file_descr].

    To reduce the size of the snapshot, it performs perfect hash-consing.
    The table for the hash-consing is on memory.
*)

val save_keeping_disk_nodes : Unix.file_descr -> Context.t -> Node.t -> unit

val save' : Unix.file_descr -> Context.t -> disk_expansion: bool -> Node.t -> unit
(** Unified version of [save] and [save_keeping_disk_nodes] *)

type t
(** Snapshot dump element *)

val encoding : t Data_encoding.encoding

val save'' : (t -> unit) -> Context.t -> disk_expansion: bool -> Node.t -> unit
(** Generalized version of [save'].  The writer is parameteraized. *)

val load : Data_encoding_tools.reader -> Node.t * (Index.t * Hash.t) list
(** Load the snapshot file of Plebeia tree from [reader].

    The entire tree is node-hash computed.

    The entire tree is loaded on memory.

    [load] returns an association list of [Index.t * Hash.t].
    Hash only nodes are loaded as [Disk (i, _)], whose index [i] is
    recorded in this association list with the node hash.
*)

val load' : t Stream.t -> Node.t * (Index.t * Hash.t) list
(** Quiet version of [load] *)
