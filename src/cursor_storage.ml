(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)
open Cursor

let commit_top_cursor (Cursor (trail, node, context, info) as c) =
  match trail with
  | Top ->
      let (node, i, h) = Node_storage.commit_node context node in
      _Cursor (_Top, node, context, info), i, h
  | _ ->
      let segs = Cursor.segs_of_cursor c in
      failwith (Printf.sprintf "commit: cursor must point to the root: %s"
                  (* LRLLLRRLR... Not quite useful... *)
                  (Segment.string_of_segments segs))

let load_fully_for_test (Cursor (trail, node, context, i)) =
  _Cursor (trail, Node_storage.load_node_fully_for_test context node, context, i)

let load_fully ~reset_index (Cursor (trail, node, context, i)) =
  _Cursor (trail, Node_storage.load_node_fully ~reset_index context node, context, i)
