(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Utils

module Int64 = Stdint.Int64

(* XXX Problem in 32bit arch

   * [Cstruct.of_bigarray] only takes [char] indexed [Bigstring.t].
   * Offset must be [int] in [Cstruct].

   The current simple implementation to map the entire file to one [Bigstring.t]
   restricts the maximum file size in 32bit arch to [1_073_741_823], which is roughly just 1GiB.
*)

module C = Xcstruct

type mode =
  | Private
  | Reader
  | Writer

let is_shared = function
  | Private -> false
  | Reader | Writer -> true

module Make(P : sig
    val head_string : string
    val version : int
    val bytes_per_cell : int
    val max_index : Index.t
end) = struct

  include P

  type nonrec mode = mode = Private | Reader | Writer

  let () =
    assert (String.length head_string = 24);
    assert (version >= 0 && Int64.of_int version <= Int64.of_uint32 Stdint.Uint32.max_int)

  let bytes_per_cell_int64 = Int64.of_int P.bytes_per_cell

  let start_index =
    (* We use 32 * 3 = 96 bytes to store the header
       +0 for PLEBEIA.
       +32 and +64 for header
    *)
    let cells_for_header =
      (32 * 3 + bytes_per_cell - 1) / bytes_per_cell
    in
    Index.Unsafe.of_int cells_for_header

  type storage = {
    mutable array : Bigstring.t ;
    (* mmaped array where the nodes are written and indexed. *)

    mutable current_length : Index.t ;
    (* Current length of the node table.
       The next index number to be used.

       current_length < mapped_length
    *)

    mutable mapped_length : Index.t ;

    mutable last_root_index : Index.t option ;
    (* The last commit entry position *)

    fd : Unix.file_descr ;
    (* File descriptor to the mapped file *)

    pos : int64 ;
    (* Position of the first cell in the file, in bytes *)

    mode : mode ;

    version : int ;
    fn : string ;

    resize_step : Index.t ;
    (* How much space allocated for each resize, in cells *)

    mutable closed : bool ;
    (* [true] if [fd] is already closed. *)
  }

  type t = storage

  let set_last_root_index t x  = t.last_root_index <- x
  let get_last_root_index t    = t.last_root_index

  let get_current_length t = t.current_length

  let size t = Stdint.Int64.(Index.to_int64 (get_current_length t ) * bytes_per_cell_int64)

  let filename t = t.fn

  let mode t = t.mode

  let version t = t.version

  (* Resize *)

  let make_array fd ~pos mode mapped_length =
    let shared = is_shared mode in
    let open Bigarray in
    let size = Int64.(of_uint32 mapped_length * bytes_per_cell_int64) in
    let size =
      if size > Int64.of_int Stdlib.max_int then
        failwithf "Size %Ld is too big in this archtecture" (Int64.of_uint32 mapped_length)
      else Int64.to_int size
    in
    array1_of_genarray @@ Unix.map_file fd ~pos char c_layout shared [| size |]

  let msync t =
    if t.mode = Writer then
      Plebeia_msync.Msync.msync @@ Bigarray.genarray_of_array1 t.array

  let resize required t =
    let open Index in
    assert (t.mode = Writer);
    assert (required > t.mapped_length);
    msync t;
    let new_mapped_length =
      ((required - t.mapped_length) / t.resize_step + Index.one) * t.resize_step  + t.mapped_length
    in
    Log.notice "Storage: resizing to %Ld" (Index.to_int64 new_mapped_length);
    let array = make_array t.fd ~pos:t.pos t.mode (Index.to_uint32 new_mapped_length) in
    t.array <- array;
    t.mapped_length <- new_mapped_length

  let may_resize =
    fun required t ->
      if t.mapped_length < required then
        resize required t
      else ()

  (* Access *)

  (* for stat *)
  let accessed_indices = ref None

  let set_accessed_indices x = accessed_indices := x

  let get_bytes t i n =
    (* XXX May overflow in 32bits arch! *)
    begin match !accessed_indices with
      | None -> ()
      | Some ref -> ref := i :: !ref
    end;
    let i = Index.Unsafe.to_int i in
    C.of_bigarray ~off:(i*bytes_per_cell) ~len:n t.array

  let get_cell t i = get_bytes t i bytes_per_cell
  let get_cell2 t i = get_bytes t i (bytes_per_cell * 2)

  let new_index c =
    (* XXX check of size *)
    let i = c.current_length in
    let i' = Index.succ i in
    c.current_length <- i';
    may_resize i' c;
    i

  let new_indices c n =
    (* XXX check of size *)
    assert (n > 0);
    let i = c.current_length in
    let i' = Index.(i + Unsafe.of_int n) in
    c.current_length <- i';
    may_resize i' c;
    i

  module Header = struct
    type t =
      { last_next_index : Index.t
      ; last_root_index : Index.t option
      }

    (*
    +0
    |0                   19|20         23|24        27|28        31|
    |NAME                                |<-version ->|     0      |

    +32
    |0                   19|20         23|24        27|28        31|
    |< hash of the right               ->|<- i root ->|<- i next ->|

    +64
    |0                   19|20         23|24        27|28        31|
    |< hash of the right               ->|<- i root ->|<- i next ->|

    *)

    module Blake2B_24 = Blake2B.Make(struct let bytes = 24 end)

    (* If Writer modifies the data about being read, it should be detected
       by a hash disagreement.
    *)
    let raw_read' array i =
      let cstr = C.of_bigarray ~off:(i*32) ~len:32 array in
      let last_next_index = C.get_index cstr 28 in
      let last_root_index = Index.zero_then_none @@ C.get_index cstr 24 in
      let h = C.copy cstr 0 24 in
      let string_24_31 = C.copy cstr 24 8 in
      let h' = Blake2B_24.of_string string_24_31 in
      if h <> h' then None
      else Some { last_next_index ; last_root_index }

    let raw_read array =
      match raw_read' array 1 with
      | Some x -> Some x
      | None -> (* something wrong in +32 *)
          match raw_read' array 2 with
          | Some x -> Some x
          | None -> None (* something wrong in +64 *)

    let write' t i ({ last_next_index ; last_root_index } as x) =
      if t.mode = Reader then invalid_arg "Reader cannot write";
      let cstr = C.of_bigarray ~off:(i*32) ~len:32 t.array in
      C.set_index cstr 28 last_next_index;
      C.set_index cstr 24 (Option.default Index.zero last_root_index);
      let string_24_31 = C.copy cstr 24 8 in
      let h = Blake2B_24.of_string string_24_31 in
      C.write_string h cstr 0 24;
      (* read back and compare.  XXX removed when assured *)
      match raw_read' t.array i with
      | None -> assert false
      | Some x' -> assert (x = x')

    let write t x =
      (* The write is NOT atomic but corruption can be detected by the checksum
         and the double writes
      *)
      write' t 1 x;
      write' t 2 x

    let commit t =
      msync t;
      let cp = { last_next_index = t.current_length (* XXX inconsistent names ... *)
               ; last_root_index = t.last_root_index }
      in
      write t cp

    let write_version t =
      let cstr = C.of_bigarray ~off:0 ~len:32 t.array in
      C.blit_from_string (head_string ^ String.make 8 '\000') 0 cstr 0 32;
      C.set_index cstr 24 (Index.Unsafe.of_int t.version)

    let check_type_and_version array =
      let cstr = C.of_bigarray ~off:0 ~len:32 array in
      if not (C.copy cstr 0 24 = head_string) then failwith "This is not data file we expect";
      let version = Stdint.Uint32.to_int @@ C.get_uint32 cstr 24 in
      version
  end

  let commit = Header.commit

  let default_resize_step = Index.Unsafe.of_int (32_000_000 / bytes_per_cell) (* 32MB *)

  let create
      ?(pos=0L)
      ?length
      ?(resize_step=default_resize_step)
      ?(version=Version.version)
      fn =
    let fd = Unix.openfile fn [O_CREAT; O_EXCL; O_RDWR] 0o644 in
    let mapped_length =
      (* XXX Simpler if length has uint32 *)
      match length with
      | None -> resize_step
      | Some i ->
          assert (i > 0);
          match Sys.int_size with
          | 31 -> Index.Unsafe.of_int i
          | 63 ->
              if i > Index.(Unsafe.to_int max_int) then failwithf "create: too large: %d@." i
              else Index.Unsafe.of_int i
          | _ -> assert false
    in
    let array = make_array fd ~pos Writer (Index.to_uint32 mapped_length) in

    let cstr = C.of_bigarray ~off:0 ~len:32 array in
    C.blit_from_string head_string 0 cstr 0 24;
    C.set_index cstr 24 (Index.Unsafe.of_int version);
    let t =
      { array ;
        mapped_length ;
        current_length = start_index ;
        last_root_index = None ;
        fd ;
        pos ;
        mode= Writer;
        version ;
        fn ;
        resize_step ;
        closed = false ;
      }
    in
    Header.commit t;
    t

  let override_version t version =
    let t = { t with version } in
    Header.write_version t

  let truncate ?length t =
    msync t;
    Unix.ftruncate t.fd (Int64.to_int t.pos);
    let mapped_length =
      match length with
      | None -> t.resize_step
      | Some i ->
          match Sys.int_size with
          | 31 | 32 -> Index.Unsafe.of_int i
          | 63 ->
              if i > Index.(Unsafe.to_int max_int) then failwithf "create: too large: %d@." i
              else Index.Unsafe.of_int i
          | _ -> assert false
    in
    let array = make_array t.fd ~pos:t.pos t.mode (Index.to_uint32 mapped_length) in
    Header.write_version t;

    t.array <- array;
    t.mapped_length <- mapped_length;
    t.current_length <- start_index;
    t.last_root_index <- None ;
    Header.commit t

  let open_ ?(pos=0L) ?(resize_step=default_resize_step) ?(ignore_version=false) ~mode fn =
    (* XXX no check of cell_size *)
    if not @@ Sys.file_exists fn then
      if mode = Writer then create ~pos fn
      else failwithf "%s: file not found" fn
    else begin
      let fd = Unix.openfile fn [O_RDWR] 0o644 in
      let st = Unix.LargeFile.fstat fd in
      let sz = Int64.sub st.Unix.LargeFile.st_size pos in
      assert (Int64.rem sz bytes_per_cell_int64 = 0L);  (* XXX think about the garbage *)
      let cells = Int64.(sz / bytes_per_cell_int64) in
      if cells > Index.to_int64 max_index then assert false;
      let mapped_length = Index.of_int64 cells in
      let array = make_array fd ~pos mode (Index.to_uint32 mapped_length) in

      let version = Header.check_type_and_version array in

      if not ignore_version && not @@ Version.check version then begin
        Log.fatal "ERROR: version mismatch.  The file's version is %d while the system version is %d.." version Version.version;
        exit 2;
      end;

      match Header.raw_read array with
      | None -> failwithf "Failed to load header"
      | Some h ->
          { array ;
            mapped_length ;
            current_length = h.Header.last_next_index;
            last_root_index = h.Header.last_root_index;
            fd = fd ;
            pos ;
            mode ;
            version ;
            fn ;
            resize_step ;
            closed = false ;
          }
    end

  (* XXX auto close when GC'ed *)
  let close ({ fd ; mode ; closed ; _ } as t) =
    if closed then ()
    else begin
      if mode <> Reader then begin
        Header.commit t;
        msync t; (* make sure the header is flushed *)
      end;
      Unix.close fd;
      t.closed <- true;
    end

  let reopen t =
    (* We load the header first, before fstat the file.

       If we would do opposite, the following might happen:

       * Reader fstats
       * Writer extends the file, making the fstats obsolete
       * Write update the header, last_indices point out of the obsolete fstat
       * Reader reads the header
       * Reader fails to load the last_indices, since it is not mapped
    *)
    msync t;
    match Header.raw_read t.array with
    | None -> failwithf "Failed to load header"
    | Some h ->
        let st = Unix.LargeFile.fstat t.fd in
        let sz = Int64.sub st.Unix.LargeFile.st_size t.pos in
        assert (Int64.rem sz bytes_per_cell_int64 = 0L);  (* XXX think about the garbage *)
        let cells = Int64.(sz / bytes_per_cell_int64) in
        if cells > Index.to_int64 max_index then assert false;
        let mapped_length = Index.of_int64 cells in
        let array = make_array t.fd ~pos:t.pos t.mode (Index.to_uint32 mapped_length) in
        t.array            <- array;
        t.current_length   <- h.Header.last_next_index;
        t.mapped_length    <- mapped_length;
        t.last_root_index  <- h.Header.last_root_index

  let sync t =
    match t.mode with
    | Writer | Private -> ()
    | Reader -> reopen t
end
