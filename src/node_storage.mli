(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Node

exception LoadFailure of Error.t

val parse_cell : Storage.t -> Index.t -> view
(** Parse the cell of the storage at the given index.  Exposed for test. *)

val commit_node : Context.t -> node -> node * Index.t * Hash.Prefix.t
(** Write a node to the storage, and returns the updated version of the node
   with its index and hash.
*)

val load_node : Context.t -> Index.t -> extender_witness -> view
(** Read the node at the given index of the context,
    parse it and create a view node with it. *)

val load_node_fully_for_test : Context.t -> node -> node
(** Recusively visit and load all the subnodes in memory.
    Only for test purposes.  Not tail recursive.
*)

val load_node_fully : reset_index: bool -> Context.t -> node -> node
(** Recusively visit and load all the subnodes in memory.

    (if [reset_index], all the indices are reset to [Not_Indexed].)
*)

val load_diff : src:Context.t -> dst:Context.t -> t -> t
(** Make the diff from [src] context applicable to another context [dst] *)

val equal : Context.t -> node -> node -> (unit, (node * node)) Result.t
(** Used for test. *)


val leaf : Context.t -> (Value.t * indexed * hashed) -> Node.t
val internal : Context.t -> (Node.t * Node.t * indexed * hashed) -> Node.t
val bud : Context.t -> (Node.t option * indexed * hashed) -> Node.t
val extender : Context.t -> (Segment.t * Node.t * indexed * hashed) -> Node.t
