(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** { 1 Root hash table }

  All the data should be in memory.

  The root hashes are stored in the context.  They are chained and the last
  root hash entry is recorded in the header of the context.

  Note that the chain of root hashes in the context do not correspond with
  the parent-children relationship of root hashes.

*)

module Hash : sig
  (** Hash stored in the roots in 32bytes.
      This can be completely different from Plebeia's root Merkle hash prefixes:
      as of 2020-06 Tezos uses hash of Irmin, which is not by Plebeia.
  *)

  type t

  val of_string       : string -> t
  (** 32byte binary string representation to [t].  Fails if the string is inappropriate. *)

  val to_string       : t -> string
  (** Get the 32byte binary string representation of [t]. *)

  val of_plebeia_hash_prefix : Hash.Prefix.t -> t
  (** Conversion of Plebeia hash prefix (28bytes) to [t] by appending 4 zeros at the end *)

  val to_hex_string   : t -> string
  (** Convert to 64chars of hexdigits *)

  val show_ref : (t -> string) ref
  (** Used for [pp].  Default: [to_hex_string].  By modifying this ref
      the [pp] can be overridden.  The default is [to_hex_string].
  *)

  val pp : Format.formatter -> t -> unit
end

module Entry : sig
  type t = private
    { parent : Index.t option
      (** Index of the parent Plebeia root node in storage_context.
          [Some Index.one] if the entry is a dummy.
      *)

    ; index  : Index.t
      (** Index of the Plebeia tree root node in storage_context.
          The index of the entry itself on storage_context if it is a dummy.
      *)

    ; hash   : Hash.t
      (** Context hash *)

    ; info   : Index.t option
      (** Index of the Info.t in storage_conetxt. *)
    }

  val make : parent:Index.t option -> index:Index.t -> hash:Hash.t -> info:Index.t option -> t

  type Error.t += Dummy_commit_cannot_have_parent_for_now

  val is_genesis : t -> bool
  (** Returns [true] if the entry is a genesis *)

  val is_dummy : t -> bool
  (** Returns [true] if the entry is a dummy without the actual data *)

  val pp : Format.formatter -> t -> unit

  val compare : t -> t -> int
  (** Comparison by [index] field *)

  module Store : sig
    val write_at : Storage.t -> at:Index.t -> prev:Index.t option -> t -> unit
    (** Write (or overwrite) [t] to the specified index [at] and its predecessor.
        [prev] is the index of the previous Entry.
    *)
  end
end

type entry = Entry.t = private
  { parent : Index.t option
  ; index  : Index.t
  ; hash   : Hash.t
  ; info   : Index.t option
  }

type t
(** Storage type *)

val create : storage_context: Storage.t -> storage_roots: Storage.t -> t
(** Load root hashes of the context and return t *)

val create' : storage_context: Storage.t -> storage_roots: Storage.t option -> t
(** Load root hashes of the context and return t

    storage_roots is optional.
*)

val close : t -> unit

type Error.t += Conflict

val read_additional_commits : t -> (int, Error.t) Result.t
(** Reload additional commits from the updated context.

    Readers must call [Storage.sync] prior to it in order to load new
    roots added by the Writer.
*)

val sync : t -> unit
(** For reader, to update the root table informat which may be updated
    by the writer.  (It calls [read_additional_commits] internally.)
*)

val add : t -> parent: Index.t option -> Hash.t -> Index.t -> info:Index.t option -> Entry.t * Index.t
(** Add a new root hash, with its index, parent.

    It immediately saves the root to the disk.

    If the root hash already exists in the table, it is overridden
    with the new one by [add].
*)

val add_dummy : t -> Hash.t -> Entry.t * Index.t
(** Add a root hash without the actual data.

    It immediately saves the root to the disk.

    If the root hash already exists in the table, it is overridden
    with the new one by [add].
*)

val mem : t -> Hash.t -> bool
(** Existence check.  O(1) using hash table  *)

val find : t -> Hash.t -> entry option
(** Find a root of the given hash.  O(1) using hash table  *)

val find_by_index : t -> Index.t -> entry option
(** Find by index.  O(1) using hash table *)

val parent : t -> entry -> (entry option, Error.t) result
(** Returns the entry of the parent:
     * [Error]: unknown since the entry is a dummy
     * [Ok None]: genesis
     * [Ok (Some ent)]: parent exists
*)

val children : t -> entry -> entry list
(** Returns the childlren of the index.  O(1) using hash table *)

val iter : (entry -> unit) -> t -> unit
(** Iteration *)

val length : t -> int
(** The number of entries in the table *)

val to_list : t -> entry list
(** Returns the entries.  Older to newer. *)

val fold : (entry -> 'a -> 'a) -> t -> 'a -> 'a

val genesis : t -> entry list
(** Returns the hashes which have no parents.  O(n) *)

val dummies : t -> entry list
(** Returns the dummy hashes.  O(n) *)

val read_the_latest : t -> entry option
(** Get the latest addition to the roots. *)

val decendants : t -> entry -> entry list
(** The decendants of the given entry *)

val ancestors : t -> while_:(entry -> bool) -> entry -> (entry list, Error.t) result
(** Ancestors of the given entry.  The scan is terminated when [while_] returns [false].
    For all the returned entries [e], [while_ e = true].
*)
