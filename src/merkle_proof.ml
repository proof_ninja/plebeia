(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)
open Result
open Node
open Cursor

module Proof : sig
  type t
  val from_cursor : Cursor.t -> (t, Error.t) Result.t
  val to_node : t -> Node.t * (Index.t * Hash.t) list
  val encoding : t Data_encoding.t
end = struct
  type t = Snapshot.t list

  let from_cursor cur =
    let Cursor (_, node, ctx, _) = cur in
    let revref : Snapshot.t list ref = ref [] in
    let disk_expansion = false in
    Snapshot.save'' (fun t -> revref := t :: !revref) ctx ~disk_expansion node;
    return (List.rev !revref)

  let to_node proof =
    Snapshot.load' (Stream.of_list proof)

  let encoding = Data_encoding.list Snapshot.encoding
end

type t = Proof.t

type key = Segment.t list

let show_key key=
  List.map Segment.to_string key
  |> String.concat " / "

let get_and_go_down cur seg =
  match Cursor.access_gen cur seg with
  | Ok (Reached (cur, Bud (_, _, _))) -> `Reached cur
  | Ok (Reached (cur, Leaf (_, _, _))) -> `Reached cur
  | Ok (Reached (cur, _)) -> `Unreached cur
  | Ok Empty_bud -> `Unreached cur
  | Ok (Collide (cur, _)) -> `Unreached cur
  | Ok (Middle_of_extender (cur, _, _, _)) -> `Unreached cur
  | Error err -> Error.raise err

type Error.t += Merkleproof_needs_to_be_committed
let () = Error.register_printer @@ function
  | Merkleproof_needs_to_be_committed ->
     Some ("The cursor needs to be committed for generating the merkle proofs")
  | _ -> None

type find_result =
  | Found of Cursor.t
  | NotFound of Cursor.t

let cursor_of_find_result = function
  | Found cur -> cur
  | NotFound cur -> cur

let generate_proof cur (key : key) =
  let is_bud = function
    | Bud _ -> true
    | _ -> false
  in
  let is_leaf = function
    | Leaf _ -> true
    | _ -> false
  in
  let rec iter cur = function
    | [] ->
       let (cur, v) = view cur in
       if is_leaf v then Found cur
       else NotFound cur
    | seg :: rest ->
       let (cur, v) = view cur in
       if is_bud v then
         match get_and_go_down cur seg with
         | `Reached cur -> iter cur rest
         | `Unreached cur ->
            NotFound cur
       else
         NotFound cur
  in
  Cursor.go_top cur >>= fun cur ->
  let cur = cursor_of_find_result @@ iter cur key in(* forget if found or not *)
  Cursor.go_top cur >>= fun cur ->
  return cur


let encoding = Proof.encoding

let unload cur =
  match Cursor.may_forget cur with
  | Some cur -> return cur
  | None -> Error (Merkleproof_needs_to_be_committed)

let generate cur keys =
  Cursor.go_top cur >>= fun cur ->
  unload cur >>= fun init_cur ->
  Result.fold_leftM generate_proof init_cur keys >>= fun cur ->
  Proof.from_cursor cur >>= fun proof ->
  return proof

type Error.t += Merkleproof_unloaded_part_detected of string
type Error.t += Merkleproof_invalid_proof of string

let () = Error.register_printer @@ function
  | Merkleproof_unloaded_part_detected message ->
     Some ("Can not validate the merkle proof since unloaded part detected: " ^ message)
  | Merkleproof_invalid_proof message ->
     Some ("The merkle proof is invalid: " ^ message)
  | _ -> None

let rm_prefix prefix seg =
  let (_, prefix', seg') = Segment.common_prefix prefix seg in
  if Segment.is_empty prefix' then Some seg'
  else None

let get_without_loading key_for_error node seg =
  let rec iter seg = function
    | Node.View v ->
       begin match (Segment.cut seg, v) with
       | (None, Leaf _) | (None, Bud _) ->
          return @@ Some v
       | (None, _) ->
          return None
       | (Some _, Leaf _) | (Some _, Bud _) ->
          return None
       | (Some (Segment.Left, seg'), Internal (l, _, _, _)) ->
          iter seg' l
       | (Some (Segment.Right, seg'), Internal (_, r, _, _)) ->
          iter seg' r
       | (Some _, Extender (prefix, n, _, _)) ->
          begin match rm_prefix prefix seg with
          | Some rest -> iter rest n
          | None -> return None
          end
       end
    | Disk _ -> Error (Merkleproof_unloaded_part_detected (show_key key_for_error))
  in
  match node with
  | View (Bud (Some n, _, _)) ->
     iter seg n
  | View _ ->
     return None
  | Disk _ ->
     Error (Merkleproof_unloaded_part_detected
              ("get_without_loading: " ^ show_key key_for_error))

let find_without_loading node key =
  let rec iter node = function
    | [] -> return (Some node)
    | seg :: rest ->
       begin
       get_without_loading key node seg >>= function
         | Some v -> iter (View v) rest
         | None -> return None
       end
  in
  iter node key

(* CR jun to yoshihiro : "validate" is confusing.   See the laster CR
   for details. *)
let validate_proof (node, _dict) key =
  find_without_loading node key >>= fun node_opt ->
  return node_opt

let top_hash_of_node node dict =
  try
    let (_, nh) = Node_hash.compute_with_disk_node_hashes dict node in
    return nh
  with
  | _ ->
     Error (Merkleproof_invalid_proof "Can not compute the node hash of root")

(* CR jun to yoshihiro

   The name "validation" is confusing and should be renamed, since Merkle proof
   validation is the comparison of 2 top hashes.  For example, we can say:
      "Hey, this Merkle proof is valid, but it does not contain what I want to know!"

   This function must be split into 2

   - compute_hash : reconstruct the top hash of the given proof
   - get_node : query the value of given path
*)
let validate keys proof =
  if keys = [] then
    Error (Merkleproof_invalid_proof "The keys must not be empty")
  else
    let (node, dict) = Proof.to_node proof in
    top_hash_of_node node dict >>= fun node_hash ->
    Result.mapM (validate_proof (node, dict)) keys >>= fun node_opts ->
    return (node_hash, node_opts)
