(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)
(* Bud cache with some easy heuristics *)

open Utils

type stat = { ever_hit : int ; ever_added : int }

type config =
  { threshold_commit : int   (* max elems survive each commit *)
  ; threshold_absolute : int (* max elems of any time *)
  ; shrink_ratio : float     (* how much we shrink *)
  }

let default_config =
  { threshold_commit = 1_000_000
  ; threshold_absolute = 5_000_000
  ; shrink_ratio = 0.8
  }

let check_config c =
  if c.threshold_commit <= c.threshold_absolute
     && 0. <= c.shrink_ratio && c.shrink_ratio <= 1.0
  then Ok () else Error ()

type t =
  { tbl : (Hash.t, (Index.t * int ref)) Hashtbl.t
    (* int ref for the score.

       Size: 16 words, 128bytes in 64bit arch
       for (Hash.t * (Index.t * int ref)) *)
  ; mutable stat : stat
  ; config : config
  }

let get_stat t = t.stat

let create config =
  match check_config config with
  | Error () -> invalid_arg "Invalid config"
  | Ok () ->
      { tbl = Hashtbl.create 0
      ; stat = { ever_hit= 0; ever_added= 0 }
      ; config
      }

let size t = Hashtbl.length t.tbl

let find_opt t nh = match Hashtbl.find_opt t.tbl nh with
  | None -> None
  | Some (i, cntr) ->
      cntr := min 65536 (!cntr + 256); (* completely no logical idea *)
      t.stat <- { t.stat with ever_hit = t.stat.ever_hit + 1 };
      Some i

let pp_stat ppf t =
  Format.fprintf ppf "size=%d added=%d hit=%d ratio=%.2f"
    (size t)
    t.stat.ever_added t.stat.ever_hit
    (float t.stat.ever_hit /. float (t.stat.ever_added + t.stat.ever_hit))

(* Obj hack.  Only for development *)
let reachable_words t = Obj.reachable_words (Obj.repr t)

let shrink' t threshold =
  let sz = size t in
  if sz > threshold then begin
    let goal = int_of_float @@ float threshold *. t.config.shrink_ratio in
    Log.notice "node_cache shrinking from %d to %d" sz goal;
    (* XXX Very simple inefficient loop.  Sometimes overclean things *)
    let rec loop () =
      let sz = size t in
      if sz <= goal then ()
      else begin
        let must_be_removed = ref (sz - goal) in
        Hashtbl.filter_map_inplace (fun _nh (i, cntr) ->
            let n = !cntr / 2 in
            if n = 0 && !must_be_removed > 0 then begin
              decr must_be_removed;
              None
            end
            else Some (i, ref n)) t.tbl;
        loop ()
      end
    in
    let (), secs = with_time loop in
    let sz_current = size t in
    (* reachable_words may take long time... *)
    let words, secs_count = with_time @@ fun () -> reachable_words t in
    Log.notice "node_cache shrink from %d to %d in %.2f secs, %d words %.2f MB in %.2f secs, %.2f B per entry "
      sz sz_current
      secs
      words (float words *. float (Sys.word_size / 8) /. 1000_000.0)
      secs_count
      (float words *. float (Sys.word_size / 8) /. float sz_current)
    ;
  end

let shrink t = shrink' t t.config.threshold_commit

let add t nh i =
  Hashtbl.replace t.tbl nh (i, ref 256); (* survives 8 shrink loops at least *)
  t.stat <- { t.stat with ever_added= t.stat.ever_added + 1 };
  shrink' t t.config.threshold_absolute
