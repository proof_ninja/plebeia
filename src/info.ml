(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2020 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(* Info stored in Cursor.t *)

open Utils

type t =
  { copies : Segment.t list list  (* invariant: Segment.list is never empty *)
  ; tribune : Index.t option
  }

let empty = { copies = [] ; tribune = None }

let equal t1 t2 =
  if List.length t1.copies <> List.length t2.copies then false
  else
    (* not tail recursive... *)
    List.for_all2 (fun segs1 segs2 ->
        Segment.compare_list segs1 segs2 = 0
      ) t1.copies t2.copies

let encode t =
  if t = empty then ""
  else
    (* for future extensibility, info is saved as (tag*content) list
       ended with 0
    *)
    let copies =
      if t.copies = [] then []
      else "\001" :: List.map Segment.Serialization.encode_list (t.copies @ [[]])
    in
    let tribune =
      match t.tribune with
      | None -> []
      | Some i -> [ "\002"; Xcstruct.encode_index i ]
    in
    String.concat "" @@ copies @ tribune @ [ "\000" ]

let decode_copies (s, off) =
  let rec g rev_copies off =
    match Segment.Serialization.decode_list_slice (s, off) with
    | None -> None (* error *)
    | Some ([], off) -> (* end *)
        Some (List.rev rev_copies, off)
    | Some (copy, off) ->
        g (copy :: rev_copies) off
  in
  g [] off

let decode s =
  if s = "" then Some empty
  else
    let slen = String.length s in
    let rec f copies tribune off =
      match String.unsafe_get s off with
      | '\000' when off + 1 = slen -> (* EOS *)
          Some { copies =
                   begin match copies with
                     | None -> []
                     | Some copies -> copies
                   end
               ; tribune
               }
      | '\000' -> None (* garbage at the end *)
      | '\001' when copies <> None ->
          None (* copies cannot appear more than once *)
      | '\001' ->
          begin match decode_copies (s, off+1) with
            | None -> None
            | Some (copies, off) -> f (Some copies) tribune off
          end
      | '\002' when tribune <> None ->
          None (* tribune cannot appear more than once *)
      | '\002' ->
          begin
            match String.sub s (off+1) 4 with
            | exception _ -> None
            | i -> f copies (Some (Xcstruct.decode_index i)) (off+5)
          end
      | _c -> None (* unknown kind *)
    in
    f None None 0

let write storage info = Storage.Chunk.write storage (encode info)

type Error.t += Invalid

let () = Error.register_printer @@ function
  | Invalid -> Some "Invalid commit info"
  | _ -> None

let read storage i =
  match decode (Storage.Chunk.read storage i) with
  | None -> Error Invalid
  | Some v -> Ok v
