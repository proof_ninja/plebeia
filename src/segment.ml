(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2018 Arthur Breitman <arthur.breitman+nospam@tezos.com>     *)
(* Copyright (c) 2019 DaiLambda, Inc. <contact@dailambda.jp>                 *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(*  For hash and on disk:

        |<- nbits/8 + 1 bytes ---->|
        |<- segment bits ->|10{0,7}|

        Longest sides: 255 * 8 - 1, since whole encoding size is limited
        to 255bytes by Hash.(^^).


    In Extender format on disk:

                         |<-- 1 Plebeia cell (32bytes) -->|
                         |<-27bytes->|
        |<- ncells * 32 + 27 bytes ->|<-8bits->|<-4bytes->|
        +----------------------------|---------+
        |<-- segment bits -->|10{0,7}|ncells|01|
                                     |6bits |

        The longest segment fits in one Plebeia cell: 27 * 8 - 1

    In memory for fast comparison:

        (nbits : int),

        |<- (nbits+7)/8 bytes --->|
        +-------------------------+
        |<- segment bits ->|0{0,7}|
*)

open Utils

let max_short_segment_length = 215 (*  = 27 * 8 - 1 *)

let max_length = 2039 (*  = 255 * 8 - 1 *)
(* The limit is determined by the Hash.(^^)
   The max len of the second arg of (^^) is 28+255 chars.
   The base hash is 28 chars.
   28+255 - 28 = 255 chars for a segment encoding.
   255 * 8 - 1 = 2039  (1 for the start bit)
*)

type side = Left | Right

let string_of_side = function
  | Left -> "L"
  | Right -> "R"

type segment =
  | List of side list
  | Cons of side * segment
  | Append of segment * segment
  | Encoding of int (* number of sides *) * string (* without tail *)
  | Slice of int (* head position *) * segment (* must be Encoding *)

type t = segment

module Normalization = struct
  (* In memory *)

  (* | off(bytes) <--- segment bits ---> ??....?| *)
  let decode_with_length len off s : side list =
    let sides_of_char bits c =
      let c = Char.code c in
      let f x = if c land x = 0 then Left else Right in
      let xs = [f 128; f 64; f 32; f 16; f 8; f 4; f 2; f 1] in
      if bits = 8 then xs else fst @@ List.split_at bits xs
    in
    let slen = String.length s - off in
    let l =
      List.concat
      @@ List.init slen (fun i ->
          let bits = if i = slen - 1 then len - i * 8 else 8 in
          assert (0 <= bits && bits <= 8);
          sides_of_char bits @@ String.unsafe_get s (off + i))
    in
    assert (List.length l = len);
    l

  (* <--- segment bits ---> 0{0,7} *)
  let encode sides =
    let len = List.length sides in
    let nbytes = (len + 7) / 8 in
    let tail_zero_bits = nbytes * 8 - len in
    assert (0 <= tail_zero_bits && tail_zero_bits <= 7);
    let bytes = Bytes.create nbytes in
    let rec make_byte = function
      | x1 :: x2 :: x3 :: x4 :: x5 :: x6 :: x7 :: x8 :: seg ->
          let bit = function
            | Left -> 0
            | Right -> 1
          in
          let byte =
            (bit x1) lsl 7
            + (bit x2) lsl 6
            + (bit x3) lsl 5
            + (bit x4) lsl 4
            + (bit x5) lsl 3
            + (bit x6) lsl 2
            + (bit x7) lsl 1
            + (bit x8) * 1
          in
          (Char.chr byte, seg)
      | seg ->
          make_byte (seg @ (List.init (8 - List.length seg) (fun _ -> Left)))
    in
    let rec fill_bytes byte_pos = function
      | [] ->
          assert (byte_pos = nbytes);
          Bytes.to_string bytes
      | seg ->
          let (c, seg) = make_byte seg in
          Bytes.unsafe_set bytes byte_pos c;
          let byte_pos' = byte_pos + 1 in
          assert (byte_pos' <= nbytes);
          fill_bytes byte_pos' seg
    in
    let s = fill_bytes 0 sides in
    (* assert (decode_with_length len 0 s = sides); *)
    s
end

let empty = List []

let of_sides l = List l

let rec is_empty = function
  | List [] -> true
  | List _ -> false
  | Cons _ -> false
  | Append (seg1, seg2) -> is_empty seg1 && is_empty seg2
  | Encoding (0, _) -> true
  | Encoding _ -> false
  | Slice (n, Encoding (len, _)) -> len = n
  | Slice _ -> assert false

let rec length = function
  | List ss -> List.length ss
  | Cons (_, seg) -> length seg + 1
  | Append (seg1, seg2) -> length seg1 + length seg2
  | Encoding (len, _) -> len
  | Slice (pos, Encoding (len, _)) -> len - pos
  | Slice _ -> assert false

let cons x xs = Cons (x, xs)

let append seg1 seg2 =
  if is_empty seg1 then seg2
  else if is_empty seg2 then seg1
  else Append (seg1, seg2)

let rec concat = function
  | [] -> List []
  | seg::segs -> append seg @@ concat segs

let rec to_sides seg =
  match seg with
  | List ss -> ss
  | Cons (s, seg) -> s :: to_sides seg
  | Append (seg1, seg2) -> to_sides seg1 @ to_sides seg2
  | Encoding (len, s) -> Normalization.decode_with_length len 0 s
  | Slice (off, Encoding (len, s)) ->
      List.drop (off mod 8)
      @@ Normalization.decode_with_length (len-(off/8*8)) (off/8) s
  | Slice _ -> assert false

let rec equal seg1 seg2 =
  seg1 == seg2 ||
  match seg1, seg2 with
  | List ss1, List ss2 -> ss1 = ss2
  | Cons (s1, seg1), Cons (s2, seg2) ->
      s1 = s2 && equal seg1 seg2
  | Encoding (len, s), Encoding (len', s') -> len = len' && s = s'
  | _ -> to_sides seg1 = to_sides seg2 (* XXX can be optimized *)

let rec compare seg1 seg2 =
  if seg1 == seg2 then 0
  else
    match seg1, seg2 with
    | Encoding (len, s), Encoding (len', s') when len = len' && s == s' -> 0
    | Encoding (size1, s1), Encoding (size2, s2) ->
        begin match Stdlib.compare s1 s2 with
          | 0 -> Stdlib.compare size1 size2
          | x -> x
        end
    | List ss1, List ss2 -> Stdlib.compare ss1 ss2
    | Cons (side1, seg1), Cons (side2, seg2) when side1 = side2 ->
       compare seg1 seg2
    | Cons (side1, _), Cons (side2, _) -> Stdlib.compare side1 side2
    | _ -> Stdlib.compare (to_sides seg1) (to_sides seg2)

let compare_list segs1 segs2 =
  match Stdlib.compare (List.length segs1) (List.length segs2) with
  | 0 ->
      let rec f segs1 segs2 = match segs1, segs2 with
        | [], [] -> 0
        | seg1::segs1, seg2::segs2 ->
            begin match compare seg1 seg2 with
            | 0 -> f segs1 segs2
            | n -> n
            end
        | _ -> assert false
      in
      f segs1 segs2
  | n -> n

let get_side s pos =
  let off = pos / 8 in
  let bit = pos mod 8 in
  let c = Char.code @@ String.unsafe_get s off in
  let f x = if c land x = 0 then Left else Right in
  match bit with
  | 0 -> f 128
  | 1 -> f 64
  | 2 -> f 32
  | 3 -> f 16
  | 4 -> f 8
  | 5 -> f 4
  | 6 -> f 2
  | 7 -> f 1
  | _ -> assert false

let rec cut = function
  | List [] -> None
  | List (x::xs) -> Some (x, List xs)
  | Cons (x,seg) -> Some (x, seg)
  | Append (seg1, seg2) ->
      begin match cut seg1 with
      | None -> cut seg2
      | Some (side, seg1) when is_empty seg1 -> Some (side, seg2)
      | Some (side, seg1) -> Some (side, Append (seg1, seg2))
      end
  | Encoding (0, _) -> None
  | Encoding (_len, s) as enc ->
      Some (get_side s 0, Slice (1, enc))
  | Slice (pos, (Encoding (len, s) as enc)) ->
      if pos = len then None
      else Some (get_side s pos,
                 let pos = pos + 1 in
                 if pos = len then List []
                 else Slice (pos, enc))
  | Slice _ -> assert false

let take n seg =
  let rec f rev_st n seg =
    if n = 0 then Some (List.rev rev_st, seg)
    else
      match cut seg with
      | None ->
          None
      | Some (s, seg) ->
          f (s :: rev_st) (n - 1) seg
  in
  f [] n seg

let normalize = function
  | Encoding _ as t -> t
  | t ->
      (* XXX Slice can be optimized *)
      let sides = to_sides t in
      let seg = Encoding (List.length sides, Normalization.encode sides) in
      (* assert (to_sides seg = sides); *)
      seg

let to_string s = String.concat "" (List.map string_of_side @@ to_sides s)
let string_of_sides ss = String.concat "" (List.map string_of_side ss)
let pp ppf s = Format.fprintf ppf "%s" (to_string s)

let string_of_segments segs =
  "["
  ^ String.concat
    "; "
    (List.map
       (fun seg ->
          if is_empty seg then "<empty>" else to_string seg)
       segs)
  ^ "]"

let pp_segments ppf segs = Format.fprintf ppf "%s" (string_of_segments segs)

let of_string s =
  let rec aux st = function
    | -1 -> Some (of_sides st)
    | n ->
        match String.unsafe_get s n with
        | 'L' -> aux (Left :: st) (n-1)
        | 'R' -> aux (Right :: st) (n-1)
        | _ -> None
  in
  aux [] @@ String.length s - 1

let common_prefix seg1 seg2 =
  let rec aux seg1 seg2 = match (cut seg1, cut seg2) with
    | (Some (h1, t1), Some (h2, t2)) ->
      if h1 = h2 then
        let (prefix, r1, r2) = aux t1 t2 in
        (h1 :: prefix, r1, r2)
      else
        ([], seg1, seg2)
    | (None, _) -> ([], empty, seg2)
    | (_, None) -> ([], seg1, empty)
  in
  let sides, seg2, seg3 = aux seg1 seg2 in
  of_sides sides, seg2, seg3

module Segs = struct
  (* growing segments at the end *)
  type t =
    { rev_segs : segment list
    ; bottom : side list (* reversed! *)
    }

  let empty = { rev_segs = []; bottom = [] } (* '/' *)
  let add_side t side = { t with bottom = side :: t.bottom }
  let append_seg t seg = { t with bottom = List.rev_append (to_sides seg) t.bottom }
  let append_sides t sides = { t with bottom = List.rev_append sides t.bottom }
  let append_rev_sides t rev_sides = { t with bottom = rev_sides @ t.bottom }
  let push_bud t =
    (* root bud never changes the segs *)
    if t.bottom = [] && t.rev_segs = [] then t
    else begin
      assert ( t.bottom <> [] );
      (* XXX Should encode ? *)
      { rev_segs = of_sides (List.rev t.bottom) :: t.rev_segs; bottom= [] }
    end
  let finalize t = List.rev (of_sides (List.rev t.bottom) :: t.rev_segs)

  let to_string t =
    let segs = finalize t in
    String.concat "/" (List.map to_string segs)

  let of_segments segs = match List.rev segs with
    | [] -> empty
    | bottom :: rev_segs -> { rev_segs; bottom= to_sides bottom }

  let last t = List.rev t.bottom
end

module Serialization = struct
  (* How to stored on disk *)

  let decode_slice_exn (s,off,len) =
    (*
       |<-----------  len ----------->|
       |xxxxxxxxxxxxxxxxxxx10..0|0...0|
       | 0  | ..         |  nz  |
                           ^
                           |
                           +------ last_one
    *)
    assert (String.length s >= off + len);
    let nz =
      let rec skip_last_zeros i =
        if i < 0 then assert false
        else
          let c = String.unsafe_get s (off+i) in
          if c = '\x00' then skip_last_zeros (i-1)
          else i
      in
      skip_last_zeros (len-1)
    in
    assert (nz >= 0);
    let ss = String.sub s off (nz+1) in
    let last_c = Char.code @@ String.unsafe_get ss nz in
    assert (last_c <> 0); (* XXX proper error? *)
    let last_one, last_byte =
      if last_c land 1 <> 0 then       7, last_c land 0b11111110
      else if last_c land 2 <> 0 then  6, last_c land 0b11111100
      else if last_c land 4 <> 0 then  5, last_c land 0b11111000
      else if last_c land 8 <> 0 then  4, last_c land 0b11110000
      else if last_c land 16 <> 0 then 3, last_c land 0b11100000
      else if last_c land 32 <> 0 then 2, last_c land 0b11000000
      else if last_c land 64 <> 0 then 1, last_c land 0b10000000
      else 0, 0
    in
    let seglen = nz * 8 + last_one in
    if last_one = 0 then
      Encoding (seglen, String.sub s off nz)
    else
      let ss = Bytes.of_string ss in (* XXX 2 copies unsafe is ok? *)
      let ss =
        Bytes.(unsafe_set ss nz @@ (Char.chr (Char.code (unsafe_get ss nz) land last_byte)));
        Bytes.to_string ss
      in
      Encoding (seglen, ss)

  let decode_exn s = decode_slice_exn (s,0,String.length s)

  let decode s = try Some (decode_exn s) with _ -> None

  let decode_slice slice = try Some (decode_slice_exn slice) with _ -> None

  let fix_tail len s =
    let last_char_pat = 128 lsr (len mod 8) in
    let s = Bytes.of_string s in (* String copy... *)
    let pos = Bytes.length s - 1 in
    Bytes.unsafe_set s pos
      (Char.chr
         (Char.code (Bytes.unsafe_get s pos)
            lor last_char_pat));
    Bytes.unsafe_to_string s

  let encode seg =
    let len, s = match normalize seg with
      | Encoding (len, s) -> len, s
      | _ -> assert false
    in
    let s =
      if len mod 8 = 0 then s ^ "\128"
      else fix_tail len s
    in
    if not @@ equal seg (decode_exn s) then
      Format.eprintf "??? %a %a %a@." pp seg pp (normalize seg) pp (decode_exn s);
    (* assert (equal seg (decode_exn s)); *)
    s

  (* Encoding of t list
     |l1 (1 byte)|encoding in l1 bytes|l2 (1 byte)|encoding in l2 bytes|...|0|
  *)

  let decode_list_slice (s,off) =
    let slen = String.length s in
    let rec f rev_segs i =
      if slen <= i then None (* overrun *)
      else
        let l = Char.code @@ String.unsafe_get s i in
        if l = 0 then Some (List.rev rev_segs, i+1)
        else
          match decode_slice_exn (s, (i+1), l) with
          | exception _ -> None
          | seg -> f (seg::rev_segs) (i+1+l)
    in
    f [] off

  let decode_list s =
    match decode_list_slice (s,0) with
    | Some (ss,off) when String.length s = off -> Some ss
    | _ -> None

  let encode_list ts =
    let s =
      (String.concat ""
       @@ List.concat_map (fun t ->
           let s = encode t in
           let len = String.length s in (* 1815+1 bits = 227 bytes < 256 *)
           assert (0 <= len && len < 256);
           [String.make 1 (Char.chr len); s]
         ) ts) ^ "\000" (* XXX inefficient string allocation *)
    in
    (*
      let ts' = from_Some @@ decode_list s in
      List.iter2 (fun s s' -> assert (equal s s')) ts ts';
    *)
    s

end

module StringEnc = struct
  (* Only for testing purpose *)

  let of_char c =
    let c = Char.code c in
    let bit n = if c land n = 0 then Left else Right in
    [ bit 128 ; bit 64 ; bit 32 ; bit 16 ; bit 8 ; bit 4 ; bit 2 ; bit 1]

  let encode s =
    let open Data_encoding in
    match Binary.to_bytes Data_encoding.Encoding.string s with
    | Error _ -> assert false
    | Ok b ->
        let of_binary_string s =
          let rec f st = function
            | -1 -> st
            | i ->
                let c = String.unsafe_get s i in
                f (of_char c @ st) (i-1)
          in
          f [] (String.length s - 1)
        in
        of_sides @@ of_binary_string (Bytes.to_string b)

  let decode seg =
    let sides = to_sides seg in
    let buf = Buffer.create 10 in
    let bit n = function
      | Left -> 0
      | Right -> n
    in
    let rec f = function
      | [] -> Some (Buffer.contents buf)
      | b7::b6::b5::b4::b3::b2::b1::b0::sides ->
          Buffer.add_char buf
            @@ Char.chr @@ bit 128 b7
                         + bit 64 b6
                         + bit 32 b5
                         + bit 16 b4
                         + bit 8 b3
                         + bit 4 b2
                         + bit 2 b1
                         + bit 1 b0;
            f sides
      | _ -> None
    in
    match f sides with
    | None -> None
    | Some s ->
        match Data_encoding.Binary.of_bytes Data_encoding.Encoding.string (Bytes.of_string s) with
        | Error _ -> None
        | Ok x -> Some x
end

let encoding =
  let open Data_encoding in
  conv
    (fun s -> Bytes.of_string @@ Serialization.encode s)
    (fun b -> Serialization.decode_exn @@ Bytes.to_string b) bytes
