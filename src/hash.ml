(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

module Prefix : sig
  (** { 1 The first half 224bit/28byte part of node hash. } *)

  (** { 2 The type } *)

  type t = private string
  (** Type for the hash prefix.  28 bytes *)

  val zero : t
  (** 28 bytes of 0's *)

  (** { 2 Printer and parser } *)

  external to_string : t -> string = "%identity"
  (** Returns a binary string representation of hash *)

  val of_string : string -> t
  (** Create a hash from 28 bytes binary string.
      String with an invalid length raises [Assert_failure] *)

  val to_hex : t -> Hex.t
  val to_hex_string : t -> string

  val of_hex : Hex.t -> t
  (** Create a hash from a hexiadecimal representation of 28 bytes binary string.
      Input with an invalid length raises [Assert_failure] *)

  val encoding : t Data_encoding.t
  (** Data encoding *)

  (** { 2 Hash computation } *)

  val bytes_hash_list : string list -> bytes
  (** Compute the hash of the concatenation of the given binary strings
      in bytes.
  *)

  val test : unit -> unit
  (** check the correctness of blake2b *)
end = struct
  (** The prefix of hash, fixed width 224 bits *)

  module Blake2B_28 = Blake2B.Make(struct let bytes = 28 end)

  let test () =
    assert (Hex.of_string @@ Blake2B_28.of_string "Replacing SHA1 with the more secure function"
            = `Hex "6bceca710717901183f66a78a7a9f59441c56defcff32f33f1e1b578");
    assert (Hex.of_string @@ Blake2B_28.of_strings ["Replacing SHA1 with the "; "more secure function" ]
            = `Hex "6bceca710717901183f66a78a7a9f59441c56defcff32f33f1e1b578")
  (* obtained by
  #!/usr/local/bin/python3

  from hashlib import blake2b

  h = blake2b(digest_size=28)
  h.update(b'Replacing SHA1 with the more secure function')
  print (h.hexdigest())
  *)

  let bytes_hash_list = Blake2B_28.bytes_of_strings

  type t = string

  external to_string : t -> string = "%identity"

  let of_string x = assert (String.length x = 28); x
  (* This module is only for internal use.
     Error recovery is not required. *)

  let to_hex x = Hex.of_string x
  let to_hex_string x = Hex.show @@ Hex.of_string x
  let of_hex h = of_string @@ Hex.to_string h

  let zero = String.make 28 '\000'

  let encoding = Data_encoding.(conv Bytes.of_string Bytes.to_string bytes)
end

type t = Prefix.t * string

let encoding : t Data_encoding.t =
  let open Data_encoding in
  let postfix = conv Bytes.of_string Bytes.to_string bytes in
  tup2 Prefix.encoding postfix

let to_hex_string t =
  let to_string (s, s') = Prefix.to_string s ^ s' in
  let `Hex s = Hex.of_string @@ to_string t in s

let of_prefix h = h, ""
let of_hash_prefix = of_prefix

let prefix : t -> Prefix.t = fst

let is_long = function
  | _, "" -> false
  | _ -> true

let to_strings = function
  | s, "" -> [Prefix.to_string s]
  | s, s' -> [Prefix.to_string s; s']

let make_h ~set_last_2bits n ss =
  let bs = Prefix.bytes_hash_list ( String.make 1 (Char.chr n) :: ss ) in
  begin match set_last_2bits with
  | None -> ()
  | Some bits ->
      Bytes.unsafe_set bs 27
        (Char.chr @@ Char.code (Bytes.unsafe_get bs 27) land 0xfc lor bits)
  end;
  Prefix.of_string @@ Bytes.unsafe_to_string bs

let (^^) nh1 nh2 =
  let s1 = to_strings nh1 in
  let s2 = to_strings nh2 in
  let l2 = match nh2 with _, s -> String.length s in
  assert (0 <= l2 && l2 < 256); (* This must be always true *)
  let c = Char.chr l2 in
  s1 @ s2 @ [ String.make 1 c ]

let of_bud = function
  | None -> (Prefix.zero, "")
  | Some hash -> (make_h ~set_last_2bits:(Some 0b11) 2 @@ to_strings hash, "")

let of_internal l r = (make_h ~set_last_2bits:(Some 0b00) 1 (l ^^ r), "")

let of_leaf v = (make_h ~set_last_2bits:None 0 [ Value.to_string v ], "")

let of_extender seg (h_fst, h_snd) =
  assert (h_snd = "");
  (h_fst, Segment.Serialization.encode seg )
