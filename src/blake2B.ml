module Make(A : sig
    val bytes : int
  end) : sig
  val of_string : string -> string
  val of_strings : string list -> string
  val bytes_of_strings : string list -> bytes
end = struct
  open Hacl_star

  let () =
    if A.bytes < 1 || A.bytes > 64 then
      invalid_arg "Blake2B.Make: bytes must be between 1 and 64"

  let key = Bytes.create 0

  let of_bytes inbuf =
    let outbuf = Bytes.create A.bytes in
    (* HACL* doesn't yet provide a multiplexing interface for Blake2b so we
     * perform this check here and use the faster version if possible *)
    if AutoConfig2.(has_feature AVX2) then
      Hacl.Blake2b_256.hash key inbuf outbuf
    else Hacl.Blake2b_32.hash key inbuf outbuf ;
    outbuf

  let of_string inbuf =
    Bytes.unsafe_to_string @@ of_bytes @@ Bytes.unsafe_of_string inbuf

  let of_strings ss = of_string (String.concat "" ss)

  let bytes_of_strings ss =
    of_bytes @@ Bytes.unsafe_of_string @@ String.concat "" ss
end
