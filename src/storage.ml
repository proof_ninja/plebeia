(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

open Utils

module C = Xcstruct

include Gstorage.Make(struct
    (* XXX Strange header string *)
    let head_string = "PLEBEIA " ^ String.make (24 - String.length "PLEBEIA ") '\000'
    let version = Version.version
    let bytes_per_cell = 32
    let max_index = Index.(max_int - Unsafe.of_int 256)
  end)

module Chunk = struct

  (* Store data bigger than 32 bytes *)

  (* 6, not 8!  but changing it incurs incompatibility *)
  let ncells size = (size + 8 + 31) / 32

  let get_footer_fields storage last_index =
    let buf = get_cell storage last_index in
    let cdr = Index.of_uint32 @@ C.get_uint32 buf 28 in
    let size = C.get_uint16 buf 26 in
    (cdr, size)

  let get_chunk storage last_index =
    let cdr, size = get_footer_fields storage last_index in
    let ncells = ncells size in
    let first_index = Index.(last_index - Unsafe.of_int ncells + one) in
    (get_bytes storage first_index size, size, cdr)

  let get_chunks storage last_index =
    let rec aux (bufs, size) last_index =
      let buf, bytes, cdr = get_chunk storage last_index in
      let bufs = buf :: bufs in
      let size = size + bytes in (* overflow in 32bit? *)
      if cdr = Index.zero then (bufs, size)
      else aux (bufs, size) cdr
    in
    aux ([], 0) last_index

  let string_of_cstructs bufs =
    String.concat "" @@ List.map C.to_string bufs

  let read t i = string_of_cstructs @@ fst @@ get_chunks t i

  let write_to_chunk storage cdr s off len =
    if mode storage = Reader then invalid_arg "Reader cannot write";
    assert (String.length s >= off + len);
    let ncells = ncells len in
    let cdr_pos = ncells * 32 - 4 in
    let size_pos = cdr_pos - 2 in

    let i = new_indices storage ncells in
    let last_index = Index.(i + Unsafe.of_int ncells - one) in
    let chunk = get_bytes storage i (32 * ncells) in

    C.blit_from_string s off chunk 0 len;
    C.set_uint16 chunk size_pos len;
    C.set_uint32 chunk cdr_pos (Index.to_uint32 cdr);
    last_index

  let write storage ?(max_cells_per_chunk=1000) s =
    if mode storage = Reader then invalid_arg "Reader cannot write";
    let max_bytes_per_chunk = 32 * max_cells_per_chunk - 6 in
    let rec f off remain cdr  =
      let len = if remain > max_bytes_per_chunk then max_bytes_per_chunk else remain in
      let cdr' = write_to_chunk storage cdr s off len in
      let off' = off + len in
      let remain' = remain - len in
      if remain' > 0 then f off' remain' cdr'
      else cdr'
    in
    f 0 (String.length s) Index.zero

  let test_write_read st storage =
    let max_cells_per_chunk = Random.State.int st 246 + 10 in
    let size = Random.State.int st (max_cells_per_chunk * 32) + 32 in
    let s = String.init size @@ fun i -> Char.chr (Char.code 'A' + i mod 20) in
    let i = write storage ~max_cells_per_chunk s in
    let s' = string_of_cstructs @@ fst @@ get_chunks storage i in
    assert (s = s')
end
