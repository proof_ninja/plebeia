(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)
include Debug
include Utils

module RS = Random.State

let tempfile = Filename.temp_file "plebeia" ".context"

let with_tempdir f =
  (* temp_file creats a file! *)
  let d = Filename.temp_file "plebeia_test" ".dir" in
  Format.eprintf "Using tempdir %s@." d;
  Unix.unlink d;
  Unix.mkdir d 0o700;
  f d

let test_with_context length f =
  if Sys.file_exists tempfile then Sys.remove tempfile;
  let context = Context.create ~length tempfile in
  let res = f context in
  Context.close context;
  res

let with_context = test_with_context

let with_vc n f =
  Format.eprintf "Using VC %s@." n;
  f @@ Vc.create' n

let test_with_cursor f =
  test_with_context 1000000 (fun context ->
    let cursor = Cursor.empty context in
    f cursor)

let with_cursor = test_with_cursor

let path_of_string s = from_Some @@ Segment.of_string s

let ok_or_fail = function
  | Ok x -> x
  | Error s -> Error.raise s

let must_fail = function
  | Ok _ -> failwith "must fail"
  | Error _ -> ()

let path = path_of_string
let value = Value.of_string

open Node

(* only for test *)
let rec normalize n = match n with
  | Disk _ -> n
  | View v -> View (normalize_view v)

and normalize_view v = match v with
  | Internal (n1, n2, i, h) -> _Internal (normalize n1, normalize n2, i, h)
  | Bud (None, _, _) -> v
  | Bud (Some n, i, h) -> _Bud (Some (normalize n), i, h)
  | Leaf _ -> v
  | Extender (seg, n, i, h) ->
      (* XXX should have Segment.normalize *)
      _Extender (Segment.(of_sides @@ to_sides seg), n, i, h)

let equal_nodes n1 n2 = normalize n1 = normalize n2

(* ls . *)
let all_children context node =
  let rec aux store = function
    | [] -> store
    | (segs_rev, node) :: tail ->
       match Node.view context node with
       | Leaf _ ->
          let segment = List.rev segs_rev |> Segment.concat in
          aux ((segment, `File) :: store) tail
       | Bud (Some child, _, _) ->
          let segment = List.rev segs_rev |> Segment.concat in
          aux ((segment, `Directory child) :: store) tail
       | Bud (None, _, _) -> aux store tail
       | Extender (seg, node', _, _) ->
          aux store ((seg::segs_rev, node') :: tail)
       | Internal (l, r, _, _) ->
          aux store (
              (Segment.of_sides [Segment.Left] :: segs_rev, l)
              :: (Segment.of_sides [Segment.Right] :: segs_rev, r)
              :: tail)
  in
  aux [] [([], node)]

let xassert b =
  let open Printexc in
  if not b then begin
    prerr_endline ("*****************************************************\n" ^ raw_backtrace_to_string (get_callstack 10));
    assert false
  end

let reraise_after f x e =
  try
    f x
  with
    exn -> e exn; raise exn

let with_random f =
  let seed =
    let default () =
      let st_seed = RS.make_self_init() in
      RS.int st_seed @@ (1 lsl 30) - 1
    in
    match Sys.getenv "PLEBEIA_TEST_SEED" with
    | exception Not_found -> default ()
    | s ->
        try
          let i = int_of_string s in
          Format.eprintf "PLEBEIA_TEST_SEED=%d@." i;
          i
        with _ -> default ()
  in
  let st = RS.make [| seed |] in
  reraise_after f st @@ fun _exn ->
    Format.eprintf "Failed with seed: Random.State.make [| %d |]@." seed

(* XXX This STILL may break the invaraint *)
let forget_random_nodes rs prob n =
  let rec f n = match n with
    | Disk _ -> n
    | View v ->
        if RS.float rs 1.0 < prob then
          match Node.may_forget n with
          | None -> n
          | Some n -> n
        else
          match v with
          | Leaf _ -> n
          | Bud (None, _, _) -> n
          | Bud (Some n', i, h) ->
              let n' = f n' in
              begin match _Bud (Some n', i, h) with
                | exception _ -> n
                | v -> View v
                end
          | Extender (seg, n', i, h) ->
              let n' = f n' in
              begin match _Extender (seg, n', i, h) with
                | exception _ -> n
                | v -> View v
                end
          | Internal (nl, nr, i, h) ->
              let nl' = f nl in
              let nr' = f nr in
              begin match _Internal (nl', nr', i, h) with
                | exception _ -> n
                | v -> View v
              end
  in
  f n

let leaves context node : Segment.t list list =
  let rec aux store = function
    | [] -> store
    | (pathes_rev, node) :: tail ->
       all_children context node
       |> List.map (function
              | (seg, `File) -> List.rev (seg :: pathes_rev) :: store
              | (seg, `Directory child) when Segment.is_empty seg ->
                 aux store ((pathes_rev, child) :: tail)
              | (seg, `Directory child) ->
                 aux store ((seg :: pathes_rev, child) :: tail))
       |> List.concat
  in
  aux [] [([], node)]

let choose_random_node st ctxt n0 =
  let open Segment in
  let rec f segs n =
    let v = Node.view ctxt n in
    if RS.int st 20 = 0 then segs
    else
      match v with
      | Leaf _ | Bud (None, _, _) -> f Segs.empty n0
      | Bud (Some n, _, _) -> f (Segs.push_bud segs) n
      | Internal (n1, n2, _, _) ->
          if RS.bool st then f (Segs.add_side segs Left) n1
          else f (Segs.add_side segs Right) n2
      | Extender (seg, n, _, _) ->
          f (Segs.append_seg segs seg) n
  in
  f Segs.empty n0

let make_resemble st ctxt n =
  let c =
    match Node_tools.count_nodes ~upto:1000 n with
    | `EQ i | `GE i -> i
  in
  let open Segment in

  (* remove some nodes *)
  let rec rm i n =
    if i = 0 then n
    else
      let (`EQ c | `GE c) = Node_tools.count_nodes ~upto:1000 n in
      let segs = choose_random_node st ctxt n in
      let segs = Segs.finalize segs in
      if segs = [] then rm (i-1) n
      else
        let cur = Cursor.(_Cursor (_Top, n, ctxt, Info.empty)) in
        match Deep.delete' cur segs  with
        | Ok Cursor (_, n', _, _) ->
            let (`EQ c' | `GE c') = Node_tools.count_nodes ~upto:1000 n' in
            if c' * 2 < c then rm (i-1) n (* we do not remove too much *)
            else rm (i-1) n'
        | Error _ -> rm (i-1) n
  in
  let n = rm (if c <= 1000 then 2 else 4) n in

(*
  let (`EQ c' | `GE c') = Node_tools.count_nodes ~upto:1000 n in
  Format.eprintf "Resemble del: %d => %d@." c c';
*)

  let (`EQ c | `GE c) = Node_tools.count_nodes ~upto:1000 n in

  (* replace some nodes *)
  let rec add i n =
    if i = 0 then n
    else
      (* XXX segment selection is not really good *)
      let segs = choose_random_node st ctxt n in
      let segs = Segs.append_seg segs @@ Gen.(segment (int 5)) st in
      let m =
        let open Gen in
        choice
          [leaf; bud 8; internal 8; extender 8]
          st
      in
      let segs = Segs.finalize segs in
      match
        Deep.alter Cursor.(_Cursor (_Top, n, ctxt, Info.empty)) segs @@ function
        | Some _ -> Ok m
        | None ->
            (* This is possible.  If [segs] can point an empty bud,
               alter detects None there.
            *)
            Ok m
      with
      | exception _ ->
          (* This is arbitrary update therefore may break the invariant *)
          add (i-1) n
      | Ok (Cursor (_, n', _, _)) ->
          let (`EQ c' | `GE c') = Node_tools.count_nodes ~upto:1000 n' in
          if c' < c then add (i-1) n (* not to make the tree too small *)
          else add (i-1) n'
      | Error _ -> add (i-1) n
  in
  let n = add 32 n in

(*
  let (`EQ c' | `GE c') = Node_tools.count_nodes ~upto:1000 n in
  Format.eprintf "Resemble: => %d@." c';
*)

  n
