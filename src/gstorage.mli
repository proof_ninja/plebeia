(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2019,2020 DaiLambda, Inc. <contact@dailambda.jp>            *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** { 2 Types } *)
type mode = Private | Reader | Writer

module Make
    (P : sig
       val head_string : string
       (** 24byte length identifier *)

       val version : int
       (** version *)

       val bytes_per_cell : int
       (** bytes per cell, ex.  32, 4069 *)

       val max_index : Index.t
       (** The maximal index number usable *)
     end) : sig

  type nonrec mode = mode = Private | Reader | Writer

  val bytes_per_cell : int
  val max_index : Index.t

  val start_index : Index.t
  (** From which Index.t data is stored *)

  type storage
  type t = storage
  (** The type *)

  (** { 2 Open, close, and commit } *)

  val create :
    ?pos:int64
    -> ?length:int
    -> ?resize_step: Index.t
    -> ?version: int
    -> string
    -> t
  (** Create a new storage

      * pos : Where to start the storage in the file, in bytes.
      * length: The initial size of the storage, excluding [pos].
      * resize_step: How many cells allocated for each resize
      * version: Default is Verison.version
      * string : The path name
  *)

  val open_ :
    ?pos:int64
    -> ?resize_step:Index.t
    -> ?ignore_version: bool
    -> mode:mode
    -> string
    -> t
  (** Open an existing storage *)

  val reopen : t -> unit

  val truncate : ?length: int -> t -> unit
  (** Truncate the data file and reinitialize it.  All the contents are lost.
      [length] is the initial reserved size of the reinitialized file.
  *)

  val sync : t -> unit
  (** For reader to update the storage to catch up the update by the writer
      process.  For Writer and Private, it does nothing. *)

  val close : t -> unit
  (** Close the storage *)

  val commit : t -> unit
  (** Write the current state of the storage at its header.
      If the system crashes, any updates to the storage after
      the last commit will be lost, even if they are written to
      the file.
  *)

  (** { 2 Accessor } *)

  val filename : t -> string
  (** Return the file name *)

  val mode : t -> mode
  (** Return the opening mode *)

  val get_last_root_index  : t -> Index.t option
  val get_current_length   : t -> Index.t
  (** Get the status of the storage

      For Reader, it only returns the lastest information it knows in memory.
      Writer may already update this information on the disk.
  *)

  val size : t -> Int64.t
  (** Used cells in bytes *)

  val version : t -> int
  (** Storage version*)

  val override_version : t -> int -> unit
  (** Override the header version of the file *)

  val set_last_root_index  : t -> Index.t option -> unit
  (** Set the last index of root hash.
      Note that the data are only saved to the file when [Header.commit]
      is called.
  *)

  (** { 2 Read and write } *)

  val set_accessed_indices : Index.t list ref option -> unit
  (** For stat. If [Some xsref] is given, accessed indices by
      [get_bytes] and [get_cell*] will be recorded into [xsref].
  *)

  val get_cell : t -> Index.t -> Cstruct.t
  (** Get the content of the cell specified by the index *)

  val get_cell2 : t -> Index.t -> Cstruct.t
  (** make a 64 bytes writable buffer from the beginning of
      the cell of the given index
  *)

  val get_bytes : t -> Index.t -> int -> Cstruct.t
  (** Get the contiguous bytes from the head of the index *)

  val new_index : t -> Index.t
  (** Allocate a cell and returns its index *)

  val new_indices : t -> int -> Index.t
  (** Allocate cells and return the first index *)
end
