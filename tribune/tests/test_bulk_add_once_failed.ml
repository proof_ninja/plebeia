(* Bug failed this test.
   Fixed by commit 53096c79f2f20b61eccecec7b2b5c054dc8f4e4e
*)

open Plebeia
open Plebeia_tribune.Persist_plus
open Test_utils

module RS = Random.State

let with_storage f =
  with_tempdir @@ fun d ->
  let s = TStorage.create (d ^/ "tribune.context") in
  f s

let (!!) n = Index.Unsafe.of_int n

let test_bulk_adds () = with_storage @@ fun s ->
  let module P = Make(struct let storage = s let max_size_in_bytes= 64 let check= true end) in
  let count t = List.length @@ P.elements t in
  let mem t (k, v) = (P.find k t = Some v) in
  let equiv t1 t2 =
    (count t1 = count t2)
    && List.for_all (mem t2) (P.elements t1)
  in
  let check_adds kvs t =
    let t1 = P.adds kvs t in
    let t2 = P.bulk_adds kvs t in
    assert (equiv t1 t2)
  in
  let rec enum_all_segs len =
    if len <= 0 then [Segment.empty]
    else
      let segs = enum_all_segs (len - 1) in
      List.map (Segment.cons Segment.Left) segs @
      List.map (Segment.cons Segment.Right) segs
  in
  let kvs = List.mapi (fun i k -> (k, !!i)) @@ enum_all_segs 8 in
  let t_init = P.init kvs in
  let init_len = List.length kvs in

  let case i len =
    let middle = fst @@ List.split_at len @@ snd @@ List.split_at i kvs in
    let t = P.rebalance (P.deletes (List.map fst middle) t_init) in
    check_adds middle t
  in
  for i = 0 to init_len do
    for len = 0 to init_len - i do
      Printf.printf "case i =%d len =%d\n" i len;
      case i len
    done
  done

let () =
  let open Alcotest in
  run "failing"
    [  "bulk_adds", ["bulk adds", `Quick, test_bulk_adds] ]
