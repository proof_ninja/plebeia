open Plebeia
open Plebeia_tribune.Persist
open Test_utils

let with_storage f =
  with_tempdir @@ fun d ->
  let s = TStorage.create (d ^/ "tribune.context") in
  f s

let (!) s = from_Some @@ Segment.of_string s
let (!!) n = Index.Unsafe.of_int n

let test_delete () = with_storage @@ fun s ->
  let module P = Make(struct let storage = s let max_size_in_bytes= 256 end) in
  let t = P.adds [!"L", !!1] P.empty in
  prerr_endline "Added L";
  let t = P.deletes [!"L"] t in
  prerr_endline "Deleted L";
  if P.is_empty t then Format.eprintf "t= %a@." P.pp t;
  assert (P.is_empty t);

  let t = P.adds [!"L", !!1; !"R", !!1] P.empty in
  prerr_endline "Added L and R";
  let t = P.deletes [!"L"] t in
  prerr_endline "Deleted L";
  assert (P.elements t = [!"R", !!1])


let test_delete2 () = with_storage @@ fun s ->
  let module P = Make(struct let storage = s let max_size_in_bytes= 64 end) in
  let t = P.adds [!"L", !!1] P.empty in
  prerr_endline "Added L";
  let t = P.deletes [!"L"] t in
  prerr_endline "Deleted L";
  if P.is_empty t then Format.eprintf "t= %a@." P.pp t;
  assert (P.is_empty t);

  let t = P.adds [!"L", !!1; !"R", !!1] P.empty in
  prerr_endline "Added L and R";
  let t = P.deletes [!"L"] t in
  prerr_endline "Deleted L";
  assert (P.elements t = [!"R", !!1])

let assert_equal ?pp x1 x2 =
  if x1 <> x2 then
    begin (match pp with
    | Some pp ->
       Format.eprintf "expected: %a but got %a" pp x1 pp x2;
    | None ->
       ());
          assert false
    end

let pp_kv ppf (k,v)  =
  Format.fprintf ppf "(%a -> %a)"
  Segment.pp k Index.pp v

let test_bulk_adds () = with_storage @@ fun s ->
  let module P = Make(struct let storage = s let max_size_in_bytes= 64 end) in
  let assert_equiv t1 t2 =
    let elems1, elems2 = P.elements t1, P.elements t2 in
    assert_equal ~pp:Format.(pp_print_list pp_kv) elems1 elems2
  in

  let check_adds kvs t =
    let t1 = P.adds kvs t in
    let t2 = P.bulk_adds kvs t in
    assert_equiv t1 t2
  in
  check_adds [!"L", !!1] P.empty;
  let t = P.adds [!"L", !!1] P.empty in
  check_adds [!"LLR", !!10] t;

  check_adds [!"L", !!1; !"R", !!1] P.empty;
  let t = P.adds [!"L", !!1; !"R", !!1] P.empty in
  check_adds [!"LLR", !!10 ] t;
  ()

(*

let test_random_add nkvs rng =
  let added = Hashtbl.create 1000 in
  let add n =
    let kseg = Gen.(segment (int_range (1,200))) rng in
    let v = Gen.index rng in
    Hashtbl.replace added kseg v;
    P.add kseg v n
  in
  let rec f n = function
    | 0 -> n
    | i -> f (add n) (i-1)
  in
  let n = f P.empty nkvs in
  Format.eprintf "Added %d@." nkvs;
  Format.eprintf "Checking %d@." (Hashtbl.length added);
  Hashtbl.iter (fun kseg v ->
      match P.find kseg n with
      | Some v' -> assert (v = v')
      | None -> assert false) added;
  Format.eprintf "Checked %d@." (Hashtbl.length added);
  P.bench n;
  added, n

let test_random_delete kvs n =
  prerr_endline "Deleting";
  let n = Hashtbl.fold (fun kseg _v n ->
      begin match P.find kseg n with
        | Some _ -> ()
        | _ -> assert false
      end;
      let n = P.delete kseg n in
      match P.find kseg n with
      | Some _ -> assert false
      | None -> n) kvs n
  in
  assert (P.is_empty n)

let test_random_ls i rng kvs n =
  for _i = 1 to i do
    let kseg = Gen.(segment (int_range (1,200))) rng in
    let bits = Gen.int_range (1, 32) rng in
    let res = P.ls kseg bits n in
    let res' =
      let ksides = Segment.to_sides kseg in
      let nkseg = List.length ksides in
      List.sort_uniq compare @@ Hashtbl.fold (fun seg _ ss ->
          let sides = Segment.to_sides seg in
          let pre, post = List.split_at nkseg sides in
          if pre = ksides then
            let post, _ = List.split_at bits post in
            if List.length post = bits then post :: ss
            else ss
          else ss) kvs []
    in
    if res <> res' then begin
      Format.eprintf "search %s %d@." (Segment.to_string kseg) bits;
      Format.eprintf "res@.";
      List.iter (fun sides -> prerr_endline (Segment.string_of_sides sides)) res;
      Format.eprintf "res'@.";
      List.iter (fun sides -> prerr_endline (Segment.string_of_sides sides)) res';
    end;
    assert (res = res');
  done

let test_random_find_under i rng kvs n =
  for _i = 1 to i do
    let kseg = Gen.(segment (int_range (1,200))) rng in
    let res = P.find_under kseg n in
    let res' =
      let ksides = Segment.to_sides kseg in
      let nkseg = List.length ksides in
      List.sort_uniq compare @@ Hashtbl.fold (fun seg v ss ->
          let sides = Segment.to_sides seg in
          let pre, post = List.split_at nkseg sides in
          if pre = ksides then (post,v) :: ss else ss) kvs []
    in
    if res <> res' then begin
      Format.eprintf "find %s@." (Segment.to_string kseg);
      Format.eprintf "res@.";
      List.iter (fun (sides,_) -> prerr_endline (Segment.string_of_sides sides)) res;
      Format.eprintf "res'@.";
      List.iter (fun (sides,_) -> prerr_endline (Segment.string_of_sides sides)) res';
    end;
    assert (res = res');
  done

let test_random_copy i rng n =
  let rec f = function
    | 0 -> ()
    | i ->
        let src = Gen.(segment (int_range (1,200))) rng in
        let dst = Gen.(segment (int_range (1,200))) rng in
        if P.find_under dst n <> [] then f i
        else
          let src_finds = P.find_under src n in
          let n = P.copy src dst n in
          let dst_finds = P.find_under dst n in
          assert (src_finds = dst_finds);
          f (i-1)
  in
  f i

let test_random_rm i rng n =
  let rec f = function
    | 0 -> ()
    | i ->
        let src = Gen.(segment (int_range (1,200))) rng in
        let n = P.rm src n in
        let finds = P.find_under src n in
        assert (finds = []);
        f (i-1)
  in
  f i

let test_random () =
  with_random @@ fun rng ->
  let kvs, n = test_random_add 10000 rng in
  (* the following tests use the result of test_random_add *)
  test_random_delete kvs n;
  test_random_ls 10000 rng kvs n;
  test_random_find_under 100 rng kvs n;
  test_random_copy 100 rng n;
  test_random_rm 100 rng n
*)

let () =
  let open Alcotest in
  run "test_persist"
    [ "test_persist",
      [ "delete", `Quick, test_delete
      ; "delete2", `Quick, test_delete2
      ; "builk_adds", `Quick, test_bulk_adds
      ]
    ]
