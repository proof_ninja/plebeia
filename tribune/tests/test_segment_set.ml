open Plebeia_tribune
open Plebeia
open Test_utils
module SS = Set.Make (Segment)
module S = Segment_set

let ( ! ) s = from_Some @@ Segment.of_string s

let rec repeat n f x = if n <= 0 then x else repeat (n - 1) f (f x)

let seq_next seq =
  match seq () with Seq.Nil -> None | Seq.Cons (hd, tl) -> Some (hd, tl)

let seq_rev_take n seq =
  let rec take n rev seq =
    if n <= 0 then rev
    else
      match seq () with
      | Seq.Nil -> rev
      | Seq.Cons (x, seq) -> take (n - 1) (x :: rev) seq
  in
  take n [] seq

let enum_segs n =
  let rec f n =
    if n <= 0 then [ [] ]
    else
      let ss = f (n - 1) in
      List.map (List.cons Segment.Left) ss
      @ List.map (List.cons Segment.Right) ss
  in
  List.map Segment.of_sides @@ f n

(* infinite random sequence of segs, with long prefixes *)
let rec seg_seq rng rev_prefix : Segment.t Seq.t =
 fun () ->
  let seg = Gen.(segment (int_range (0, 20))) rng in
  let seg = Segment.append (Segment.of_sides (List.rev rev_prefix)) seg in
  let rev_prefix = List.drop (Gen.int_range (0, 10) rng) rev_prefix in
  let rev_prefix =
    if List.length rev_prefix >= 30 then rev_prefix
    else
      let prefix' = Gen.(segment (int_range (0, 10))) rng in
      List.append (Segment.to_sides prefix') rev_prefix
  in
  Seq.Cons (seg, seg_seq rng rev_prefix)

let random_segs n rng =
  let rev_prefix = Gen.(segment (int_range (10, 20))) rng in
  seq_rev_take n @@ seg_seq rng @@ Segment.to_sides rev_prefix

let of_s str = Stdlib.Option.get (Segment.of_string str)

let of_ss = List.fold_left (fun s k -> S.add (of_s k) s) S.empty

module Alcotest = struct
  include Alcotest

  let seg = Alcotest.testable Segment.pp Segment.equal

  let segs = list seg

  let check_segs name strs s =
    check segs name (List.map of_s strs) (S.sorted_list s)
end

let check_consistent s ss =
  S.iter
    (fun k ->
      Alcotest.(check bool)
        (Format.asprintf "Sound %a" Segment.pp k)
        true (SS.mem k ss))
    s;
  let n =
    SS.fold
      (fun k i ->
        Alcotest.(check (option int))
          (Format.asprintf "Complete %a" Segment.pp k)
          (Some i) (S.find k s);
        i + 1)
      ss 0
  in
  Alcotest.(check int) "size" n (S.length s);
  ()

type pair = S.t * SS.t

let pair_empty = (S.empty, SS.empty)

let pair_add k (s, ss) = (S.add k s, SS.add k ss)

let pair_delete k (s, ss) = (S.delete k s, SS.remove k ss)

let pair_union (s1, ss1) (s2, ss2) = (S.union s1 s2, SS.union ss1 ss2)

let pair_check_consistent (s, ss) = check_consistent s ss

let random_add rng p =
  let seg = Gen.(segment (int_range (1, 200))) rng in
  pair_add seg p

let random_delete rng p =
  let seg = Gen.(segment (int_range (1, 200))) rng in
  pair_delete seg p

let random_delete_existing rng p =
  let keys =
    List.of_seq
    @@ Seq.filter (fun _ -> RS.float rng 1.0 < 0.1)
    @@ SS.to_seq (snd p)
  in
  List.fold_left (fun p k -> pair_delete k p) p keys

let test_empty () =
  Alcotest.(check int) "length of empty" 0 (S.length S.empty);
  Alcotest.check_segs "length of empty" [] S.empty;
  ()

let test_add () =
  let s = S.add (of_s "LLL") S.empty in
  Alcotest.(check int) "length" 1 (S.length s);
  Alcotest.check_segs "list of empty" [ "LLL" ] s;
  let s = S.add (of_s "L") s in
  Alcotest.(check int) "length" 2 (S.length s);
  Alcotest.check_segs "list" [ "L"; "LLL" ] s;
  let s = S.add (of_s "LRL") s in
  Alcotest.check_segs "list" [ "L"; "LLL"; "LRL" ] s;
  let s = S.add (of_s "LRLLLL") s in
  Alcotest.check_segs "list" [ "L"; "LLL"; "LRL"; "LRLLLL" ] s;
  let s = S.add (of_s "LRLLLR") s in
  Alcotest.check_segs "list" [ "L"; "LLL"; "LRL"; "LRLLLL"; "LRLLLR" ] s;
  ()

let test_of_seq () =
  with_random @@ fun rng ->
  let segs = random_segs 1000 rng in
  let s = List.fold_left (fun s k -> S.add k s) S.empty segs in
  let s' = S.of_seq @@ List.to_seq segs in
  Alcotest.(check segs) "of_seq" (S.sorted_list s) (S.sorted_list s')

let test_delete () =
  Alcotest.check_segs "empty" [] (S.delete !"LRL" S.empty);
  let single = of_ss [ "LRL" ] in
  Alcotest.check_segs "single" [] (S.delete !"LRL" single);
  Alcotest.check_segs "single" [ "LRL" ] (S.delete !"L" single);
  Alcotest.check_segs "single" [ "LRL" ] (S.delete !"LRLL" single);

  let ext = of_ss [ "LR"; "LRRL" ] in
  Alcotest.check_segs "ext" [ "LR" ] (S.delete !"LRRL" ext);
  Alcotest.check_segs "ext" [ "LRRL" ] (S.delete !"LR" ext);
  Alcotest.check_segs "ext" [ "LR"; "LRRL" ] (S.delete !"LRL" ext);

  let internal = of_ss [ "L"; "LL"; "R" ] in
  Alcotest.check_segs "bin" [ "LL"; "R" ] (S.delete !"L" internal);
  Alcotest.check_segs "bin" [ "L"; "R" ] (S.delete !"LL" internal);
  Alcotest.check_segs "bin" [ "L"; "LL" ] (S.delete !"R" internal);

  let s = of_ss [ ""; "L"; "LLLLR"; "LLLR"; "LLR"; "LLRLL"; "LLRR" ] in
  Alcotest.check_segs "list0"
    [ ""; "L"; "LLLLR"; "LLLR"; "LLR"; "LLRLL"; "LLRR" ]
    s;
  let s = S.delete (of_s "LLRLR") s in
  Alcotest.check_segs "list1"
    [ ""; "L"; "LLLLR"; "LLLR"; "LLR"; "LLRLL"; "LLRR" ]
    s;
  let s = S.delete (of_s "LLLR") s in
  Alcotest.check_segs "list2" [ ""; "L"; "LLLLR"; "LLR"; "LLRLL"; "LLRR" ] s;
  let s = S.delete (of_s "LLRLL") s in
  Alcotest.check_segs "list3" [ ""; "L"; "LLLLR"; "LLR"; "LLRR" ] s;
  let s = S.delete (of_s "") s in
  Alcotest.check_segs "list4" [ "L"; "LLLLR"; "LLR"; "LLRR" ] s;
  let s = S.delete (of_s "LLR") s in
  Alcotest.check_segs "list5" [ "L"; "LLLLR"; "LLRR" ] s;
  ()

let test_delete_if_found () =
  with_random @@ fun rng ->
  let keys = random_segs 1000 rng in
  let s = S.of_list keys in
  let ss = SS.of_list keys in
  let keys = SS.elements ss in
  List.iteri
    (fun i k ->
      match S.delete_if_found k s with
      | None -> assert false
      | Some (j, s') ->
          Alcotest.(check int) "found at" i j;
          Alcotest.(check segs)
            "deleted"
            (List.filter (fun k' -> not @@ Segment.equal k k') keys)
            (S.sorted_list s');
          ignore s')
    keys;
  ()

let test_union () =
  let rec test_union rng n =
    if n <= 0 then
      let keys = random_segs 100 rng in
      List.fold_left (fun s k -> pair_add k s) pair_empty keys
    else
      let x = test_union rng (n - 1) in
      let y = test_union rng (n - 1) in
      let p = pair_union x y in
      pair_check_consistent p;
      p
  in
  with_random @@ fun rng -> ignore (test_union rng 5)

let test_range_of_zero () =
  with_random @@ fun rng ->
  let l_prefix = !"LLLRL" in
  let m_prefix = !"LLRLL" in
  let r_prefix = !"LLRLR" in
  let ls = S.append_prefix l_prefix @@ S.of_list @@ random_segs 123 rng in
  let ms = S.append_prefix m_prefix @@ S.of_list @@ random_segs 213 rng in
  let rs = S.append_prefix r_prefix @@ S.of_list @@ random_segs 312 rng in
  let s = S.union ls @@ S.union ms rs in
  let f k =
    let (_, prefix, k) = Segment.common_prefix m_prefix k in
    if Segment.is_empty prefix then 0
    else if Segment.is_empty k then -1
    else Segment.compare k prefix
  in
  let (lb, ub) = S.range_of_zero f s in
  Alcotest.(check int) "lb" (S.length ls) lb;
  Alcotest.(check int) "ub" (S.length ls + S.length ms) ub;

  let s = S.of_list @@ random_segs (RS.int rng 1000) rng in
  let segs = Array.of_seq @@ S.sorted_seq s in
  let case lb ub =
    let check_lb k =
      if lb >= Array.length segs
      then false
      else Segment.compare (Array.get segs lb) k <= 0
    in
    let check_ub k =
      if ub >= Array.length segs
      then true
      else Segment.compare (Array.get segs ub) k > 0
    in
    let f k =
      if check_lb k
      then if check_ub k then 0 else 1
      else -1
    in
    let (lb', ub') = S.range_of_zero f s in
    Alcotest.(check int) "lb" lb lb';
    Alcotest.(check int) "ub" ub ub';
  in
  for _ = 1 to 10000 do
    let lb = RS.int rng (1 + Array.length segs) in
    let ub = lb + RS.int rng (1 + Array.length segs- lb) in
    case lb ub
  done

let test_search () =
  let s = S.empty in
  let search k v = match S.search k v with `Exact n -> n | `Before n -> n in
  Alcotest.(check int) "search" 0 (search (of_s "L") s);
  Alcotest.(check (option int)) "find" None (S.find (of_s "L") s);
  let s = S.add (of_s "LR") s in
  Alcotest.(check int) "search" 0 (search (of_s "L") s);
  Alcotest.(check (option int)) "find" None (S.find (of_s "L") s);
  Alcotest.(check int) "search" 0 (search (of_s "LL") s);
  Alcotest.(check (option int)) "find" None (S.find (of_s "LL") s);
  Alcotest.(check int) "search" 0 (search (of_s "LR") s);
  Alcotest.(check (option int)) "find" (Some 0) (S.find (of_s "LR") s);
  Alcotest.(check int) "search" 1 (search (of_s "R") s);
  Alcotest.(check (option int)) "find" None (S.find (of_s "R") s);
  ()

let test_nth () =
  with_random @@ fun rng ->
  let keys = random_segs 10000 rng in
  let s, ss = List.fold_left (fun s k -> pair_add k s) pair_empty keys in
  let arr = Array.of_seq @@ SS.to_seq ss in
  Array.iteri
    (fun i k -> Alcotest.(check (option seg)) "nth_Some" (Some k) (S.nth i s))
    arr;
  Alcotest.(check (option seg)) "nth_None" None (S.nth (Array.length arr) s);

  let get_fst x = fst @@ Option.from_Some x in
  let get_snd x = snd @@ Option.from_Some x in
  let single = of_ss [ "LRL" ] in
  Alcotest.(check seg) "single" !"LRL" (get_fst @@ S.pop_nth 0 single);
  Alcotest.check_segs "single" [] (get_snd @@ S.pop_nth 0 single);

  let ext = of_ss [ "LR"; "LRRL" ] in
  Alcotest.check_segs "ext0" [ "LRRL" ] (get_snd @@ S.pop_nth 0 ext);
  Alcotest.check_segs "ext1" [ "LR" ] (get_snd @@ S.pop_nth 1 ext);

  let internal = of_ss [ "L"; "LL"; "R" ] in
  Alcotest.check_segs "internal0" [ "LL"; "R" ] (get_snd @@ S.pop_nth 0 internal);
  Alcotest.check_segs "internal1" [ "L"; "R" ] (get_snd @@ S.pop_nth 1 internal);
  Alcotest.check_segs "internal2" [ "L"; "LL" ] (get_snd @@ S.pop_nth 2 internal);

  let s = of_ss [ ""; "L"; "LLLLR"; "LLLR"; "LLR"; "LLRLL"; "LLRR" ] in
  let s = get_snd @@ S.pop_nth 3 s in
  Alcotest.check_segs "list2" [ ""; "L"; "LLLLR"; "LLR"; "LLRLL"; "LLRR" ] s;
  let s = get_snd @@ S.pop_nth 4 s in
  Alcotest.check_segs "list3" [ ""; "L"; "LLLLR"; "LLR"; "LLRR" ] s;
  let s = get_snd @@ S.pop_nth 0 s in
  Alcotest.check_segs "list4" [ "L"; "LLLLR"; "LLR"; "LLRR" ] s;
  let s = get_snd @@ S.pop_nth 2 s in
  Alcotest.check_segs "list5" [ "L"; "LLLLR"; "LLRR" ] s;

  let keys = random_segs 10000 rng in
  let s, ss = List.fold_left (fun s k -> pair_add k s) pair_empty keys in
  let rec loop n s ss =
    if n <= 0 || SS.cardinal ss <= 0 then ()
    else
      let ix = RS.int rng (S.length s) in
      let k = Option.from_Some @@ S.nth ix s in
      let k', s = Option.from_Some @@ S.pop_nth ix s in
      Alcotest.(check seg) "removed key" k k';
      let ss = SS.remove k ss in
      Alcotest.(check (list seg))
        "remained"
        (List.of_seq @@ SS.to_seq ss)
        (S.sorted_list s);
      loop (n - 1) s ss
  in
  loop 100 s ss

let test_split_at () =
  with_random @@ fun rng ->
  let random_test () =
    let len = Gen.int_range (100, 20000) rng in
    let keys = random_segs len rng in
    let s = List.fold_left (fun s k -> S.add k s) S.empty keys in
    let len = S.length s in
    (*remove duplication*)
    let split_at n =
      assert (0 <= n && n <= len);
      let s1, s2 = S.split_at n s in
      Alcotest.(check int) "s1 size" n (S.Debug.length_full s1);
      Alcotest.(check int) "s2 size" (len - n) (S.Debug.length_full s2);
      Alcotest.(check (list seg))
        "appended" (S.sorted_list s)
        (S.sorted_list s1 @ S.sorted_list s2)
    in
    split_at 0;
    split_at len
  in
  for _ = 0 to 30 do
    random_test ()
  done

let test_prefix () =
  with_random @@ fun rng ->
  let segs = random_segs 1000 rng in
  let s = S.of_list segs in
  let segs = S.sorted_list s in
  let prefix = Gen.(segment (int_range (10, 20))) rng in
  let prefixed_segs = List.map (Segment.append prefix) segs in
  let s = S.append_prefix prefix s in
  Alcotest.(check (list seg)) "prefixed" prefixed_segs (S.sorted_list s);
  let s = S.remove_prefix_exn prefix s in
  Alcotest.(check (list seg)) "prefixed" segs (S.sorted_list s);
  let s = S.add !"LLLL" s in
  let raised =
    try
      ignore (S.remove_prefix_exn !"R" s);
      false
    with _ -> true
  in
  Alcotest.(check bool) "invalid prefix" true raised;
  ()

let check_length t =
  Alcotest.(check int) "length" (S.length t) (S.Debug.length_full t)

let test_random () =
  with_random @@ fun rng ->
  let p = pair_empty in
  let p = repeat 10000 (random_add rng) p in
  let keys = random_segs 10000 rng in
  let p = List.fold_left (fun s k -> pair_add k s) p keys in
  pair_check_consistent p;
  let p = repeat 10000 (random_delete rng) p in
  pair_check_consistent p;
  let p = random_delete_existing rng p in
  pair_check_consistent p;
  let s = fst p in
  check_length s;
  ()

let test_persist () =
  with_random @@ fun rng ->
  let s, ss = repeat 200 (random_add rng) pair_empty in
  let cstr =
    let file_name = Filename.temp_file "test_persist_" "" in
    Format.printf "file: %s\n" file_name;
    let open Bigarray in
    let fd = Unix.openfile file_name [ O_RDWR ] 0o644 in
    let arr =
      array1_of_genarray @@ Unix.map_file fd char c_layout true [| 4096 |]
    in
    Plebeia.Xcstruct.of_bigarray arr
  in
  let pos_begin = Rand.int rng 100 in
  let pos_end_write = S.save cstr pos_begin s in
  let size_bytes = pos_end_write - pos_begin in
  Alcotest.(check int) "size" size_bytes (S.size s);
  let pos_end_read, s' = S.load cstr pos_begin in
  Alcotest.(check int) "write and read size" pos_end_write pos_end_read;
  Alcotest.(check segs) "compare" (S.sorted_list s) (S.sorted_list s');
  pair_check_consistent (s', ss);
  ()

let cases =
  [
    ("empty", `Quick, test_empty);
    ("add", `Quick, test_add);
    ("of_seq", `Quick, test_of_seq);
    ("delete", `Quick, test_delete);
    ("delete_if_found", `Quick, test_delete_if_found);
    ("union", `Quick, test_union);
    ("range_of_zero", `Quick, test_range_of_zero);
    ("search", `Quick, test_search);
    ("nth", `Quick, test_nth);
    ("split_at", `Quick, test_split_at);
    ("prefix", `Quick, test_prefix);
    ("random", `Quick, test_random);
    ("persist", `Quick, test_persist);
  ]

let () = Alcotest.run "segment_set" @@ [ ("patricia", cases) ]
