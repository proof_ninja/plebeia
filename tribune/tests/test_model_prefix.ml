open Plebeia_tribune.Model_prefix

let rec seq_of_list xs =
  match xs with
  | [] -> Seq.empty
  | x :: xs' -> fun () -> Seq.Cons (x, seq_of_list xs')

let remove_prefix_seq seq1 seq2 =
  let rec aux seq1 seq2 rev_prefix =
    match seq1 () with
    | Seq.Nil -> (rev_prefix, Seq.empty, seq2)
    | Seq.Cons (c1, seq1') -> (
        let seq1 () = Seq.Cons (c1, seq1') in
        match seq2 () with
        | Seq.Nil -> (rev_prefix, seq1, Seq.empty)
        | Seq.Cons (c2, seq2') ->
            if c1 = c2 then aux seq1' seq2' (c1 :: rev_prefix)
            else
              let seq2 () = Seq.Cons (c2, seq2') in
              (rev_prefix, seq1, seq2) )
  in
  let rev_prefix, seq1, seq2 = aux seq1 seq2 [] in
  (seq_of_list @@ List.rev rev_prefix, seq1, seq2)

module StringKey = struct
  type t = string

  let pp = Format.pp_print_string

  let compare = String.compare

  let size s = String.length s

  let empty = ""

  let is_empty = ( = ) ""

  let append = ( ^ )

  let remove_prefix s1 s2 =
    let prefix, s1, s2 =
      remove_prefix_seq (String.to_seq s1) (String.to_seq s2)
    in
    (String.of_seq prefix, String.of_seq s1, String.of_seq s2)

  let remove_prefix_opt prefix seg =
    let _, prefix, seg = remove_prefix prefix seg in
    if prefix = "" then Some seg else None

  let common_prefix seg1 seg2 =
    let prefix, _, _ = remove_prefix seg1 seg2 in
    prefix

  let random charset maxlen =
    assert (charset <> []);
    let charset = Array.of_list charset in
    let rec seq maxlen () =
      if maxlen <= 0 || Random.float 1.0 < 0.1 then Seq.Nil
      else
        let ix = Random.int (Array.length charset) in
        Seq.Cons (charset.(ix), seq (maxlen - 1))
    in
    String.of_seq (seq maxlen)
end

module M = Make (struct
  module Key = StringKey

  module Value = struct
    type t = int

    let pp = Format.pp_print_int

    let size _ = 4
  end

  let max_size_in_bytes = 256
end)

module StringKeyTest = struct
  let test_remove_prefix () =
    let p, s1, s2 = StringKey.remove_prefix "ABC" "ABX" in
    Alcotest.(check string) "prefix" "AB" p;
    Alcotest.(check string) "s1" "C" s1;
    Alcotest.(check string) "s2" "X" s2;
    let p, s1, s2 = StringKey.remove_prefix "ABC" "AB" in
    Alcotest.(check string) "prefix" "AB" p;
    Alcotest.(check string) "s1" "C" s1;
    Alcotest.(check string) "s2" "" s2;
    let p, s1, s2 = StringKey.remove_prefix "AB" "ABC" in
    Alcotest.(check string) "prefix" "AB" p;
    Alcotest.(check string) "s1" "" s1;
    Alcotest.(check string) "s2" "C" s2;
    ()

  let tests =
    [
      ( "StringKey.remove_prefix",
        [ ("remove_prefix", `Quick, test_remove_prefix) ] );
    ]
end

module ModelTest = struct
  let insert_random set max_key_len tbl t =
    let k = StringKey.random set max_key_len in
    let v = Random.int 10000 in
    Hashtbl.replace tbl k v;
    let t = M.add k v t in
    M.check_maximumly_prefixed_full t;
    t

  let delete_random set max_key_len tbl t =
    let k = StringKey.random set max_key_len in
    Hashtbl.remove tbl k;
    let t = M.delete k t in
    M.check_maximumly_prefixed_full t;
    t

  let check_consistent tbl t =
    List.iter
      (fun (k, v) ->
        Alcotest.(check (option int))
          (Printf.sprintf "Sound %s %d" k v)
          (Some v) (Hashtbl.find_opt tbl k))
      (M.elements t);
    Hashtbl.iter
      (fun k v ->
        Alcotest.(check (option int))
          (Printf.sprintf "Complete %s %d" k v)
          (Some v) (M.find k t))
      tbl;
    ()

  let delete_key tbl k t = Hashtbl.remove tbl k; M.delete k t
  let insert_key tbl k v t = Hashtbl.replace tbl k v; M.add k v t

  let rec iter n f x = if n <= 0 then x else iter (n - 1) f (f x)

  let test_insert () =
    let t = M.empty in
    Alcotest.(check (option int)) "empty" None (M.find "AAA" t);
    let t = M.add "AAA" 0 t in
    Alcotest.(check (option int)) "Add the first key" (Some 0) (M.find "AAA" t);
    let t = M.add "AAA" 1 t in
    Alcotest.(check (option int))
      "Override the first key" (Some 1) (M.find "AAA" t);
    let t = M.add "AAB" 2 t in
    Alcotest.(check (option int)) "Add the second key" (Some 2) (M.find "AAB" t);
    ()

  let test_random_insert () =
    let tbl = Hashtbl.create 100 in
    let t = iter 10000 (insert_random [ 'A'; 'B' ] 50 tbl) M.empty in
    check_consistent tbl t

  let test_delete () =
    let t = M.empty in
    let t = M.delete "AAA" t in (* delete from empty *)
    let t = M.add "AAA" 0 t in
    let t = M.add "AAA" 1 t in
    let t = M.add "AAB" 2 t in
    let t = M.delete "AAA" t in
    Alcotest.(check (option int)) "deleted AAA" None (M.find "AAA" t);
    Alcotest.(check (option int)) "not deleted AAB" (Some 2) (M.find "AAB" t);
    let t = M.delete "AAB" t in
    Alcotest.(check (option int)) "deleted AAB" None (M.find "AAB" t);
    M.check_maximumly_prefixed_full t;
    let t = M.add "CCC" 3 t in
    Alcotest.(check (option int)) "insert after delete" 
      (Some 3) (M.find "CCC" t);
   ()

  let test_random_insert_delete () =
    let tbl = Hashtbl.create 100 in
    let t = iter 10000 (insert_random [ 'A'; 'B' ] 20 tbl) M.empty in
    let keys =
      Hashtbl.fold
        (fun k _ l -> if Random.float 1.0 < 0.5 then k :: l else l )
        tbl []
    in
    let t = List.fold_left (fun t k -> delete_key tbl k t) t keys in
    check_consistent tbl t

  let test_random_operations () =
    let tbl = Hashtbl.create 100 in
    let genkey () = StringKey.random [ 'A'; 'B' ] 20 in
    let insert t =
      let key = genkey () in
      let v = Random.int 10000 in
      Format.printf "INSERT %s\n" key;
      insert_key tbl key v t
    in
    let delete_single t =
      let key = genkey () in
      Format.printf "DELETE SINGLE %s\n" key;
      delete_key tbl key t
    in
    let delete_existing t =
      Format.printf "DELETE EXISTING\n";
      let n = Hashtbl.length tbl in
      let chance = if n <= 0 then 1.0 else 1.0 /. float_of_int n in
      let keys =
        Hashtbl.fold
          (fun k _ l -> if Random.float 1.0 < chance then k :: l else l )
          tbl []
      in
      List.fold_left (fun t k -> delete_key tbl k t) t keys
    in
    let empty t =
      Format.printf "EMPTY\n";
      let t = Hashtbl.fold (fun k _ t -> M.delete k t) tbl t in
      Hashtbl.reset tbl; assert (M.is_empty t); t
    in
    let add_many t =
      let rec aux t =
        if Hashtbl.length tbl < 1000
        then aux (insert_random [ 'A'; 'B' ] 20 tbl t)
        else t
      in
      Format.printf "ADD_MANY\n"; aux t
    in
    let random_op t =
      assert (Hashtbl.length tbl = M.nelems t);
      Format.printf "%d elems\n" (Hashtbl.length tbl);
      if Random.float 1.0 < 0.001 then
        empty t
      else if Random.float 1.0 < 0.001 then
        add_many t
      else
        let x = Random.float 1.0 in
        if x < 0.8 then insert t
        else if x < 0.9 then delete_single t
        else delete_existing t
    in
    let batch t =
      let t = iter 1000 random_op t in
      check_consistent tbl t;
      t
    in
    let t = M.empty in
    ignore (iter 100 batch t);
    ()

  let tests =
    [
      ( "ModelTest.insert",
        [
          ("insert", `Quick, test_insert);
          ("random_insert", `Quick, test_random_insert);
        ] );
      ( "ModelTest.delete",
        [
          ("delete", `Quick, test_delete);
          ("random_insert_delete", `Quick, test_random_insert_delete);
          ("random_operations", `Quick, test_random_operations);
        ] );
    ]
end

let () = Alcotest.run "model_prefix" @@ StringKeyTest.tests @ ModelTest.tests
