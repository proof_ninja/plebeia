open Plebeia
open Plebeia_tribune.Persist_plus
open Test_utils

module RS = Random.State

let with_storage f =
  with_tempdir @@ fun d ->
  let s = TStorage.create (d ^/ "tribune.context") in
  f s

let (!) s = from_Some @@ Segment.of_string s
let (!!) n = Index.Unsafe.of_int n

let test_add_delete () = with_storage @@ fun s ->
  let module P = Make(struct let storage = s let max_size_in_bytes= 256 let check= true end) in
  let rec enum_all_segs len =
    if len <= 0 then [Segment.empty]
    else
      let segs = enum_all_segs (len - 1) in
      List.map (Segment.cons Segment.Left) segs @
      List.map (Segment.cons Segment.Right) segs
  in
  let segs = enum_all_segs 10 in (* 1024 segments *)
  let zeros = List.map (fun k -> (k, !!0)) segs in
  let t = P.rebalance @@ P.adds zeros P.empty in
  List.iter (fun k -> assert (P.find k t = Some !!0)) segs;
  let ones = List.map (fun k -> (k, !!1)) segs in
  let t = P.rebalance @@ P.adds ones t in
  List.iter (fun k -> assert (P.find k t = Some !!1)) segs;
  let t = P.rebalance @@ P.deletes segs t in
  assert (P.is_empty t);
  ()

let test_save () = with_storage @@ fun s -> with_random @@ fun rng ->
  let module P = Make(struct let storage = s let max_size_in_bytes= 256 let check= true end) in
  let add_random t =
    let k = Gen.(segment (int_range (1,100))) rng in
    let v = Gen.index rng in
    P.adds [k, v] t
  in
  let rec iter f n x =
    if n <= 0 then x else iter f (n - 1) (f x)
  in
  let t = P.rebalance @@ iter add_random 10000 P.empty in
  P.save P.empty;
  P.save t


let test_delete () = with_storage @@ fun s ->
  let module P = Make(struct let storage = s let max_size_in_bytes= 256 let check= true end) in
  let t = P.adds [!"L", !!1] P.empty in
  prerr_endline "Added L";
  let t = P.deletes [!"L"] t in
  prerr_endline "Deleted L";
  if P.is_empty t then Format.eprintf "t= %a@." P.pp t;
  assert (P.is_empty t);

  let t = P.adds [!"L", !!1; !"R", !!1] P.empty in
  prerr_endline "Added L and R";
  let t = P.deletes [!"L"] t in
  prerr_endline "Deleted L";
  List.iter2 (fun (k, v) (k', v') -> assert (Segment.equal k k' && v = v'))
    (P.elements t) [!"R", !!1]


let test_delete2 () = with_storage @@ fun s ->
  let module P = Make(struct let storage = s let max_size_in_bytes= 64 let check= true end) in
  let t = P.adds [!"L", !!1] P.empty in
  prerr_endline "Added L";
  let t = P.deletes [!"L"] t in
  prerr_endline "Deleted L";
  if P.is_empty t then Format.eprintf "t= %a@." P.pp t;
  assert (P.is_empty t);

  let t = P.adds [!"L", !!1; !"R", !!1] P.empty in
  prerr_endline "Added L and R";
  let t = P.deletes [!"L"] t in
  prerr_endline "Deleted L";
  List.iter2 (fun (k, v) (k', v') -> assert (Segment.equal k k' && v = v'))
    (P.elements t) [!"R", !!1]


let test_init () = with_storage @@ fun s ->
  let module P = Make(struct let storage = s let max_size_in_bytes= 64 let check= true end) in
  let kvs = List.init 100 (fun i -> !(String.make i 'L'), !!i) in
  let n = P.init kvs in
  List.iter (fun (k,_) -> assert (P.find k n <> None)) kvs

(* CR jun to yoshihiro503 : not used *)
let assert_equal ?pp x1 x2 =
  if x1 <> x2 then
    begin (match pp with
    | Some pp ->
       Format.eprintf "expected: %a but got %a" pp x1 pp x2;
    | None ->
       ());
          assert false
    end

(* CR jun to yoshihiro503 : not used *)
let pp_kv ppf (k,v)  =
  Format.fprintf ppf "(%a -> %a)"
  Segment.pp k Index.pp v

let test_bulk_adds () = with_storage @@ fun s ->
  let module P = Make(struct let storage = s let max_size_in_bytes= 64 let check= true end) in
  let count t = List.length @@ P.elements t in
  let mem t (k, v) = (P.find k t = Some v) in
  let equiv t1 t2 =
    (count t1 = count t2)
    && List.for_all (mem t2) (P.elements t1)
  in
  let check_adds kvs t =
    let t1 = P.adds kvs t in
    let t2 = P.bulk_adds kvs t in
    assert (equiv t1 t2)
  in
  check_adds [!"L", !!1; !"R", !!1] P.empty;
  check_adds [!"LLLRL", !!1; !"LLLRR", !!1] (P.init (List.init 100 (fun i -> !(String.make i 'L'), !!i)));
  let generate =
    let open Gen in
    Gen.list (int 1000) (segment (int 10)) >>= fun segs ->
    let sorted_segs = List.sort Segment.compare segs |> List.uniq_sorted Segment.equal in
    list (return @@ List.length sorted_segs) index >>= fun values ->
    let kvs = List.combine sorted_segs values in
    let split xs = if xs = [] then return ([], [])
                   else int (List.length xs) >>| fun i -> List.split_at i xs
    in
    split kvs >>= fun (lefts, rest) ->
    split rest >>= fun (centers, rights) ->
    let node = P.init lefts |> P.adds rights |> P.adds centers |> P.deletes (List.map fst centers) |> P.rebalance in
    return (node, centers)
  in
  with_random begin fun st ->
    for _ = 0 to 10000 do
      let (node, kvs) = generate st in
      check_adds kvs node
    done
  end

let test_delete_under () = with_storage @@ fun s ->
  let module P = Make(struct let storage = s let max_size_in_bytes= 64 let check= true end) in

  let kvs = List.init 100 (fun i -> !(String.make i 'L'), !!i) in
  let n = P.init kvs in
  let n = P.rebalance @@ P.delete_under !(String.make 5 'L') n in
  assert (List.map (fun (seg, _) -> Segment.to_string seg) @@ P.elements n
            = [ ""; "L"; "LL"; "LLL"; "LLLL" ]);
  with_random @@ fun rng ->

  (* random delete_under *)
  for i = 1 to 100 do
    Format.eprintf "random deletion #%d@." i;
    let kvs =
      List.uniq_sorted (fun (k1, _) (k2, _) -> Segment.equal k1 k2)
      @@ List.sort (fun (k1,_) (k2,_) -> Segment.compare k1 k2)
      @@ Gen.(list (return 1000)
                (segment (int_range (1,100)) >>= fun seg ->
                 index >>| fun i ->
                 (seg, i))) rng
    in
    let n = P.init kvs in
    let pat = Gen.(segment (int_range (0,100))) rng in
    let n = P.rebalance @@ P.delete_under pat n in
    let kvs' = List.filter (fun (k,_) ->
        let common, _, _ = Segment.common_prefix pat k in
        not @@ Segment.equal common pat) kvs
    in
    let es = P.elements n in
    assert (List.length es = List.length kvs');
    assert (List.for_all2 (fun (k,v) (k',v') -> Segment.equal k k' && v = v') es kvs')
  done

let () =
  let open Alcotest in
  run "test_persist_plus"
    [ "key_save",      [ "key_save",   `Quick, Key.test ]
    ; "value_save",    [ "value_save", `Quick, Value.test ]
    ; "save",          [ "save",       `Quick, test_save ]

    ; "add_delete",    [ "add_delete", `Quick, test_add_delete ]
    ; "delete",        [ "delete",     `Quick, test_delete
                       ; "delete2",    `Quick, test_delete2
                       ]

    ; "init",          [ "init",         `Quick, test_init ]
    ; "delete_under",  [ "delete_under", `Quick, test_delete_under ]
    ; "bulk_adds",     ["bulk adds",     `Quick, test_bulk_adds]
    ]
