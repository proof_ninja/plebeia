(* Implementation using B-tree.  Due to its complexity of the internal nodes
   which have kv pairs, we have decided to use B+ instead.  See persist_plus.ml
   for the B+ implementation.
*)
open Plebeia
open Utils

module TStorage = Gstorage.Make(struct
    let head_string =
      let s = "PLEBEIA TRIBUNE" in
      s ^ String.make (24 - String.length s) '\000'

    let version = 1 (* 0.1 *)

    let max_index = Index.max_int

    let bytes_per_cell = 4096
  end)

module A = struct
  include Array

  let (++) = append

  let replace a pos len xs =
    try
      sub a 0 pos (* 0..pos-1 *)
      ++ xs (* replaces pos,..,pos+len-1 *)
      ++ sub a (pos+len) (length a - pos - len) (* pos+len..last *)
    with
    | e ->
        Format.eprintf "replace len=%d pos=%d len=%d@."
          (length a) pos len;
        raise e

  let findi f xs =
    let len = Array.length xs in
    let rec iter i =
      if i <= len - 1 then
        let xi = Array.unsafe_get xs i in
        if f xi then Some (xi, i)
        else iter (i + 1)
      else None
    in
    iter 0
end

let (++) = A.(++)

module C = Plebeia.Xcstruct

type 'a loader = C.t -> int -> int * 'a
type 'a saver = C.t -> int -> 'a -> int

let load_list (f : 'a option loader) cstr i =
  let rec loop st i =
    match f cstr i with
    | i, None -> i, List.rev st
    | i, Some x -> loop (x::st) i
  in
  loop [] i

let save_list (f : 'a option saver) cstr i xs =
  let i = List.fold_left (fun i x -> f cstr i (Some x)) i xs in
  f cstr i None

module Make
    (P : sig
       val storage : TStorage.t
       val max_size_in_bytes : int
     end)
= struct

  let max_size_in_bytes = P.max_size_in_bytes

  module Key = struct
    include Segment
    let size s = Segment.length s / 8 + 1

    let load : t option loader = fun cstr i ->
      let bytes = C.get_uint8 cstr i in
      let i = i + 1 in
      if bytes = 0 then i, None
      else
        let seg =
          let segstr = C.copy cstr i bytes in
          match Serialization.decode segstr with
          | Some seg -> seg
          | None -> assert false (* XXX *)
        in
        i + bytes, Some seg

    let save : t option saver = fun cstr i -> function
      | None ->
          C.set_uint8 cstr i 0;
          i + 1
      | Some k ->
          let sz = size k in
          assert (sz < 256);
          C.set_uint8 cstr i sz;
          let i = i + 1 in
          let enc = Serialization.encode k in
          let l = String.length enc in
          assert (l = sz);
          C.blit_from_string enc 0 cstr i l;
          i + l

    (* save with check.  already well checked.
    let save cstr i ko =
      let res = save cstr i ko in
      match load cstr i with
      | res', ko' ->
          assert (res = res');
          assert (match ko, ko' with
              | Some k, Some k' -> Segment.equal k k'
              | None, None -> true
              | _ -> false);
          res
    *)
  end

  module Value = struct
    include Index
    let size _ = 4

    let load : t option loader = fun cstr i ->
      let open Stdlib in
      let idx = of_uint32 @@ C.get_uint32 cstr i in
      i + 4, if idx = Index.zero then None else Some idx

    let save : t option saver = fun cstr i -> function
      | None ->
          let open Stdlib in
          C.set_uint32 cstr i (to_uint32 Index.zero);
          i + 4
      | Some v ->
          let open Stdlib in
          C.set_uint32 cstr i (to_uint32 v);
          i + 4
  end

  type key = Key.t
  type value = Value.t

  let load_key_value cstr i =
    match Key.load cstr i with
    | i, None -> i, None
    | i, Some k ->
        match Value.load cstr i with
        | _, None -> assert false
        | i, Some v -> i, Some (k,v)

  let save_key_value cstr i = function
    | None -> Key.save cstr i None
    | Some (k,v) ->
        let i = Key.save cstr i (Some k) in
        Value.save cstr i (Some v)

  (* Space optimized ents by prefix compression.
     Used only for persistence.
  *)
  module Compressed_ents : sig
    type t
    val size : t -> int
    val make : (Key.t * Value.t) array -> t
    val expand : t -> (Key.t * Value.t) array
    val load : t loader
    val save : t saver
  end = struct
    type t =
      { prefix : key
      ; ents : (key * value) array
      ; size : int
      }

    let size t = t.size

    let compute_size prefix ents =
      1 + Key.size prefix
      + A.fold_left (fun sum (k, v) ->
        sum + 1 + Key.size k + Value.size v) 1 ents

    let make ents =
      if ents = [||] then (* empty root *)
        (* not sure... *)
        { prefix= Key.empty; ents= [||]; size = 1 + Key.size Key.empty + 1 }
      else
        let nents = A.length ents in
        let l, _ = A.get ents 0 in
        let r, _ = A.get ents (nents - 1) in (* nents > 0 *)
        let prefix, lpost, rpost = Key.common_prefix l r in
        let ents = A.mapi (fun i (k,v) ->
            let k =
              if i = 0 then lpost
              else if i = nents - 1 then rpost
              else
                let common, lpost', kpost = Key.common_prefix prefix k in
                assert (Key.equal prefix common);
                assert (Key.is_empty lpost');
                kpost
            in
            (k,v)) ents
        in
        let size = compute_size prefix ents in
        { prefix ; ents ; size }

    let expand ents =
      Array.map (fun (k, v) ->
          (Key.append ents.prefix k, v)) ents.ents

    let load : t loader = fun cstr i ->
      match Key.load cstr i with
      | _, None -> assert false
      | i, Some prefix ->
          let i, kvs = load_list load_key_value cstr i in
          let kvs = Array.of_list kvs in
          let size = compute_size prefix kvs in
          i, { prefix; ents= kvs ; size }

    let save cstr i ents =
      let i = Key.save cstr i @@ Some ents.prefix in
      save_list save_key_value cstr i @@ A.to_list ents.ents

    let _equal ents ents' =
      Key.equal ents.prefix ents'.prefix
      && A.length ents.ents = A.length ents'.ents
      &&
      let rec f i =
        if i = A.length ents.ents then true
        else
          let k,v = A.get ents.ents i in
          let k',v' = A.get ents'.ents i in
          let b = Key.equal k k' && v = v' in
          if b then f (i+1) else b
      in
      f 0

    (* save with check.  already well checked
    let save cstr i ents =
      let res = save cstr i  ents in
      let res', ents' = load cstr i in
      assert (res = res' && equal ents ents');
      res
    *)
  end

  type t =
    { root : bool

    ; mutable desc : desc
      (* physically shared nodes are loaded/forgot at the same time
         by mutation
      *)
    }

  and desc =
    | Disk of Index.t
    | View of view

  and view =
    { ents : (key * value) array
    ; subnodes : t array (* leaves+1 *) option
    ; mutable index : Index.t option
    ; mutable size : [`Exactly of int | `Overflow] option
      (* Size on disk using Compressed_ents in bytes.
         The size needs not be exact for overflown nodes.
      *)
    }

  (* view i/o *)

  let size_of_view view =
    match view.size with
    | Some (`Exactly x) -> x
    | _ ->
        let sz =
          let nents = A.length view.ents in
          if nents = 0 then
            match view.subnodes with
            | None ->
                (1 (* prefix sz *) + 1 (* prefix *)
                 + 1 (* ent null *) + 4 (* subnodes null *))
            | Some ss ->
                (1 (* prefix sz *) + 1 (* prefix *)
                 + 1 (* ent null *)
                 + A.length ss * 4 (* subnodes *) + 4 (* subnodes null *))
          else
            let common, _, _ =
              Segment.common_prefix
              (fst @@ A.get view.ents 0)
              (fst @@ A.get view.ents (nents - 1)) (* nents > 0 *)
            in
            let prefix_len = Segment.length common in
            1 + prefix_len / 8 + 1
            + Array.fold_left (fun sum (k, v) ->
                sum + 1 +
                (Segment.length k - prefix_len) / 8 + 1
                + Value.size v) 1 view.ents
            + match view.subnodes with
              | None -> 4
              | Some subnodes ->
                  assert (A.length subnodes = A.length view.ents + 1);
                  A.length subnodes * 4 + 4
        in
        view.size <- Some (`Exactly sz);
        sz

  let size_of_view' view =
    match view.size with
    | Some res -> res
    | _ ->
        let fixed =
          1 + (* prefix size *)
          + A.length view.ents * (1 + 4) + 1 (* ents seg size + index and sentinel *)
          + (if view.subnodes = None then 0 else 4 (* at least one subnode pointer *)) + 4 (* subnode sentinel *)
        in
        if fixed > max_size_in_bytes then `Overflow
        else
          let nents = A.length view.ents in

          let common =
            if A.length view.ents = 0 then Segment.empty
            else
              let common, _, _ =
                  Segment.common_prefix
                    (fst @@ A.get view.ents 0)
                    (fst @@ A.get view.ents (A.length view.ents - 1)) (* view.ents <> [||] *)
              in
              common
          in

          let prefix_len = Segment.length common in
          let fixed = fixed + prefix_len / 8 + 1 in

          let rec loop sum i =
            if i = nents then begin
              (* Debug. Check the correctness *)
              if sum <> size_of_view view then
                Format.eprintf "invalid sizes %d %d@." sum (size_of_view view);
              assert (sum = size_of_view view);
              `Exactly sum
            end else
              let seg, _ = A.get view.ents i in
              let seglen = Segment.length seg - prefix_len in
              let sum =
                sum + seglen / 8 + 1 (* ent seg *)
                + if view.subnodes = None then 0 else 4 (* ss *)
              in
              if sum > max_size_in_bytes then `Overflow
              else loop sum (i+1)
          in
          loop fixed 0

  let loaded = ref 0

  let load_view idx =
    let cstr = TStorage.get_cell P.storage idx in
    let i, compressed_ents = Compressed_ents.load cstr 0 in
    let ents = Compressed_ents.expand compressed_ents in
    let _i, subnodes = load_list Value.load cstr i in
    let subnodes = match subnodes with
      | [] -> None
      | ss -> Some (Array.of_list (List.map (fun i -> { root= false; desc= Disk i }) ss))
    in
    incr loaded;
    if !loaded mod 1000 = 0 then
      Format.eprintf "total loaded tribune nodes: %d@." !loaded;
    { ents
    ; subnodes
    ; index= Some idx
    ; size= Some (`Exactly (Compressed_ents.size compressed_ents))
    }

  let saved = ref 0

  let save_view v =
    match v.index with
    | Some _ -> ()
    | None ->
        let sz = size_of_view v in
        assert (sz <= max_size_in_bytes);
        let module C = Plebeia.Xcstruct in
        let idx = TStorage.new_index P.storage in
        let cstr = TStorage.get_cell P.storage idx in
        let i = 0 in
        let i = Compressed_ents.save cstr i (Compressed_ents.make v.ents) in

        let j = i in

        let ss = match v.subnodes with
          | None -> []
          | Some ss ->
              List.map (function
                  | {desc= Disk i; _} -> i
                  | {desc= View { index= Some i ; _}; _} -> i
                  | _ -> assert false)
              @@ Array.to_list ss
        in
        let i = save_list Value.save cstr i ss in

        if size_of_view v <> i then begin
          Format.eprintf "size_of_view %d i %d@." sz i;
          Format.eprintf "ents %d  j %d@."
            (Array.fold_left (fun sum (k,_v) ->
                 sum + Key.size k + 1 + 4) 1 v.ents)
            j;
        end;
        assert (size_of_view v = i);
        v.index <- Some idx;
        incr saved

  (* view *)

  let may_forget n =
    let desc = match n.desc with
      | View { index= Some i; _ } -> Disk i
      | desc -> desc
    in
    n.desc <- desc

  let view n = match n.desc with
    | View v -> v
    | Disk i ->
        let v = load_view i in
        n.desc <- View v;
        v

  let size n = size_of_view @@ view n

  let overflow v =
    begin (* check the lower bound first since size_of_view is very costy *)
      match v.subnodes with
      | None    -> A.length v.ents * (2 + 4) + 2
      | Some ss -> A.length v.ents * (2 + 4) + 2 + A.length ss * 4 + 4
    end > max_size_in_bytes
    || match size_of_view' v with
       | `Exactly x -> x > max_size_in_bytes
       | `Overflow -> true

  let _underflow v =
    match size_of_view' v with
    | `Overflow -> false
    | `Exactly sz -> sz < max_size_in_bytes / 2

  let rec save node =
    match node.desc with
    | Disk _ -> ()
    | View { index= Some _; _ } -> ()
    | View ({ index= None; subnodes= None; _ } as v) -> save_view v
    | View ({ index= None; subnodes= Some ss; _ } as v) ->
        Array.iter save ss;
        save_view v

  (* only for small data *)
  let rec pp ppf t =
    let open Format in
    fprintf ppf
      "@[root=%b@ %a@]"
      t.root
      (fun ppf -> function
       | Disk i -> fprintf ppf "Disk %a" Index.pp i
       | View v ->
           fprintf ppf "ents=@[%a@] subnodes=%a"
             (list ";@ " (fun ppf (s, i) ->
                  fprintf ppf "%a %a"
                    Key.pp s Value.pp i))
             (A.to_list v.ents)
             (fun ppf -> function
              | None -> fprintf ppf "None"
              | Some subnodes ->
                  fprintf ppf "@[%a@]"
                    (list "; @ " pp) (A.to_list subnodes)) v.subnodes)
      t.desc

  (* just for debugging *)
  let rec elements n =
    let v = view n in
    match v.subnodes with
    | None -> A.to_list v.ents
    | Some subnodes ->
        List.sort (fun (k1, _) (k2, _) -> Key.compare k1 k2)
        @@ A.to_list v.ents @ List.concat_map elements (A.to_list subnodes)

  let empty =
    { root= true;
      desc= View { ents= [||]; subnodes= None; index= None
                 ; size= Some (`Exactly (1 + 1 + 4))
                 }
    }

  let is_empty_view v = v.ents = [||] && v.subnodes= None

  let is_empty n = is_empty_view @@ view n

  (* check as many things as possible without any loading *)
  let validate n =
    (* skip some tests when nents is very large.
       things will be checked properly when they are split into much
       smaller nodes *)
    match n.desc with
    | Disk _ -> n
    | View ({ ents; subnodes; _ } as v) ->
        begin
          if is_empty_view v then ()
          else begin
            let nents = A.length ents in
            (* nents can be 0! *)
            if nents < 1000 then
              for i = 0 to nents - 2 do
                let seg , _ = A.get ents i in
                let seg', _ = A.get ents (i+1) in
                assert (Key.compare seg seg' = -1)
              done;
            match subnodes with
            | None -> ()
            | Some subnodes ->
                (* Only check subnodes already loaded *)
                let nsubnodes = A.length subnodes in
                assert (nents + 1 = nsubnodes);
                if nents < 1000 then
                  for i = 0 to nents do
                    match (A.get subnodes i).desc with
                    | Disk _ -> () (* never mind *)
                    | View subv ->
                        let subents = subv.ents in
                        let nsubents = A.length subents in
                        if nsubents = 0 then ()
                        else begin
                          let subents_smallest = fst (A.get subents 0) in
                          let subents_largest = fst (A.get subents (A.length subents - 1)) in
                          let prev_largest =
                            if i = 0 then None else Some (fst (A.get ents (i-1)))
                          in
                          let next_smallest =
                            if i = nents then None else Some (fst (A.get ents i))
                          in
                          begin match prev_largest with
                          | None -> ()
                          | Some prev_largest -> assert (Key.compare prev_largest subents_smallest = -1)
                          end;
                          begin match next_smallest with
                          | None -> ()
                          | Some next_smallest -> assert (Key.compare subents_largest next_smallest = -1)
                          end;
                        end
                  done
            end
        end;
        n

  let build root ents subnodes =
    validate
      { root;
        desc= View { ents; subnodes; index= None; size= None } }

  let update_ent n i x =
    let v = view n in
    assert (0 <= i && i < A.length v.ents);
    let ents = A.copy v.ents in
    let seg, _ = A.get ents i in
    A.set ents i (seg, x);
    let v = { ents; index= None; subnodes= v.subnodes; size= None } in
    { n with desc= View v }

  let update_subnode n j subnode =
    let v = view n in
    (* v.ents are shared *)
    match v.subnodes with
    | None -> assert false
    | Some subnodes ->
        let subnodes = A.copy subnodes in
        A.set subnodes j subnode;
        (* XXX size is never changed! *)
        let v = { ents= v.ents; subnodes= Some subnodes; index= None; size= None } in
        { n with desc= View v }

  (* XXX use bin tree *)
  let inject n j (kseg, x) lro =
    let v = view n in
    let ents = A.replace v.ents j 0 [|(kseg,x)|] in
    (* very slow when the ents becomes big
    assert (List.sort compare ((kseg,x)::A.to_list v.ents)
            = List.sort compare (A.to_list ents));
    *)
    let subnodes = match lro, v.subnodes with
      | None, None -> None
      | Some (left, right), Some subnodes ->
          let subnodes' = A.replace subnodes j 1 [|left; right|] in
          Some subnodes'
      | _ -> assert false
    in
    { n with desc= View { ents; subnodes; index= None; size= None } }

  let rec depth n =
    match (view n).subnodes with
    | None -> 1
    | Some subnodes -> depth (A.get subnodes 0) + 1

  let bsearch kseg v =
    let nleaves = A.length v.ents in
    (* nleaves can be 0 *)
    if nleaves = 0 then `Not_found 0
    else
      let get = A.get v.ents in
      let check j =
        let seg, i = get j in
        let comp = Key.compare kseg seg in
        i, comp
      in
      let rec bsearch (l,r) =
        let j = (l + r) / 2 in
        match check j with
        | i, 0 -> `Found (j, i)
        | _, -1 ->
            if j = l then `Not_found l
            else bsearch (l,j-1)
        | _, 1 ->
            if j = r then `Not_found (l+1)
            else bsearch (j+1,r)
        | _ -> assert false
      in
      bsearch (0, nleaves-1)

  let search kseg n = bsearch kseg (view n)

  let rec find kseg n =
    if is_empty n then None
    else
      match search kseg n with
      | `Found (_, x) -> Some x
      | `Not_found j ->
          let v = view n in
          match v.subnodes with
          | None -> None
          | Some subnodes ->
              find kseg @@ A.get subnodes j

  let split_array ats sz =
    let ranges =
      let rec f start = function
        | [] -> [(start, sz - 1)]
        | at::ats -> (start, at-1) :: f (at+1) ats
      in
      f 0 ats
    in
    ats, ranges

  let split x n =
    assert (x >= 2);
    let v = view n in
    let nents = A.length v.ents in
    let points = List.init (x-1) (fun i -> nents * (i+1) / x) in
    let ups, downs = split_array points (A.length v.ents) in
    let ups = List.map (A.get v.ents) ups in
    let downs =
      List.map (fun (start, end_) ->
          let ents = A.sub v.ents start (end_ - start + 1) in
          let subnodes = match v.subnodes with
            | None -> None
            | Some subnodes ->
                Some (A.sub subnodes start (end_ - start + 2))
          in
          build false ents subnodes
        ) downs
    in
    ups, downs

  (* merge underflows of subnodes of n

     XXX if the entries under [n] are very small, it is possible
     that subnode underflow is never resolved by this function.
     In that case the result has 0 ents and 1 underflow subnode.
  *)
  let rec merge_underflows n =
    match n.desc with
    | Disk _ | View { index= Some _; _ } -> n
    | _ ->
        let v = view n in
        match v.subnodes with
        | None -> (* nothing to do *) n
        | Some ss ->
            (* XXX this still loads many nodes! *)
            let ss = Array.map merge_underflows ss in
            let kvss = None :: List.map (fun kv -> Some kv) (A.to_list v.ents) in
            let ss = List.map (fun s ->
                let sz = match s.desc with
                  | Disk _ | View { index= Some _; _ } ->
                      (* fixated already.  not to try merge with it as possible *)
                      4096 (* dummy *)
                  | _ -> size_of_view (view s)
                in
                sz, s) @@ A.to_list ss in
            let xs =
              List.concat @@ List.map2 (fun kvs s ->
                  match kvs with
                  | None -> [`Sub s]
                  | Some kv -> [`Ent kv; `Sub s]
                ) kvss ss
            in
            let merge s1 kv s2 =
              let vs1 = view s1 in
              let vs2 = view s2 in
              Format.eprintf "Merging %d %d , %d %d in bytes@."
                (A.length vs1.ents)
                (A.length vs2.ents)
                (size_of_view vs1)
                (size_of_view vs2);
              let ents = vs1.ents ++ [|kv|] ++ vs2.ents in
              let ss = match vs1.subnodes, vs2.subnodes with
                | None, None -> None
                | Some ss1, Some ss2 -> Some (ss1 ++ ss2)
                | _ -> assert false
              in
              build n.root ents ss
            in
            let rec f (rev_ents, rev_ss) = function
              | [] -> List.rev rev_ents, List.rev rev_ss
              | `Sub (_, s) :: [] -> f (rev_ents, s :: rev_ss) []
              | `Sub (_sz1, s1) :: `Ent kv :: `Sub (sz2, s2) :: [] when sz2 < max_size_in_bytes / 2 ->
                  let s1_kv_s2 = merge s1 kv s2 in
                  f (rev_ents, s1_kv_s2 :: rev_ss) []
              | `Sub (sz1, s1) :: `Ent kv :: `Sub (sz2, s2) :: (`Ent _ :: `Sub (sz3, _s3) :: _ as xs)  when sz2 < max_size_in_bytes / 2 && sz1 < sz3 ->
                  let s1_kv_s2 = merge s1 kv s2 in
                  let sz = size_of_view @@ view s1_kv_s2 in
                  f (rev_ents, rev_ss) (`Sub (sz, s1_kv_s2) :: xs)
              | `Sub (sz1, s1) :: `Ent kv :: `Sub (_sz2, s2) :: xs when sz1 < max_size_in_bytes / 2 ->
                  let s1_kv_s2 = merge s1 kv s2 in
                  let sz = size_of_view @@ view s1_kv_s2 in
                  f (rev_ents, rev_ss) (`Sub (sz, s1_kv_s2) :: xs)
              | `Sub (_sz1, s1) :: `Ent kv :: xs ->
                  f (kv::rev_ents, s1::rev_ss) xs
              | _ -> assert false
            in
            let ents, ss = f ([],[]) xs in
            match n.root, ents, ss with
            | true, [], [s] -> { s with root= true } (* 1 level less *)
            | _ -> build n.root (A.of_list ents) (Some (A.of_list ss))

  let rec split_overflows n =
    assert n.root;
    (* split subnode [n] if it is too big *)
    let rec split_node n = match n.desc with
      | Disk _ | View { index = Some _ ; _ } ->
          (* it is on disk, which means the size is ok *)
          [], [n]
      | View v ->
          let n0 = n in
          (* XXX we may have a boolean flag to tell already balanced *)
          (* rebalance the subnodes *)
          let ents, subnodes =
            match v.subnodes with
            | None -> v.ents, None
            | Some ss ->
                let kvss1 = [] :: List.map (fun kv -> [kv]) (A.to_list v.ents) in
                let kvss2, sss =
                  let ss = A.to_list ss in
                  let ss = List.map split_node ss in
                  List.split @@ ss
                in
                let kvs = List.concat @@ List.map2 (fun kvs1 kvs2 -> kvs1 @ kvs2) kvss1 kvss2 in
                let ss = List.concat sss in
                A.of_list kvs, Some (A.of_list ss)
          in

          (* ok now the subnodes are rebalanced *)
          let n = build n.root ents subnodes in
          if not @@ overflow (view n) then [], [n]
          else begin
            (* need split *)

            (* 1: rough split *)

            (* if subnodes = [], then in theory, 4096 bytes
               can carry (4096 - 2) / 6 = 682 ents

               if subnodes <> [] then in theory, i entries
               carry at least  i * (2 + 4) + 1 + (i + 1) * 4 + 4 =
               6i + 4i + 1 + 4 + 4 = 10i + 9.
               4096 bytes can carry at most (4096 - 9) / 10 = 408 ents
            *)

            let nents = A.length ents in
            Format.eprintf "Split nents=%d ...@." nents;
            let nsplits = match subnodes with
              | None -> max 2 (nents / 682 + 1)
              | Some _ -> max 2 (nents /  408 + 1)
            in
            let kvs, ss = split nsplits n in

            (* 2: ss may be still too big *)
            let kvss1 = [] :: List.map (fun kv -> [kv]) kvs in

            (* make sure the node size <= max_size_in_bytes *)
            let kvss2, sss =
              let split n = match n.desc with
                | Disk _ | View { index = Some _ ; _ } ->
                    (* it is on disk, which means the size is ok *)
                    [], [n]
                | View v when not @@ overflow v ->
                    [], [n]
                | View _ ->
                    let rec f nsplit =
                      let kvs, ss = split nsplit n in
                      if List.exists (fun n -> overflow @@ view n) ss then f (nsplit+1)
                      else kvs, ss
                    in
                    f 2
              in
              List.split @@ List.map split ss
            in

            let kvs = List.concat @@ List.map2 (fun kvs1 kvs2 -> kvs1 @ kvs2) kvss1 kvss2 in
            let ss = List.concat sss in
            let minsz = List.fold_left (fun minsz s ->
                if s.root then minsz
                else min minsz @@ size_of_view @@ view s) 4096 ss
            in
            Format.eprintf "Split nents=%d %d bytes into %d subnodes minsz= %d@."
              (A.length ents)
              (size_of_view @@ view n0)
              (List.length ss)
              minsz;
            kvs, ss
          end
    in

    match split_node n with
    | [], [n] -> { n with root= true }
    | kvs, ss ->
        let n = build true (A.of_list kvs) (Some (A.of_list ss)) in
        let v = view n in
        if overflow v then split_overflows n else n

  let init kvs =
    split_overflows
    @@ build true (Array.of_list kvs) None

  (* without rebalancing *)
  let add kseg x n =
    let rec add' kseg x n =
      match search kseg n with
      | `Found (_, x') when x = x' -> `NoChange
      | `Found (j, _) ->
          (* overwrite *)
          `Updated (update_ent n j x )
      | `Not_found j ->
          let v = view n in
          let no = match v.subnodes with
            | None ->
                Some (inject n j (kseg, x) None)
            | Some subnodes ->
                match add' kseg x (A.get subnodes j) with
                | `NoChange -> None
                | `Updated subnode ->
                    Some (update_subnode n j subnode)
          in
          match no with
          | None -> `NoChange
          | Some n -> `Updated n
    in
    if is_empty n then build true [| (kseg, x) |] None
    else
      match add' kseg x n with
      | `Updated n -> n
      | `NoChange -> n

  let adds kvs n =
    let len = List.length kvs in
    Format.eprintf "Add %d ...@." len;
    let n, secs = Utils.with_time @@ fun () ->
      split_overflows @@ fst @@ List.fold_left (fun (n,i) (k,v) ->
          if i mod 10000 = 0 then Format.eprintf "Add %d : %d done@." len i;
          add k v n, i+1) (n,0) kvs
    in
    Format.eprintf "Add %d : done in %.2f secs@." len secs;
    let (), secs = Utils.with_time @@ fun () ->List.iter (fun (k,v) ->
        match find k n with
        | None -> assert false
        | Some v' -> assert (v = v')
      ) kvs
    in
    Format.eprintf "Add %d : verified in %.2f secs@." len secs;
    n

    (*
      invaliants:
      - kvs sorted
      - forall key in n, key < kvs[0] and kvs[len-1] < key
     *)
  let bulk_adds kvs (n: t) =
    match kvs with
    | [] -> n
    | (key0, _value0) :: _ ->
       let rec find_leftmost_leaf_greator_than_key0_and_do f n =
         let v = view n in
         let i =
           (* CR jun : use bsearch *)
           match A.findi (fun (k,_) -> key0 <= k) v.ents with
           | None -> (* forall k in kvs, k < key0 *)  (* CR jun : comment is wrong i think *)
              Array.length v.ents
           | Some (_, i) -> (* kvs[i-1] <= key0 < kvs[i] *)  (* CR jun : comment is wrong i think *)
              i
         in
         (* CR jun: should check last kvs's key <= v.ents.(i)'s key *)
         begin match v.subnodes with
         | None -> f i v.ents n.root
         | Some ss ->
            assert (Array.length ss > i);
            let subnode = Array.unsafe_get ss i in
            update_subnode n i (find_leftmost_leaf_greator_than_key0_and_do f subnode)
         end
       in
       let f i ents is_root = (* ents[i-1] <= key0 and key0 < ents[i] *)
         (* CR jun : A.replace? *)
         let (left, right) =
           if i = 0 then ([||], ents)
           else if i = Array.length ents then (ents, [||])
           else (* 0 < i <= Array.length - 1 *)
             (Array.sub ents 0 i, Array.sub ents i (Array.length ents - i))
         in
         let ents = left ++ (Array.of_list kvs) ++ right in
         (* CR jun : should use build *)
         let view = { ents; subnodes = None; index = None; size = None } in
         {root = is_root; desc = View view}
       in
       find_leftmost_leaf_greator_than_key0_and_do f n
       |> split_overflows


  (* get the maximum node from [n].  If [n] is compltely empty
     it returns [None]
  *)
  let rec get_max n =
    let v = view n in
    match v.subnodes with
    | None when A.length v.ents = 0 ->
        (* This is a bottom node and completely empty! *)
        None
    | None ->
        (* v.ents <> [||] *)
        let ents = A.sub v.ents 0 (A.length v.ents - 1) in
        let maxe = A.get v.ents (A.length v.ents - 1) in
        let n = build n.root ents None in
        Some (maxe, n)
    | Some subnodes ->
        let j = A.length subnodes - 1 in
        let sj = A.get subnodes j in
        match get_max sj with
        | Some (maxe, sj) ->
            Some (maxe, update_subnode n j sj)
        | None when A.length v.ents = 0 ->
            (* m is empty and its sole subnode is also empty! *)
            None
        | None ->
            let maxe = A.get v.ents (A.length v.ents - 1) in
            let ents = A.sub v.ents 0 (A.length v.ents - 1) in
            let ss = A.sub subnodes 0 (A.length subnodes - 1) in
            Some (maxe, build n.root ents (Some ss))

  let rec delete kseg n =
    let v = view n in
    match search kseg n with
    | `Not_found j ->
        begin match v.subnodes with
        | None -> prerr_endline "warning: removing non existing element"; `Unchanged
        | Some subnodes ->
            begin match delete kseg (A.get subnodes j) with
            | `Unchanged -> `Unchanged
            | `Ok subnode -> `Ok (update_subnode n j subnode)
            end
        end
    | `Found (j, _i) ->
        match v.subnodes with
        | None ->
            (* This may make the node empty! *)
            let ents = A.replace v.ents j 1 [||] in
            `Ok (build n.root ents None)
        | Some subnodes ->
            (* find the max elem in the prev subnode
               for the alternative
            *)
            match get_max (A.get subnodes j) with
            | None ->
                (* we do not find any since the prev_subnode is completely empty!
                   we remove prev_subnode too
                *)
                let ents = A.replace v.ents j 1 [||] in
                let subnodes = A.replace subnodes j 1 [||] in
                `Ok (build n.root ents (Some subnodes))
            | Some (maxe, prev_subnode) ->
                let ents = A.replace v.ents j 1 [|maxe|] in
                let subnodes = A.replace subnodes j 1 [|prev_subnode|] in
                `Ok (build n.root ents (Some subnodes))

  let deletes ksegs n =
    let nksegs = List.length ksegs in
    Format.eprintf "Delete %d ...@." nksegs;
    let deleted = ref 0 in
    let report () = Format.eprintf "Delete %d / %d@." !deleted nksegs in
    let n, secs = Utils.with_time @@ fun () ->
      split_overflows
      @@ merge_underflows
      @@ List.fold_left (fun n kseg ->
          let n =
            match delete kseg n with
            | `Unchanged -> n
            | `Ok n' -> n'
          in
          incr deleted;
          if !deleted mod 1000 = 0 || !deleted = nksegs then report ();
          n
        ) n ksegs
    in
    let len = List.length ksegs in
    Format.eprintf "Delete %d : done in %.2f secs@." len secs;
    let (), secs = Utils.with_time @@ fun () ->List.iter (fun k ->
        match find k n with
        | None -> ()
        | Some _ -> assert false
      ) ksegs
    in
    Format.eprintf "Delete %d : verified in %.2f secs@." len secs;
    n

  let bench i n =
    let rec traverse f n =
      f n;
      let v = view n in
      match v.subnodes with
      | None -> ()
      | Some subnodes -> A.iter (traverse f) subnodes
    in
    let nnodes = ref 0 in
    let pleaves = ref 0 in
    let occupied = ref 0 in
    let underflows = ref 0 in
    traverse (fun n ->
        incr nnodes;
        let v = view n in
        pleaves := !pleaves + A.length v.ents;
        occupied := !occupied + size n;
        if size n < max_size_in_bytes / 2 then incr underflows
      ) n;
    let g = float (Obj.reachable_words (Obj.repr n)) *. float Sys.word_size /. 8_000_000_000. in
    Format.eprintf "tag: %d\tdepth: %d\tnodes: %d\tunderflows: %d\tgbs-on-disk: %.2f\tgbs-on-memory: %.2f\tpleaves/node: %.2f\tfill: %.2f@."
      i
      (depth n)
      !nnodes
      !underflows
      (float (!nnodes * max_size_in_bytes) /. 1_000_000_000.)
      g
      (float !pleaves /. float !nnodes)
      (float !occupied /. (float (!nnodes * max_size_in_bytes)))

  open Plebeia
  open Utils

  let ls kseg bits n =
    let nkseg = Key.length kseg in
    let lowest =
      Key.append
        kseg
        @@ Key.of_sides (List.init bits (fun _ -> Key.Left))
    in
    let highest =
      Key.append
        kseg
        @@ Key.of_sides @@ List.init (Key.max_length - nkseg) (fun _ -> Key.Right)
    in
    let rec f n =
      let v = view n in
      let left =
        match bsearch lowest v with
        | `Found (j, _) -> j * 2 + 1
        | `Not_found j -> j * 2
      in
      let right =
        match bsearch highest v with
        | `Found (j, _) -> j * 2 + 1
        | `Not_found j -> j * 2
      in
      let get_ent j =
        let seg, _ = A.get v.ents j in
        let a, postfix = List.split_at nkseg @@ Key.to_sides seg in
        assert (Key.to_sides kseg = a);
        let sides, _ = List.split_at bits postfix in
        if List.length sides = bits then [sides] else []
      in
      match v.subnodes with
      | None ->
          (* Tricky: if right = 0 (left most ss), then the new right must be -1 *)
          let left = left / 2 in
          let right = if right = 0 then -1 else (right - 1) / 2 in
          List.uniq_sorted (=)
          @@ List.flatten @@ List.init (right - left + 1) (fun i ->
              get_ent (left + i))
      | Some ss ->
          let targets =
            List.init (right - left + 1) (fun i ->
                let pos = i + left in
                let pos' = pos / 2 in
                if pos mod 2 = 0 then
                  `Sub (A.get ss pos')
                else `Ent (get_ent pos'))
          in
          (* skip searching subnodes if possible *)
          let rec loop = function
            | `Sub n :: xs -> `Sub (f n) :: loop xs
            | `Ent [sides1] :: `Sub _ :: `Ent [sides2] :: xs when sides1 = sides2 ->
                (* we can skip the sub *)
                (* XXX we can also skip if sides1 + 1 = sides... *)
                `Ent [sides1] :: loop xs
            | `Ent sidess :: `Sub n :: xs ->
                `Ent sidess :: `Sub (f n) :: loop xs
            | [] -> []
            | _ -> assert false
          in
          List.uniq_sorted (=)
          @@ List.concat_map (function
              | `Sub xs -> xs
              | `Ent xs -> xs)
          @@ loop targets
    in
    f n


  (* { (seg,v) | (src++seg,v) \in n } *)
  let find_under src n =
    let nsrc = Key.length src in
    let lowest = src in
    let highest =
      Key.append
        src
        @@ Key.of_sides @@ List.init (Key.max_length - nsrc) (fun _ -> Key.Right)
    in
    let rec f n =
      let v = view n in
      let left =
        match bsearch lowest v with
        | `Found (j, _) -> j * 2 + 1
        | `Not_found j -> j * 2
      in
      let right =
        match bsearch highest v with
        | `Found (j, _) -> j * 2 + 1
        | `Not_found j -> j * 2
      in
      let get_ent j =
        let seg, x = A.get v.ents j in
        let a, postfix = List.split_at nsrc @@ Key.to_sides seg in
        assert (Key.to_sides src = a);
        [postfix, x]
      in
      match v.subnodes with
      | None ->
          (* Tricky: if right = 0 (left most ss), then the new right must be -1 *)
          let left = left / 2 in
          let right = if right = 0 then -1 else (right - 1) / 2 in
          List.flatten @@ List.init (right - left + 1) (fun i ->
              get_ent (left + i))
      | Some ss ->
          List.concat
          @@ List.init (right - left + 1) (fun i ->
              let pos = i + left in
              let pos' = pos / 2 in
              if pos mod 2 = 0 then f (A.get ss pos')
              else get_ent pos')
    in
    f n

  let copy srcseg dstseg n =
    let src = find_under srcseg n in
    let src = List.map (fun (sides, x) ->
        let seg = Key.append dstseg @@ Key.of_sides sides in
        (seg, x)) src
    in
    adds src n

  let rm srcseg n =
    let src = find_under srcseg n in
    let src = List.map (fun (sides, _) ->
        Key.append srcseg @@ Key.of_sides sides) src in
    let n = deletes src n in
    n, List.length src
end
