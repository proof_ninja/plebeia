open Plebeia
open Utils

(************************************* storage *)

module TStorage = Gstorage.Make(struct
    let head_string =
      let s = "PLEBEIA TRIBUNE" in
      s ^ String.make (24 - String.length s) '\000'

    let version = 1 (* 0.1 *)

    let max_index = Index.max_int
    (* No tags required, therefore we can use upto [max_int]. *)

    let bytes_per_cell = 4096
  end)

(************************************* array *)

module A = struct
  include Array

  (* O(n), where n is the size of the array. *)
  let replace a pos len xs =
    if length a = 0 then xs
    else
      try
        let a' = make (length a - len + length xs) (unsafe_get a 0) in
        blit a 0 a' 0 pos;
        blit xs 0 a' pos (length xs);
        blit a (pos+len) a' (pos + length xs) (length a - pos - len);
        a'
      with
      | e ->
          Format.eprintf "replace len=%d pos=%d len=%d@."
            (length a) pos len;
          raise e

  let (++) = append
end

let (++) = A.(++)

(************************************* loader/saver *)

module C = Plebeia.Xcstruct

type 'a loader = C.t -> int -> int * 'a
type 'a saver = C.t -> int -> 'a -> int

let load_nlist (f : 'a loader) cstr i n =
  let rec loop st i j =
    if j = n then i, List.rev st
    else
      let i, x = f cstr i in
      loop (x::st) i (j+1)
  in
  loop [] i 0

let load_list (f : 'a option loader) cstr i =
  let rec loop st i =
    match f cstr i with
    | i, None -> i, List.rev st
    | i, Some x -> loop (x::st) i
  in
  loop [] i

let save_nlist (f : 'a saver) cstr i xs =
  List.fold_left (fun i x -> f cstr i x) i xs

(* with terminator *)
let save_list (f : 'a option saver) cstr i xs =
  let i = List.fold_left (fun i x -> f cstr i (Some x)) i xs in
  f cstr i None

(************************************* key (Segment.t) *)

module Key = struct
  include Segment
  let size s = Segment.length s / 8 + 1

  let load : t option loader = fun cstr i ->
    let bytes = C.get_uint8 cstr i in
    let i = i + 1 in
    if bytes = 0 then i, None
    else
      let seg =
        let segstr = C.copy cstr i bytes in
        match Serialization.decode segstr with
        | Some seg -> seg
        | None -> assert false (* XXX *)
      in
      i + bytes, Some seg

  let save : t option saver = fun cstr i -> function
    | None ->
        C.set_uint8 cstr i 0;
        i + 1
    | Some k ->
        let sz = size k in
        assert (sz < 256);
        C.set_uint8 cstr i sz;
        let i = i + 1 in
        let enc = Serialization.encode k in
        let l = String.length enc in
        assert (l = sz);
        C.blit_from_string enc 0 cstr i l;
        i + l

  let test () =
    Test_utils.with_random @@ fun rng ->
    let cstr = Cstruct.create 1_000_000 in
    let segs = Gen.(list (return 300) @@ segment (int_range (0, 300))) rng in
    let i = save_list save cstr 0 segs in
    assert (i < 1_000_000);
    let j, segs' = load_list load cstr 0 in
    assert (i = j);
    assert (List.length segs = List.length segs');
    assert (List.for_all2 Segment.equal segs segs')
end

(******************************** value (Index.t) *)

module Value = struct
  include Index

  let size = 4 (* fixed *)

  let load : t loader = fun cstr i ->
    let open Stdlib in
    let idx = of_uint32 @@ C.get_uint32 cstr i in
    i + 4, idx

  let save : t saver = fun cstr i v ->
    let open Stdlib in
    C.set_uint32 cstr i (to_uint32 v);
    i + 4

  let test () =
    Test_utils.with_random @@ fun rng ->
    let cstr = Cstruct.create 1_000_000 in
    let idxs = Gen.(list (return 300) index) rng in
    let i = save_nlist save cstr 0 idxs in
    assert (i < 1_000_000);
    let j, idxs' = load_nlist load cstr 0 (List.length idxs) in
    assert (i = j);
    assert (List.length idxs = List.length idxs');
    assert (idxs = idxs')
end

(* set of segments *)
module SS (P : sig val check : bool end) = struct
  include Segment_set

  let save cstr i keys =
    let res = save cstr i keys in
    if P.check then begin
      let res', keys' = load cstr i in
      assert (res = res' && equal keys keys')
    end;
    res

end

(************************************* B+ *)

module Make
    (P : sig
       val storage : TStorage.t
       (* fixes storage per module instance to avoid
          typing storage argument for all the APIs *)

       val max_size_in_bytes : int
       (* should be 4096 but can be smaller in tests *)

       val check : bool
       (* if [true], invariants and other health checks are performed *)
     end)
= struct

  module SS = SS(P)
  module A = struct
    include A

    (* Go unsafe unless P.check! *)
    let get = if P.check then get else unsafe_get
    let set = if P.check then set else unsafe_set
  end

  let () = Format.eprintf "Tribune check %b@." P.check

  let max_size_in_bytes = P.max_size_in_bytes

  type key = Key.t
  type value = Value.t

  type leaf =
    { keys : SS.t
    ; values : value array (* nvalues = nkeys *)
    ; mutable index : Index.t option
    ; mutable size : [`Exactly of int | `Overflow] option
    }

  type t =
    { root : bool
    ; mutable desc : desc
    (* physically shared nodes are loaded/forgot at the same time
       by mutation
    *)
    }

  and desc =
    | Disk of Index.t
    | View of view

  and view =
    | Internal of internal
    | Leaf of leaf

  and internal =
    { keys : SS.t
    ; subnodes : t array
    (* nsubnodes = nkeys+1

       A binding of keys.(i), if it exists, found in "left" i.e. subnodes.(i) *)
    ; mutable index : Index.t option
    ; mutable size : [`Exactly of int | `Overflow] option
    (* Size on disk using Compressed_keys in bytes.
       The size needs not be exact for overflown nodes.
    *)
    }

  let index t = match t.desc with
    | Disk i -> Some i
    | View (Leaf { index ; _ }) -> index
    | View (Internal { index ; _ }) -> index

  let of_index i = { root= true; desc= Disk i }

  let size_of_internal inter =
    match inter.size with
    | Some (`Exactly x) -> x
    | _ ->
        let sz =
          let nkeys = SS.length inter.keys in
          if nkeys = 0 then
            1 (* internal *)
            + 1 (* ent null *)
            + A.length inter.subnodes * Value.size (* subnodes *)
          else
            1 + (* internal *)
            + SS.size inter.keys
            + A.length inter.subnodes * Value.size
            (* XXX subnodes null is not required since the number of subnodes are known from the num of entries *)
        in
        inter.size <- Some (`Exactly sz);
        sz

  (* Stop summing up when the current sum exceeds max_size_in_bytes *)
  let size_of_internal' inter =
    match inter.size with
    | Some res -> res
    | _ ->
        let fixed =
          1 + (* internal *) + A.length inter.subnodes * Value.size (* subnodes *)
        in
        if fixed > max_size_in_bytes then `Overflow
        else
        if fixed > max_size_in_bytes then `Overflow
        else
          let margin = max_size_in_bytes - fixed in
          match SS.size_margin margin inter.keys with
          | None -> `Overflow
          | Some margin -> `Exactly (max_size_in_bytes - margin)

  let size_of_leaf (leaf : leaf) =
    match leaf.size with
    | Some (`Exactly x) -> x
    | _ ->
        let sz =
          1 (* leaf *)
          + SS.size leaf.keys
          + SS.length leaf.keys * Value.size (* values *)
        in
        leaf.size <- Some (`Exactly sz);
        sz

  (* Stop summing up when the current sum exceeds max_size_in_bytes *)
  let size_of_leaf' (leaf : leaf) =
    match leaf.size with
    | Some res -> res
    | _ ->
        let fixed =
          1 (* leaf *)
          + SS.length leaf.keys * Value.size (* value *)
        in
        if fixed > max_size_in_bytes then `Overflow
        else
          let margin = max_size_in_bytes - fixed in
          match SS.size_margin margin leaf.keys with
          | None -> `Overflow
          | Some margin -> `Exactly (max_size_in_bytes - margin)

  let loaded = ref 0

  let load_view idx =
    let cstr = TStorage.get_cell P.storage idx in
    let n = C.get_uint8 cstr 0 in
    let i = 1 in
    let res = match n with
      | 0 -> (* internal *)
          let i, keys = SS.load cstr i in
          let i, subnodes = load_nlist Value.load cstr i (SS.length keys + 1) in
          (* XXX check i and size *)
          let subnodes = A.of_list (List.map (fun i -> { root= false; desc= Disk i }) subnodes) in
          Internal { keys
                   ; subnodes
                   ; index= Some idx
                   ; size= Some (`Exactly i)
                   }
      | 1 -> (* leaf *)
          let i, keys = SS.load cstr i in
          let i, values = load_nlist Value.load cstr i (SS.length keys) in
          (* XXX check i and size *)
          let values = A.of_list values in
          assert (SS.length keys = A.length values);
          Leaf { keys
               ; values
               ; index= Some idx
               ; size= Some (`Exactly i)
               }
      | _ -> assert false (* XXX proper failure *)
    in
    incr loaded;
    if !loaded mod 10000 = 0 then
      Format.eprintf "total loaded tribune nodes: %d@." !loaded;
    res

  let saved = ref 0

  let save_internal v =
    let sz = size_of_internal v in
    assert (sz <= max_size_in_bytes);

    let module C = Plebeia.Xcstruct in
    let idx = TStorage.new_index P.storage in
    let cstr = TStorage.get_cell P.storage idx in
    let i = 0 in
    C.set_uint8 cstr i 0;
    let i = 1 in
    let i = SS.save cstr i v.keys in

    let j = i in

    let ss = v.subnodes in
    let ss =
      List.map (function
          | {desc= ( Disk i
                   | View (Internal { index= Some i ; _ })
                   | View (Leaf { index= Some i ; _ }) ) ; _ } -> i
          | _ -> assert false)
      @@ A.to_list ss
    in
    let i = save_nlist Value.save cstr i ss in

    if P.check then
      if size_of_internal v <> i then begin
        Format.eprintf "size_of_internal %d i %d@." sz i;
        Format.eprintf "keys %d  j %d@."
          (Seq.fold_left (fun sum k ->
               sum + Key.size k + 1) 1 @@ SS.sorted_seq v.keys)
          j;
        assert false
      end;

    v.index <- Some idx;
    incr saved

  let save_leaf (v : leaf) =
    let sz = size_of_leaf v in
    assert (sz <= max_size_in_bytes);

    let module C = Plebeia.Xcstruct in
    let idx = TStorage.new_index P.storage in
    let cstr = TStorage.get_cell P.storage idx in
    let i = 0 in
    C.set_uint8 cstr i 1;
    let i = 1 in
    let i = SS.save cstr i v.keys in
    let i = save_nlist Value.save cstr i (A.to_list v.values) in

    let j = i in

    if P.check then
      if size_of_leaf v <> i then begin
        Format.eprintf "size_of_leaf %d i %d@." sz i;
        Format.eprintf "keys %d  j %d@."
          (Seq.fold_left (fun sum k ->
               sum + Key.size k + 1 + 4) 1 @@ SS.sorted_seq v.keys)
          j;
        assert false
      end;

    v.index <- Some idx;
    incr saved

  let may_forget n =
    let desc = match n.desc with
      | View (Internal { index= Some i; _ }) -> Disk i
      | View (Leaf { index= Some i; _ }) -> Disk i
      | desc -> desc
    in
    n.desc <- desc

  let view n = match n.desc with
    | View v -> v
    | Disk i ->
        let v = load_view i in
        n.desc <- View v;
        v

  (* May access to the disk *)
  let size_with_load n = match view n with
    | Internal v -> size_of_internal v
    | Leaf v -> size_of_leaf v

  let size_without_load n = match n.desc with
    | Disk _ -> None
    | View _ -> Some (size_with_load n)

  let overflow n = match n.desc with
    | Disk _ -> false
    | View (Internal { index= Some _ ; _ } | Leaf { index= Some _ ; _ }) ->
        (* if it is on the disk, it is not overflown *)
        if P.check then
          assert (from_Some (size_without_load n) <= max_size_in_bytes);
        false
    | View (Internal v) ->
        begin match size_of_internal' v with
          | `Overflow -> true
          | `Exactly sz -> sz > max_size_in_bytes
        end
    | View (Leaf v) ->
        begin match size_of_leaf' v with
          | `Overflow -> true
          | `Exactly sz -> sz > max_size_in_bytes
        end

  let rec save node =
    match node.desc with
    | Disk _ | View(Internal { index= Some _; _ } | Leaf { index = Some _; _ }) -> ()
    | View (Internal ({ subnodes= ss; _ } as v)) ->
        A.iter save ss;
        save_internal v
    | View (Leaf v) -> save_leaf v

  (* only for small data *)
  let rec pp ppf t =
    let open Format in
    fprintf ppf
      "{ @[root=%b@ %a@] }"
      t.root
      (fun ppf -> function
         | Disk i -> fprintf ppf "Disk %a" Index.pp i
         | View (Internal v) ->
             fprintf ppf "{ @[keys=@[%a@]@ subnodes=[ @[%a@]]@] }"
               (list ";@ " (fun ppf s ->
                    fprintf ppf "%a"
                      Key.pp s))
               (SS.sorted_list v.keys)
               (list "; @ " pp) (A.to_list v.subnodes)
         | View (Leaf v) ->
             fprintf ppf "@[keys=@[%a@]@]"
               (list ";@ " (fun ppf (s, i) ->
                    fprintf ppf "%a %a"
                      Key.pp s Value.pp i))
               (List.combine (SS.sorted_list v.keys) (A.to_list v.values)))
      t.desc

  let rec elements n =
    match view n with
    | Internal { subnodes ; _ } ->
        List.concat_map elements (A.to_list subnodes)
    | Leaf { keys; values; _ } ->
        List.combine (SS.sorted_list keys) (A.to_list values)

  let elems = elements

  let rec nelems n =
    match view n with
    | Internal { subnodes ; _ } ->
        A.fold_left (fun sum s -> sum + nelems s) 0 subnodes
    | Leaf { keys; _ } -> SS.length keys

  let empty =
    { root= true;
      desc= View (Leaf { keys = SS.empty; values= [||]; index= None; size= None })
    }

  let is_empty_view = function
    | Leaf v -> SS.is_empty v.keys
    | _ -> false

  let is_empty n = is_empty_view @@ view n

  (* Check as many things as possible without any loading.

     We skip some tests when nkeys is very large.
     Things will be checked properly when they are split into much
     smaller nodes before saving to the disk.

     We do not check size conditions here, since we allow overflow/underflow nodes in memory.

     All the leaves must be at the same depth, but this is not checked by [validate].
  *)
  let validate n =
    if not P.check then n
    else begin
      match n.desc with
      | Disk _ -> n
      | View (Internal { keys; subnodes; _ }) ->
          begin
            let keys = A.of_seq @@ SS.sorted_seq keys in
            let nkeys = A.length keys in
            let nsubnodes = A.length subnodes in
            assert (nkeys + 1 = nsubnodes);

            (* nkeys can be 0! *)

            if nkeys < 1000 then begin
              (* subnodes are all Leaf or all Internal *)
              let nleaves =
                A.fold_left (fun nleaves s ->
                    match view s with
                    | Internal _ -> nleaves
                    | Leaf _ -> nleaves + 1) 0 subnodes
              in
              assert (nleaves = 0 || nleaves = A.length subnodes);

              (* keys must be sorted *)
              for i = 0 to nkeys - 2 do
                let seg  = A.get keys i in
                let seg' = A.get keys (i+1) in
                assert (Key.compare seg seg' = -1)
              done;

              (* ordering of the keys of subnodes and n *)
              for i = 0 to nkeys do
                match (A.get subnodes i).desc with
                | Disk _ -> () (* we skip Disk for speed *)
                | View (Internal subv) ->
                    let subkeys = A.of_seq @@ SS.sorted_seq @@ subv.keys in
                    let nsubkeys = A.length subkeys in
                    if nsubkeys = 0 then ()
                    else begin
                      let subkeys_smallest = A.get subkeys 0 in
                      let subkeys_largest = A.get subkeys (A.length subkeys - 1) in
                      let prev_largest =
                        if i = 0 then None else Some (A.get keys (i-1))
                      in
                      let next_smallest =
                        if i = nkeys then None else Some (A.get keys i)
                      in
                      begin match prev_largest with
                        | None -> ()
                        | Some prev_largest -> assert (Key.compare prev_largest subkeys_smallest = -1)
                      end;
                      begin match next_smallest with
                        | None -> ()
                        | Some next_smallest -> assert (Key.compare subkeys_largest next_smallest = -1)
                      end;
                    end

                | View (Leaf subv) ->
                    let subkeys = subv.keys in
                    let nsubkeys = SS.length subkeys in
                    if nsubkeys = 0 then ()
                    else begin
                      let subkeys_smallest = SS.nth_exn 0 subkeys in
                      let subkeys_largest = SS.nth_exn (nsubkeys - 1) subkeys in
                      let kprev =
                        if i = 0 then None else Some (A.get keys (i-1))
                      in
                      let knext =
                        if i = nkeys then None else Some (A.get keys i)
                      in
                      begin match kprev with
                        | None -> ()
                        | Some kprev ->
                            (* If exists, kprev binding MUST be found in subv *)
                            assert (Key.compare kprev subkeys_smallest <= 0)
                      end;
                      begin match knext with
                        | None -> ()
                        | Some knext ->
                            (* If exists, knext binding MUST NOT be found in subv *)
                            assert (Key.compare subkeys_largest knext = -1)
                      end;
                    end
              done;
            end;

            n
          end

      | View (Leaf { keys; _ } as v) ->
          let keys = A.of_seq @@ SS.sorted_seq keys in
          if is_empty_view v then ()
          else begin
            let nkeys = A.length keys in
            (* nkeys can be 0! *)
            if nkeys < 1000 then
              (* keys must br sorted *)
              for i = 0 to nkeys - 2 do
                let seg  = A.get keys i in
                let seg' = A.get keys (i+1) in
                assert (Key.compare seg seg' = -1)
              done;
          end;
          n
    end

  let build_leaf root keys values =
    let n ={ root;
             desc= View (Leaf { keys; values; index= None; size= None }) }
    in
    validate n


  let build_internal root keys subnodes =
    let n ={ root;
             desc= View (Internal { keys; subnodes; index= None; size= None }) } in
    validate n

  let update_leaf_value n i x = match view n with
    | Internal _ -> assert false
    | Leaf leaf ->
        assert (0 <= i && i < SS.length leaf.keys);
        let values = A.copy leaf.values in
        A.set values i x;
        (* This only replace the value at i.  Invariants are never broken *)
        { n with desc= View (Leaf { keys= leaf.keys
                                  ; values
                                  ; index= None
                                  ; size= leaf.size }) }

  let update_internal_subnode n j subnode = match view n with
    | Leaf _ -> assert false
    | Internal inter ->
        (* inter.keys are shared *)
        let subnodes = inter.subnodes in
        let subnodes = A.copy subnodes in
        A.set subnodes j subnode;
        build_internal n.root inter.keys subnodes

  (* XXX use bin tree *)
  let inject_in_leaf n j (kseg, x) = match view n with
    | Internal _ -> assert false
    | Leaf leaf ->
        let keys = SS.add kseg leaf.keys in
        let values = A.replace leaf.values j 0 [|x|] in
        build_leaf n.root keys values

  let rec depth n =
    match view n with
    | Leaf _ -> 1
    | Internal { subnodes; _ } -> depth (A.get subnodes 0) + 1

(*
  let bsearch kseg a =
    let nleaves = A.length a in
    (* nleaves can be 0 *)
    if nleaves = 0 then `Left_of 0
    else
      let get = A.get a in
      let check j =
        let kj = get j in
        let comp = Key.compare kseg kj in
        kj, comp
      in
      let rec bsearch (l,r) =
        let j = (l + r) / 2 in
        match check j with
        | i, 0 -> `Found (j, i)
        | _, -1 ->
            if j = l then `Left_of l
            else bsearch (l,j-1)
        | _, 1 ->
            if j = r then `Left_of (r+1)
            else bsearch (j+1,r)
        | _ -> assert false
      in
      bsearch (0, nleaves-1)
*)

  let rec find kseg n =
    match view n with
    | Internal { keys; subnodes; _ } ->
        begin match SS.search kseg keys with
          | `Before j ->
              find kseg @@ A.get subnodes j
          | `Exact j ->
              (* leaf key same as an internal key is found
                 in the RIGHT of the internal key *)
              find kseg @@ A.get subnodes (j+1)
        end
    | Leaf { keys; values; _ } ->
        Option.map (A.get values) (SS.find kseg keys)

  (* check [n] has [kvs] *)
  let has_bindings n kvs =
    let kvs' = elements n in
    List.length kvs = List.length kvs' &&
    let rec f = function
      | [] -> true
      | (k,v)::kvs ->
          match find k n with
          | Some v' when v = v' -> f kvs
          | _ -> false
    in
    f kvs'

  let split_internal x inter =
    let split_array ats sz =
      let ranges =
        let rec f start = function
          | [] -> [(start, sz - 1)]
          | at::ats -> (start, at-1) :: f (at+1) ats
        in
        f 0 ats
      in
      ats, ranges
    in
    let split_set ats ss =
      let rec f ats ss offset =
        match ats with
        | [] -> [ss]
        | at::ats ->
            let length = at - offset in
            let (ssl, ssr) = SS.split_at length ss in
            let (_, ssr) = Option.from_Some @@ SS.pop_nth 0 ssr in
            ssl :: f ats ssr (offset + length + 1)
      in f ats ss 0
    in
    assert (x >= 2);
    let nkeys = SS.length inter.keys in
    let points = List.init (x-1) (fun i -> nkeys * (i+1) / x) in
    let ups, downs = split_array points (SS.length inter.keys) in
    let ups = List.map (fun i -> SS.nth_exn i inter.keys) ups in
    let downs_keys = split_set points inter.keys in
    let downs =
      List.map (fun (start, end_) ->
          A.sub inter.subnodes start (end_ - start + 2)
        ) downs
    in
    let downs = List.map2 (build_internal false) downs_keys downs in
    ups, downs

  let split_leaf x (leaf : leaf) =
    assert (x >= 2);
    let split_array ats sz =
      let ranges =
        let rec f start = function
          | [] -> [(start, sz - 1)]
          | at::ats -> (start, at-1) :: f at ats
        in
        f 0 ats
      in
      ats, ranges
    in
    let split_set ats ss =
      let rec f ats ss offset =
        match ats with
        | [] -> [ss]
        | at::ats ->
            let length = at - offset in
            let (ssl, ssr) = SS.split_at length ss in
            ssl :: f ats ssr (offset + length)
      in f ats ss 0
    in
    let nkeys = SS.length leaf.keys in
    (* split at the lefts of the points *)
    let points = List.init (x-1) (fun i -> nkeys * (i+1) / x) in
    let ups, downs = split_array points (SS.length leaf.keys) in
    (*
       org:    k1 k2 ... ki-1 ki ki+1 ...
               v1 v2 ... vi-1 vi vi+1 ...

       at: [i]

       ups:                ki
       downs:   k1 .. ki-1    ki ...
                v1 .. vi-1    vi ...
    *)
    let ups =
      (* We need not use key.(i) as is *)
      List.map (fun i ->
          assert (i > 0);
          let ka = SS.nth_exn (i - 1) leaf.keys in
          let kb = SS.nth_exn (i) leaf.keys in
          let common, _kapostfix, kbpostfix = Key.common_prefix ka kb in
          let k =
            match Segment.cut kbpostfix with
            | None -> assert false (* impossible *)
            | Some (side, _) -> Key.(append common (of_sides [side]))
          in
          if P.check then
            if not (Segment.compare ka k < 0 && Segment.compare k kb <= 0) then begin
              Format.eprintf "ka=%a@." Segment.pp ka;
              Format.eprintf "kb=%a@." Segment.pp kb;
              Format.eprintf "k =%a@." Segment.pp k;
              assert false
            end;
          k
        ) ups
    in
    let downs_keys = split_set points leaf.keys in
    let downs_values =
      List.map (fun (start, end_) ->
          A.sub leaf.values start (end_ - start + 1)
        ) downs
    in
    let downs = List.map2 (build_leaf false) downs_keys downs_values in
    ups, downs

  let split_node x n = match view n with
    | Internal inter -> split_internal x inter
    | Leaf leaf -> split_leaf x leaf

  (* merge underflows of subnodes of n

     XXX if the entries under [n] are very small, it is possible
     that subnode underflow is never resolved by this function.
     In that case the result has 0 keys and 1 underflow subnode.
  *)
  let rec merge_underflows n =
    match n.desc with
    | Disk _ | View (Leaf _) | View (Internal { index= Some _; _ }) -> n
    | View (Internal v) ->
        let ss = v.subnodes in
        (* XXX this still loads many nodes! *)
        let ss = A.map merge_underflows ss in
        let kss = None :: List.map (fun k -> Some k) (SS.sorted_list v.keys) in
        let ss = List.map (fun s ->
            let cap sz = match sz with
              | `Exactly x -> x
              | `Overflow -> max_size_in_bytes
            in
            let sz = match s.desc with
              | Disk _ | View (Internal { index= Some _; _ } | Leaf { index= Some _; _ }) ->
                  (* fixated already.  not to try merge with it as possible *)
                  max_size_in_bytes (* dummy *)
              | View (Internal v) -> cap @@ size_of_internal' v
              | View (Leaf v) -> cap @@ size_of_leaf' v
            in
            sz, s) @@ A.to_list ss
        in
        let xs =
          List.concat @@ List.map2 (fun k s ->
              match k with
              | None -> [`Sub s]
              | Some k -> [`Ent k; `Sub s]
            ) kss ss
        in
        let merge s1 k s2 =
          let vs1 = view s1 in
          let vs2 = view s2 in
          match vs1, vs2 with
          | Internal vs1, Internal vs2 ->
              let keys = SS.add k @@ SS.union vs1.keys vs2.keys in
              let ss = vs1.subnodes ++ vs2.subnodes in
              let n = build_internal n.root keys ss in
              Format.eprintf "Merging %d %d => %d@."
                (size_of_internal vs1)
                (size_of_internal vs2)
                (from_Some @@ size_without_load n);
              n
          | Leaf vs1, Leaf vs2 ->
              let keys = SS.union vs1.keys vs2.keys in
              let values = vs1.values ++ vs2.values in
              let n = build_leaf n.root keys values in
              Format.eprintf "Merging %d %d => %d@."
                (size_of_leaf vs1)
                (size_of_leaf vs2)
                (from_Some @@ size_without_load n);
              n
          | _ -> assert false
        in
        let rec f (rev_keys, rev_ss) = function
          | [] -> List.rev rev_keys, List.rev rev_ss
          | `Sub (sz, s) :: [] ->
              (* XXX If it is the sole element, it is left unmerged even if it underflows *)
              if sz < max_size_in_bytes / 2 then
                Format.eprintf "Merging warn: small but not merged %d@." sz;
              f (rev_keys, s :: rev_ss) []
          | `Sub (_sz1, s1) :: `Ent k :: `Sub (sz2, s2) :: [] when sz2 < max_size_in_bytes / 2 ->
              let s1_s2 = merge s1 k s2 in
              f (rev_keys, s1_s2 :: rev_ss) []
          | `Sub (sz1, s1) :: `Ent k :: `Sub (sz2, s2) :: (`Ent _ :: `Sub (sz3, _s3) :: _ as xs)  when sz2 < max_size_in_bytes / 2 && sz1 < sz3 ->
              let s1_s2 = merge s1 k s2 in
              let sz = from_Some @@ size_without_load s1_s2 in
              f (rev_keys, rev_ss) (`Sub (sz, s1_s2) :: xs)
          | `Sub (sz1, s1) :: `Ent k :: `Sub (_sz2, s2) :: xs when sz1 < max_size_in_bytes / 2 ->
              let s1_kv_s2 = merge s1 k s2 in
              let sz = from_Some @@ size_without_load s1_kv_s2 in
              f (rev_keys, rev_ss) (`Sub (sz, s1_kv_s2) :: xs)
          | `Sub (_sz1, s1) :: `Ent kv :: xs ->
              f (kv::rev_keys, s1::rev_ss) xs
          | _ -> assert false
        in
        let keys, ss = f ([],[]) xs in
        match n.root, keys, ss with
        | true, [], [s] -> merge_underflows @@ { s with root= true } (* 1 level less *)
        | _ -> build_internal n.root (SS.of_list keys) (A.of_list ss)

  (* i entries carry at least

     1 + i * 2 + 1 + (i + 1) * 4 + 4 =
     2i + 4i + 1 + 1 + 4 + 4 = 6i + 10.
     4096 bytes can carry at most (4096 - 10) / 6 = 681 keys
  *)
  let rough_splits_internal = (max_size_in_bytes - 10) / 6

  (* in theory, 4096 bytes can carry (4096 - 2) / 6 = 682 keys
  *)
  let rough_splits_leaf = (max_size_in_bytes - 2) / 6

  let rec split_overflows n =
    assert n.root;
    let rec deep_split n = match n.desc with
      | Disk _ ->
          (* it is on disk, which means the size is ok *)
          [], [n]
      | View (Internal { index = Some _ ; _ } | Leaf { index = Some _ ; _ }) ->
          (* it is on disk, which means the size is ok *)
          if P.check then
            assert (from_Some @@ size_without_load n <= max_size_in_bytes);
          [], [n]

      | View (Internal v) ->
          let n0 = n in
          (*
              __jkl__
             /_______\

             =>
                   k
              __j     l__
             /___\   /___\
          *)
          (* rebalance the subnodes *)
          let keys, subnodes =
            let kss1 = [] :: List.map (fun k -> [k]) (SS.sorted_list v.keys) in
            let kss2, sss =
              let ss = A.to_list v.subnodes in
              let ss = List.map deep_split ss in
              List.split ss
            in
            let ks = List.concat @@ List.map2 (fun ks1 ks2 -> ks1 @ ks2) kss1 kss2 in
            let ss = List.concat sss in
            SS.of_list ks, A.of_list ss
          in

          (* ok now the subnodes are rebalanced *)
          let n = build_internal n.root keys subnodes in
          if not @@ overflow n then [], [n]
          else begin
            let v = match view n with
              | Internal v -> v
              | _ -> assert false
            in
            (* need split *)

            (* 1: rough split *)

            let nkeys = SS.length keys in
            (* Format.eprintf "Split nkeys=%d ...@." nkeys; *)
            let nsplits = max 2 (nkeys / rough_splits_internal + 1) in
            let ks, ss = split_internal nsplits v in

            (* 2: ss may be still too big *)
            let kss1 = [] :: List.map (fun k -> [k]) ks in

            (* make sure the node size <= max_size_in_bytes *)
            let kss2, sss =
              let split n =
                if not @@ overflow n then
                  [], [n]
                else
                  let rec f nsplit =
                    let ks, ss = match view n with
                      | Internal v -> split_internal nsplit v
                      | Leaf v -> split_leaf nsplit v
                    in
                    if List.exists overflow ss then f (nsplit+1)
                    else ks, ss
                  in
                  f 2
              in
              List.split @@ List.map split ss
            in

            let ks = List.concat @@ List.map2 (fun ks1 ks2 -> ks1 @ ks2) kss1 kss2 in
            let ss = List.concat sss in
            let minsz, maxsz = List.fold_left (fun (minsz, maxsz) s ->
                (* This is for information.  We do not load Disks for that *)
                match size_without_load s with
                | Some sz -> (min minsz sz, max maxsz sz)
                | None -> (minsz, maxsz)
              ) (max_size_in_bytes, 0) ss
            in
            Format.eprintf "Split internal nkeys= %d %d bytes into %d subnodes minsz= %d maxsz= %d@."
              (SS.length keys)
              (from_Some @@ size_without_load n0)
              (List.length ss)
              minsz
              maxsz;
            ks, ss
          end

      | View (Leaf v) ->
          let n0 = n in
          (* XXX we may have a boolean flag to tell already balanced *)
          (* rebalance the subnodes *)
          if not @@ overflow n then begin
            if P.check then
              assert (from_Some (size_without_load n) <= max_size_in_bytes);
            [], [n]
          end else begin
            (* need split *)

            (* 1: rough split *)

            let nkeys = SS.length v.keys in
            (* Format.eprintf "Split nkeys=%d ...@." nkeys; *)
            let nsplits = max 2 (nkeys / rough_splits_leaf + 1) in
            let ks, ss = split_leaf nsplits v in

            (* 2: ss may be still too big *)
            let kss1 = [] :: List.map (fun k -> [k]) ks in

            (* make sure the node size <= max_size_in_bytes *)
            let kss2, sss =
              let split n =
                if not @@ overflow n then [], [n]
                else
                  let rec f nsplit =
                    let ks, ss = split_node nsplit n in
                    if List.exists overflow ss then f (nsplit+1)
                    else ks, ss
                  in
                  f 2
              in
              List.split @@ List.map split ss
            in

            let ks = List.concat @@ List.map2 (fun ks1 ks2 -> ks1 @ ks2) kss1 kss2 in
            let ss = List.concat sss in
            let minsz, maxsz = List.fold_left (fun (minsz, maxsz) s ->
                (* This is for information.  We do not load Disks for that *)
                match size_without_load s with
                | Some sz -> (min minsz sz, max maxsz sz)
                | None -> (minsz, maxsz)
              ) (max_size_in_bytes, 0) ss
            in
            Format.eprintf "Split leaf     nkeys= %d %d bytes into %d subnodes minsz= %d maxsz= %d@."
              (SS.length v.keys)
              (size_with_load n0)
              (List.length ss)
              minsz
              maxsz;
            ks, ss
          end
    in

    match deep_split n with
    | [], [n] -> { n with root= true }
    | ks, ss ->
        let n = build_internal true (SS.of_list ks) (A.of_list ss) in
        if overflow n then split_overflows n else n

  let rebalance n =
    prerr_endline "Rebalancing...";
    let n, secs = Utils.with_time @@ fun () ->
      split_overflows
      @@ merge_underflows n
    in
    Format.eprintf "Rebalancing done in %.2f secs@." secs;
    n

  let init' ks vs = split_overflows @@ build_leaf true ks vs
  let init kvs =
    let ks, vs = List.split kvs in
    split_overflows @@
    build_leaf true (SS.of_list ks) (A.of_list vs)

  (* without rebalancing *)
  let add kseg x n =
    let rec add' kseg x n =
      let v = view n in
      match v with
      | Leaf { keys; values; _ } ->
          begin match SS.search kseg keys with
            | `Exact j when A.get values j = x -> `NoChange
            | `Exact j ->
                (* overwrite *)
                `Updated (update_leaf_value n j x)
            | `Before j ->
                `Updated (inject_in_leaf n j (kseg, x))
          end
      | Internal { keys; subnodes; _ } ->
          let j = match SS.search kseg keys with
            | `Exact j' -> j' + 1 | `Before j -> j
          in
          let no =
            match add' kseg x (A.get subnodes j) with
            | `NoChange -> None
            | `Updated subnode ->
                Some (update_internal_subnode n j subnode)
          in
          match no with
          | None -> `NoChange
          | Some n -> `Updated n
    in
    if is_empty n then build_leaf true (SS.of_list [kseg]) [| x |]
    else
      match add' kseg x n with
      | `Updated n -> n
      | `NoChange -> n

  let adds kvs t = List.fold_left (fun t (k,v) -> add k v t) t kvs

  let bulk_adds kvs node =
    match kvs with
    | [] -> node
    | _ ->
        let rec length_upto n st = function
          | [] -> st
          | _::xs ->
              let st = st + 1 in
              if st = n then n
              else length_upto n st xs
        in
        (* if #kvs < 100, use normal add *)
        if length_upto 100 0 kvs < 100 then adds kvs node
        else
          let (ks, vs) = List.split kvs in
          let ks = SS.of_list ks in
          let vs = A.of_list vs in
          if SS.is_empty ks then node
          else
            let k_first = SS.nth_exn 0 ks in
            let k_last = SS.nth_exn (SS.length ks - 1) ks in

            let insert_into_leaf keys vals =
              (* keys'[i-1] < k_first < keys'[i] *)
              let i = match SS.search k_first keys with
                | `Exact _ -> assert false
                | `Before i -> i
              in
              (* XXX we can check k_last *)
              let keys' = SS.union keys ks in
              let vals' = A.replace vals i 0 vs in
              (* FIXME: optimize for segment_set *)
              keys', vals'
            in

            let rec insert_into_internal keys subnodes =
              let subnodes_are_leaves =
                match subnodes with
                | [||] -> assert false
                | _ -> depth (A.get subnodes 0) = 1
              in
              let i = (* k_first must be in subnodes.(i) *)
                match SS.search k_first keys with
                | `Exact i -> i + 1
                | `Before i -> i
              in
              let j = (* k_last must be in subnodes.(j) *)
                match SS.search k_last keys with
                | `Exact j -> j + 1
                | `Before j -> j
              in
              (* [0..i-1] and [j+1..] are untouched
                 [i..j] are target.  [i+1..j-1] MUST NOT have any kv pairs *)
              let matched_subnodes = A.sub subnodes i (j-i+1) in
              let n =
                if subnodes_are_leaves then
                  let keys' = Seq.fold_left
                      (fun s n -> match view n with
                         | Leaf l -> SS.union s l.keys
                         | _ -> assert false)
                      SS.empty @@ A.to_seq matched_subnodes
                  in
                  let matched_subnodes = A.to_list matched_subnodes in
                  let vals' =
                    A.concat @@ List.map (fun n ->
                        match view n with
                        | Leaf l -> l.values
                        | _ -> assert false) matched_subnodes
                  in
                  let keys, vals = insert_into_leaf keys' vals' in
                  build_leaf false keys vals
                else
                  let (_, matched_keys) = SS.split_at i keys in
                  let (matched_keys, _) = SS.split_at (j - i) matched_keys in

                  let matched_subnode_keys = Seq.fold_left
                      (fun s n -> match view n with
                         | Internal i -> SS.union s i.keys
                         | _ -> assert false)
                    SS.empty @@ A.to_seq matched_subnodes
                  in
                  let keys' = SS.union matched_keys matched_subnode_keys in

                  let matched_subnodes = A.to_list matched_subnodes in
                  let subs' =
                    A.concat @@ List.map (fun s ->
                        match view s with
                        | Internal i -> i.subnodes
                        | _ -> assert false) matched_subnodes
                  in
                  assert (SS.length keys' + 1 = A.length subs');
                  let keys', subs' = insert_into_internal keys' subs' in
                  build_internal false keys' subs'
              in
              let (left_keys, right_keys) = SS.split_at i keys in
              let (_, right_keys) = SS.split_at (j - i) right_keys in
              (SS.union left_keys right_keys,
               A.replace subnodes i (j-i+1) [|n|])
            in

            match view node with
            | Leaf l ->
                (* FIXME: optimze for segment_set *)
                let ks, vs = insert_into_leaf l.keys l.values in
                build_leaf true ks vs
            | Internal i ->
                let ks, ss = insert_into_internal i.keys i.subnodes in
                build_internal true ks ss

  let bulk_adds kvs n =
    let n, secs = with_time @@ fun () -> bulk_adds kvs n in
    Format.eprintf "Bulk_adds %d verified in %.2f ses@." (List.length kvs) secs;
    if P.check then begin
      let (), secs = with_time @@ fun () ->
        List.iter (fun (seg, i) -> assert (find seg n = Some i)) kvs
      in
      Format.eprintf "Bulk_adds %d verified in %.2f ses@." (List.length kvs) secs;
    end;
    n

  let rec delete kseg n =
    let v = view n in
    match v with
    | Internal { keys; subnodes; _ } ->
        let j = match SS.search kseg keys with
          | `Exact j' -> j' + 1 | `Before j -> j
        in
        begin match delete kseg (A.get subnodes j) with
          | `Unchanged -> `Unchanged
          | `Ok subnode -> `Ok (update_internal_subnode n j subnode)
        end
    | Leaf { keys; values; _ } ->
        match SS.delete_if_found kseg keys with
        | None -> `Unchanged (* XXX warning? *)
        | Some (j, keys) ->
            (* This may make the node empty! *)
            let values = A.replace values j 1 [||] in
            `Ok (build_leaf n.root keys values)

  let delete kseg n = match delete kseg n with
    | `Ok n -> n
    | `Unchanged -> n

  let deletes keys n = List.fold_left (fun n k -> delete k n) n keys

  let bench i n =
    let rec traverse f n =
      f n;
      let v = view n in
      match v with
      | Leaf _ -> ()
      | Internal { subnodes; _ } ->
          A.iter (traverse f) subnodes
    in
    let nnodes = ref 0 in
    let pleaves = ref 0 in
    let occupied = ref 0 in
    let underflows = ref 0 in
    let exunderflows = ref 0 in
    traverse (fun n ->
        incr nnodes;
        let v = view n in
        begin match v with
          | Leaf v ->
              pleaves := !pleaves + SS.length v.keys;
          | Internal _ -> ()
        end;
        occupied := !occupied + size_with_load n;
        if size_with_load n < max_size_in_bytes / 2 then incr underflows;
        if size_with_load n < max_size_in_bytes / 8 then incr exunderflows
      ) n;
    let g = float (Obj.reachable_words (Obj.repr n)) *. float Sys.word_size /. 8_000_000_000. in
    Format.eprintf "tag: %d\tdepth: %d\tnodes: %d\tunderflows: %d\tgbs-on-disk: %.2f\tgbs-on-memory: %.2f\tpleaves/node: %.2f\tfill: %.2f\texunderflows: %d@."
      i
      (depth n)
      !nnodes
      !underflows
      (float (!nnodes * max_size_in_bytes) /. 1_000_000_000.)
      g
      (float !pleaves /. float !nnodes)
      (float !occupied /. (float (!nnodes * max_size_in_bytes)))
      !exunderflows

  open Plebeia
  open Utils

  (* kseg + RRRR..... filling all the possible length for a segment *)
  let highest_segment kseg =
    let nkseg = Segment.length kseg in
    Key.append kseg
    @@ Key.of_sides @@ List.init (Key.max_length - nkseg) (fun _ -> Key.Right)

  let ls kseg bits n =
    let kseg_sides = Key.to_sides kseg in
    let nkseg_sides = Key.length kseg in
    let lowest =
      Key.append
        kseg
      @@ Key.of_sides (List.init bits (fun _ -> Key.Left))
    in
    let highest = highest_segment kseg in

    let rec f lb0 rb0 n =
      let v = view n in
      match v with
      | Internal { keys ; subnodes; _ } ->
          let left = match SS.search lowest keys with
            | `Exact j -> j + 1 | `Before j -> j
          in
          let right = match SS.search highest keys with
            | `Exact j -> j + 1 | `Before j -> j
          in
          let rec g st i =
            if i < left then st
            else
              (* optimization: if the left key and right key share
                 the same prefix of length nkseg_sides + bits,
                 we do not recurse down into the subnode, since we are
                 sure that all the entries inside it has the same prefix. *)
              let lb = if i = 0 then lb0 else SS.nth_exn (i-1) keys in
              let rb = if i > SS.length keys - 1 then rb0 else SS.nth_exn i keys in
              let common, _, _ = Key.common_prefix lb rb in
              let common_sides = Key.to_sides common in
              let a,b = List.split_at nkseg_sides common_sides in
              if a = kseg_sides && List.length b >= bits then
                g (fst (List.split_at bits b) :: st) (i-1)
              else
                let sss = f lb rb (A.get subnodes i) in
                g (sss@st) (i-1)
          in
          List.uniq_sorted (=) @@ g [] right

      | Leaf { keys; _ } ->
          let left =
            match SS.search lowest keys with
            | `Exact j -> j
            | `Before j -> j
          in
          let right =
            match SS.search highest keys with
            | `Exact j -> j
            | `Before j -> j - 1
          in
          let get_sides j =
            let seg = SS.nth_exn j keys  in
            let a, postfix = List.split_at nkseg_sides @@ Key.to_sides seg in
            assert (kseg_sides = a);
            if List.length postfix >= bits then
              Some (fst @@ List.split_at bits postfix)
            else None
          in
          let rec f st i =
            if i < left then st
            else
              match get_sides i with
              | None -> f st (i-1)
              | Some sides -> f (sides :: st) (i-1)
          in
          List.uniq_sorted (=) @@ f [] right
    in
    f Key.empty (Key.of_sides @@ List.init Key.max_length (fun _ -> Key.Right)) n

  (* { (src++seg,v) | (src++seg,v) \in n } *)
  let find_under src n =
    let nsrc = Key.length src in
    let lowest = src in
    let highest = highest_segment src in

    let rec f n = match view n with
      | Internal { keys ; subnodes; _ } ->
          (* lowest can be found in left *)
          let left = match SS.search lowest keys with
            | `Exact j -> j + 1 | `Before j -> j
          in
          (* highest can be found in right *)
          let right = match SS.search highest keys with
            | `Exact j -> j + 1 | `Before j -> j
          in
          List.concat
          @@ List.init (right - left + 1) (fun i ->
              if i = 0 || i = right - left then
                (* partial *)
                f (A.get subnodes (left + i))
              else
                (* full *)
                let elems = elements @@ A.get subnodes (left + i) in
                if P.check then
                  List.iter (fun (k,_) ->
                      let a, _postfix = List.split_at nsrc @@ Key.to_sides k in
                      assert (Key.to_sides src = a)) elems;
                elems)

      | Leaf { keys; values ; _ } ->
          let left =
            match SS.search lowest keys with
            | `Exact j -> j
            | `Before j -> j
          in
          let right =
            match SS.search highest keys with
            | `Exact j -> j
            | `Before j -> j - 1
          in
          let get_ent j =
            let seg = SS.nth_exn j keys in
            let x = A.get values j in
            if P.check then begin
              let a, _postfix = List.split_at nsrc @@ Key.to_sides seg in
              assert (Key.to_sides src = a);
            end;
            (seg, x)
          in
          List.init (right - left + 1) (fun i -> get_ent (left + i))
    in
    f n

  (* Same as find_under but only returns postfix sides *)
  let find_under' src n =
    let nsrc = Key.length src in
    let lowest = src in
    let highest = highest_segment src in

    let rec f n : (Segment.side list * value) list =
      let v = view n in
      match v with
      | Internal { keys ; subnodes; _ } ->
          (* lowest can be found in left *)
          let left = match SS.search lowest keys with
            | `Exact j -> j + 1 | `Before j -> j
          in
          (* highest can be found in right *)
          let right = match SS.search highest keys with
            | `Exact j -> j + 1 | `Before j -> j
          in
          List.concat
          @@ List.init (right - left + 1) (fun i ->
              if i = 0 || i = right - left then f (A.get subnodes (left + i))
              else
                (* full *)
                let get_postfix k =
                  let a, postfix = List.split_at nsrc @@ Key.to_sides k in
                  assert (Key.to_sides src = a);
                  postfix
                in
                List.map (fun (k,v) -> get_postfix k, v) @@ elements @@ A.get subnodes (left + i))
      | Leaf { keys; values ; _ } ->
          let left =
            match SS.search lowest keys with
            | `Exact j -> j
            | `Before j -> j
          in
          let right =
            match SS.search highest keys with
            | `Exact j -> j
            | `Before j -> j - 1
          in
          let get_ent j =
            let seg = SS.nth_exn j keys in
            let x = A.get values j in
            let a, postfix = List.split_at nsrc @@ Key.to_sides seg in
            assert (Key.to_sides src = a);
            (postfix, x)
          in
          List.init (right - left + 1) (fun i -> get_ent (left + i))
    in
    f n

  let delete_under src n =
    let dels = ref 0 in
    let lowest = src in
    let highest = highest_segment src in
    let rec f n =
      let v = view n in
      match v with
      | Internal { keys ; subnodes; _ } ->
          let left = match SS.search lowest keys with
            | `Exact j -> j + 1 | `Before j -> j  (* lowest can be in subnodes.(j) *)
          in
          let right = match SS.search highest keys with
            | `Exact j -> j + 1 | `Before j -> j (* highest can be in subnodes.(j) *)
          in
          let kss =
            List.combine
              (None :: (List.map (fun x -> Some x) @@ SS.sorted_list keys))
              (A.to_list subnodes)
          in
          let kss =
            List.filter_map (fun x -> x)
            @@ List.mapi (fun i (ko, s) ->
                if left < i && i < right then begin
                  dels := !dels + nelems s;
                  None
                end else if i = left || i = right then
                  match f s with
                  | None -> None
                  | Some s -> Some (ko, s)
                else Some (ko, s)) kss
          in
          if kss = [] then None
          else
            let ks, ss = List.split kss in
            let ks = List.map from_Some @@ List.tl ks in
            Some (build_internal n.root (SS.of_list ks) (A.of_list ss))

      | Leaf { keys; values ; _ } ->
          let left =
            match SS.search lowest keys with
            | `Exact j -> j
            | `Before j -> j
          in
          let right =
            match SS.search highest keys with
            | `Exact j -> j
            | `Before j -> j - 1
          in
          let sub a = A.sub a 0 left ++ A.sub a (right+1) (SS.length keys - right - 1) in

          let (ks_left, ks_right) = SS.split_at (right + 1) keys in
          let (ks_left, _) = SS.split_at left ks_left in
          let keys = SS.union ks_left ks_right in
          let values = sub values in
          dels := !dels + right - left + 1;
          Some (build_leaf n.root keys values)
    in
    let n, secs = with_time @@ fun () ->
      match f n with
      | Some n -> n
      | None -> empty
    in
    Format.eprintf "Delete_under %d elems in %.2f secs@." !dels secs;
    n

  let copy srcseg dstseg n =
    (* XXX check of non existence of dstseg *)
    let src = find_under' srcseg n in
    let kvs = List.map (fun (sides, x) ->
        let seg = Key.append dstseg @@ Key.of_sides sides in
        (seg, x)) src
    in
    bulk_adds kvs n
end
