open Plebeia

type key = Segment.t

type t

val equal : t -> t -> bool

val length : t -> int

val empty : t

val is_empty : t -> bool

val add : key -> t -> t

val union : t -> t -> t

(* When a funtion `f` satisfies the monotonicity of sign, i.e.
 * ` forall k k', f k = 0 -> k <= k' -> f k' >= 0`
 * ` forall k k', f k > 0 -> k <= k' -> f k' >= 0`
 *
 * `range_of_zero f t` returns `(l, r)` such that
 *
 * `0 <= l <= r <= length t`
 * `f (nth_exn i t) < 0 <-> 0 <= i < l
 * `f (nth_exn i t) = 0 <-> l <= i < r
 * `f (nth_exn i t) = 0 <-> r <= i < length t
 *)
val range_of_zero : (key -> int) -> t -> int * int

val search : key -> t -> [ `Exact of int | `Before of int ]

val find : key -> t -> int option

val delete_if_found : key -> t -> (int * t) option

val delete : key -> t -> t

val nth : int -> t -> key option

val nth_exn : int -> t -> key

val pop_nth : int -> t -> (key * t) option

val delete_nth_exn : int -> t -> t

val split_at : int -> t -> t * t

val append_prefix : key -> t -> t

val remove_prefix_exn : key -> t -> t

val of_list : key list -> t

val of_seq : key Seq.t -> t

val sorted_seq : t -> key Seq.t

val sorted_list : t -> key list

val iter : (key -> unit) -> t -> unit

val normalize : t -> t

module Builder : sig
  type builder

  val empty_builder : builder

  val add : key -> builder -> builder

  val build : builder -> t
end

(* Persist *)
val save : Plebeia.Xcstruct.t -> int -> t -> int

val load : Plebeia.Xcstruct.t -> int -> int * t

val size_margin : int -> t -> int option

val size : t -> int


(* Only for Debug *)
module Debug : sig
  val length_full : t -> int

  type stat

  val stat : t -> stat

  val stat_pp : Format.formatter -> stat -> unit
end

