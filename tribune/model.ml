open Plebeia.Utils

module type Printable = sig
  type t
  val pp : Format.formatter -> t -> unit
end

module type Comparable = sig
  type t
  val compare : t -> t -> int
end

module type Serializable = sig
  type t
  val size : t -> int
end

module type S = sig
  type t
  type key
  type value

  val empty : t

  val is_empty : t -> bool

  val depth : t -> int

  val find : key -> t -> value option

  val bsearch : key -> t -> [> `Found of int * value | `Not_found of int ]

  val add : key -> value -> t -> t

  val delete : key -> t -> t

  val bench : t -> unit

  val pp : Format.formatter -> t -> unit

  val elements : t -> (key * value) list
end

module Make(M : sig
    module Key : sig
      type t
      include Printable with type t := t
      include Comparable with type t := t
      include Serializable with type t := t
    end

    module Value : sig
      type t
      include Printable with type t := t
      include Serializable with type t := t
    end

    val max_size_in_bytes : int
  end) = struct

  module A = struct
    include Array
    let (++) = append
    let prefix a n = sub a 0 n
    let postfix_from a n = sub a n (length a - n)
    let replace a pos len xs =
      try
        sub a 0 pos (* 0..pos-1 *)
        ++ xs (* replaces pos,..,pos+len-1 *)
        ++ sub a (pos+len) (length a - pos - len) (* pos+len..last *)
      with
      | e ->
          Format.eprintf "replace len=%d pos=%d len=%d@."
            (length a) pos len;
          raise e
  end

  let (++) = A.(++)

  open M

  type key = Key.t
  type value = Value.t

  type t =
    { root : bool
    ; ents : (Key.t * Value.t) array
    ; subnodes : t array (* leaves+1 *) option
    }

  (* only for small data *)
  let rec pp ppf t =
    let open Format in
    fprintf ppf
      "@[root=%b@ ents=@[%a@] subnodes=%a@]"
      t.root
      (list ";@ " (fun ppf (s, i) ->
           fprintf ppf "%a %a"
             Key.pp s Value.pp i))
      (A.to_list t.ents)
      (fun ppf -> function
       | None -> fprintf ppf "None"
       | Some subnodes ->
           fprintf ppf "@[%a@]"
             (list "; @ " pp) (A.to_list subnodes)) t.subnodes

  (* just for debugging *)
  let rec elements n =
    match n.subnodes with
    | None -> A.to_list n.ents
    | Some subnodes ->
        List.sort (fun (k1, _) (k2, _) -> Key.compare k1 k2)
        @@ A.to_list n.ents @ List.concat_map elements (A.to_list subnodes)

  let empty = { root= true; ents= [||]; subnodes= None }

  let is_empty n = n = empty

  let validate n =
    let nents = A.length n.ents in
    if is_empty n then ()
    else begin
      assert (nents > 0);
      for i = 0 to nents - 2 do
        let seg , _ = A.get n.ents i in
        let seg', _ = A.get n.ents (i+1) in
        assert (Key.compare seg seg' = -1)
      done;
      begin match n.subnodes with
      | None -> assert (A.length n.ents > 0)
      | Some subnodes ->
          let nsubnodes = A.length subnodes in
          assert (nents + 1 = nsubnodes);
          for i = 0 to nents do
            let subents = (A.get subnodes i).ents in
            let subents_smallest = fst (A.get subents 0) in
            let subents_largest = fst (A.get subents (A.length subents - 1)) in
            let prev_largest =
              if i = 0 then None else Some (fst (A.get n.ents (i-1)))
            in
            let next_smallest =
              if i = nents then None else Some (fst (A.get n.ents i))
            in
            begin match prev_largest with
            | None -> ()
            | Some prev_largest -> assert (Key.compare prev_largest subents_smallest = -1)
            end;
            begin match next_smallest with
            | None -> ()
            | Some next_smallest -> assert (Key.compare subents_largest next_smallest = -1)
            end;
          done
      end;
    end;
    n

  let build root ents subnodes = validate { root; ents; subnodes }

  let size rn =
    2 (* #ents *)
    + A.fold_left (fun sum (seg, i) ->
        sum + Key.size seg + Value.size i) 0 rn.ents
    + 2 (* #subnodes *)
    + match rn.subnodes with
      | None -> 0
      | Some subnodes -> A.length subnodes * 4

  let copy n =
    { n with
      ents = A.copy n.ents
    ; subnodes=
        match n.subnodes with
        | None -> None
        | Some subnodes -> Some (A.copy subnodes) }

  let update n i v =
    assert (0 <= i && i < A.length n.ents);
    let n = copy n in
    let seg, _ = A.get n.ents i in
    A.set n.ents i (seg, v);
    validate n (* no need to check *)

  let set_subnode n j subnode =
    (* n.ents are shared *)
    match n.subnodes with
    | None -> assert false
    | Some subnodes ->
        let subnodes = A.copy subnodes in
        A.set subnodes j subnode;
        validate (build n.root n.ents (Some subnodes))

  let inject n j (kseg, v) lro =
    let nents = A.length n.ents in
    let ents =
      A.(init (nents + 1) (fun i ->
          if i < j then get n.ents i
          else if i = j then (kseg, v)
          else get n.ents (i-1)))
    in
    assert (List.sort compare ((kseg,v)::A.to_list n.ents)
            = List.sort compare (A.to_list ents));
    let subnodes = match lro, n.subnodes with
      | None, None -> None
      | Some (left, right), Some subnodes ->
          let nsubnodes = A.length subnodes in
          let subnodes' = A.init (nsubnodes+1) (fun i ->
              if i < j then A.get subnodes i
              else if i = j then left
              else if i = j+1 then right
              else A.get subnodes (i-1))
          in
          Some subnodes'
      | _ -> assert false
    in
    build n.root ents subnodes

  let rec depth n =
    match n.subnodes with
    | None -> 1
    | Some subnodes -> depth (A.get subnodes 0) + 1

  let bsearch kseg n =
    let nleaves = A.length n.ents in
    let get = A.get n.ents in
    let check j =
      let seg, i = get j in
      let comp = Key.compare kseg seg in
      i, comp
    in
    let rec bsearch (l,r) =
      let j = (l + r) / 2 in
      match check j with
      | i, 0 -> `Found (j, i)
      | _, -1 ->
          if j = l then `Not_found l
          else bsearch (l,j-1)
      | _, 1 ->
          if j = r then `Not_found (l+1)
          else bsearch (j+1,r)
      | _ -> assert false
    in
    bsearch (0, nleaves-1)

  let lsearch kseg n =
    let ents = n.ents in
    let rec f j =
      if j = A.length ents then `Not_found j
      else
        let seg, i = A.get ents j in
        match Key.compare kseg seg with
        | -1 -> `Not_found j
        | 1 -> f (j+1)
        | 0 ->  `Found (j, i)
        | _ -> assert false
    in
    f 0

  let search kseg n =
    let res  = bsearch kseg n in
    let res' = lsearch kseg n in
    assert (res = res');
    res

  let rec find kseg n =
    if is_empty n then None
    else
      match search kseg n with
      | `Found (_, v) -> Some v
      | `Not_found j ->
          match n.subnodes with
          | None -> None
          | Some subnodes ->
              find kseg (A.get subnodes j)

  let array_nsplit ats sz =
    let ranges =
      let rec f start = function
        | [] -> [(start, sz - 1)]
        | at::ats -> (start, at-1) :: f (at+1) ats
      in
      f 0 ats
    in
    ats, ranges

  let nsplit x n =
    assert (x >= 2);
    let nents = A.length n.ents in
    let points = List.init (x-1) (fun i -> nents * (i+1) / x) in
    let ups, downs = array_nsplit points (A.length n.ents) in
    let ups = List.map (A.get n.ents) ups in
    let downs =
      List.map (fun (start, end_) ->
          let ents = A.sub n.ents start (end_ - start + 1) in
          let subnodes = match n.subnodes with
            | None -> None
            | Some subnodes ->
                Some (A.sub subnodes start (end_ - start + 2))
          in
          build false ents subnodes
        ) downs
    in
    ups, downs

  let split n = match nsplit 2 n with
    | [mident], [left; right] -> left, mident, right
    | _ -> assert false

  (* split n until each subnode becomes enough small *)
  let split_enough n =
    let rec f x =
      match nsplit x n with
      | ems, sjs when List.for_all (fun sji -> size sji < max_size_in_bytes) sjs ->
          assert (List.length ems + 1 = List.length sjs);
          ems, sjs
      | _ -> f (x+1)
    in
    f 2

  let add kseg v n =
    let rec add' kseg v n =
      match search kseg n with
      | `Found (_, v') when v = v' ->
          `NoChange
      | `Found (j, _) ->
          (* overwrite *)
          `Updated (update n j v )
      | `Not_found j ->
          let n = match n.subnodes with
            | None ->
                Some (inject n j (kseg, v) None)
            | Some subnodes ->
                match add' kseg v (A.get subnodes j) with
                | `NoChange -> None
                | `Updated subnode ->
                    Some (set_subnode n j subnode)
                | `Split (left, mid, right) ->
                    Some (inject n j mid (Some (left, right)))
          in
          match n with
          | None -> `NoChange
          | Some n ->
              if size n > max_size_in_bytes then
                let left, mid, right = split n in
                `Split (left, mid, right)
              else `Updated n
    in
    if is_empty n then build true [| (kseg, v) |] None
    else
      match add' kseg v n with
      | `Updated n -> n
      | `NoChange -> n
      | `Split (left, mid, right) ->
          build true [| mid |] (Some [| left; right |])


  (* Fix unbalanced sj, j-th subnode of n and returns the updated tree,
     with the propagation information for the parent of n.

     Strategy

     * If sj overflows, i.e. size sj > max_size_in_bytes, we MUST split sj
       into (sjl, em, sjr), then replace sj by it in n:

         from:
              ej-1     ej
         sj-1     <sj>    sj+1

         to:
              ej-1     em     ej
         sj-1      sjl    sjr    sj+1

       We MUST check size sjl and sjr do not overflow.  If overflown, (very unlikely),
       we MUST split sj into (sjx, emx, sjy, emy, sjz) and replace sj by it in n:

         to:
              ej-1     ex     ey     ej
         sj-1      sjx    sjy    sjz    sj+1

       If one of sjx, sjy, and sjz still overflows, we MUST split sj further.
       (It is real hard to imagine such a case but as far as we cannot prove
        the impossibility, we MUST be prepared.)

       We ignore the underflow of splitted subnodes, i.e.
       size sji < max_size_in_bytes / 2.  Even if underflown, their size should be
       very near to the threshold.

     * If sj underflows, i.e. size sj < max_size_in_bytes / 2, we MUST
       merge it with one of the adjacent subnode sj+1 (or sj-1) and the entry ej
       (or ej-1) between them.

         from:
              ej-1    ej      ej+1
         sj-1     >sj<   sj+1      sj+2

       From (sj, ej, sj+1), we make a node m which is:

         entries:   sj.ents ++ [|ej|] ++ sj+1.ents
         subnodes:  sj.subnodes ++ sj+1.subnodes

       If m does not overflow, we replace (sj, ej, sj+1) in n by m:

         to:
              ej-1            ej+1
         sj-1          m           sj+2

       If m does overflow, we split it into (s'j, e'j, s'j+1) then
       replace (sj, ej, sj+1) by the new triple:

         to:
              ej-1     e'j       ej+1
         sj-1      s'j     s'j+1      sj+2

       If one of s'j and s'j+1 overflows, then we further split m into (s'jx, e'jx, s'jy, e'jy, s'jz)
       and use it the replacement of (sj, ej, sj+1).

         to:
              ej-1      e'jx       e'jy      ej+1
         sj-1      s'jx       s'jy      s'jz      sj+2

       We ignore underflows of new subnodes.

     * If n is updated to n', we check its size.

       * If non root node underflows or overflows, run this rebalancing at the parent of n.
         If it is between [max_size_in_bytes / 2, max_size_in_bytes], stop the rebalancing.

       * We do not care the underflow of the root.

       * If the root node n' overflows, we need another depth: split it into (s1, e, s2) and
         replace n' by the triple:

         to:
                  e
               s1   s2

         We ignore underflows of s1 and s2.

     * The algorithm surely terminates since it only goes up.
     * The algorithm may produce slightly underflown nodes.
  *)

  let rebalance n j =
    let ss = match n.subnodes with
      | None -> assert false
      | Some ss -> ss
    in
    let sj = A.get ss j in
    let szsj = size sj in
    let n' =
      if szsj > max_size_in_bytes then begin
        let ems, sjs = split_enough sj in
        let ents = A.replace n.ents j 0 @@ A.of_list ems in
        let subnodes = A.replace ss j 1 @@ A.of_list sjs in
        build n.root ents (Some subnodes)
      end else if szsj < max_size_in_bytes / 2 then begin
        let k =
          if j = 0 then 0
          else if j = A.length ss - 1 then j - 1
          else
            (* we choose smaller *)
            let szsj_prev = size @@ A.get ss (j-1) in
            let szsj_next = size @@ A.get ss (j+1) in
            if szsj_prev < szsj_next then j - 1 else j
        in
        let m =
          let ek = A.get n.ents k in
          let sk0 = A.get ss k in
          let sk1 = A.get ss (k+1) in
          let ents = sk0.ents ++ [|ek|] ++ sk1.ents in
          let subnodes =
            match sk0.subnodes, sk1.subnodes with
            | None, None -> None
            | Some ss1, Some ss2 ->
                assert (A.length ents + 1 = A.length ss1 + A.length ss2);

                Some (ss1 ++ ss2)
            | _ -> assert false
          in
          build false ents subnodes
        in
        if size m <= max_size_in_bytes then begin
          (* if it is root and n.ents = 0 then we decrease one level *)
          if A.length n.ents = 1 then begin
            assert n.root;
            { m with root= true }
          end else
            let ents = A.replace n.ents k 1 [||] in
            let subnodes = A.replace ss k 2 [|m|] in
            build n.root ents (Some subnodes)
        end else begin
          let ems, sjs = split_enough m in
          let ents = A.replace n.ents k 1 @@ A.of_list ems in
          let subnodes = A.replace ss k 2 @@ A.of_list sjs in
          build n.root ents (Some subnodes)
        end
      end else n
    in
    n'

  let rebalance_from_root n trace =
    let rec aux n = function
      | [] -> `Unchanged
      | [j] ->
          let n' = rebalance n j in
          let szn' = size n' in
          if szn' > max_size_in_bytes then
            `Overflow n'
          else if szn' < max_size_in_bytes / 2 then
            `Underflow n'
          else `Balanced n'
      | j::js ->
          match n.subnodes with
          | None -> assert false
          | Some ss ->
              match aux (A.get ss j) js with
              | `Unchanged -> `Unchanged
              | `Balanced sj' ->
                  `Balanced (build n.root n.ents (Some (A.replace ss j 1 [|sj'|])))
              | `Overflow sj' | `Underflow sj' ->
                  let n = build n.root n.ents (Some (A.replace ss j 1 [|sj'|])) in
                  let n' = rebalance n j in
                  let szn' = size n' in
                  if szn' > max_size_in_bytes then
                    `Overflow n'
                  else if szn' < max_size_in_bytes / 2 then
                    `Underflow n'
                  else `Balanced n'
    in
    match aux n trace with
    | `Unchanged -> n
    | `Underflow n' | `Balanced n' -> n'
    | `Overflow n' ->
        let ems, sjs = split_enough n' in
        build true (A.of_list ems) (Some (A.of_list sjs))

  let rec delete kseg n =
    match search kseg n with
    | `Not_found j ->
        begin match n.subnodes with
        | None -> `Unchanged
        | Some subnodes ->
            begin match delete kseg (A.get subnodes j) with
            | `Unchanged -> `Unchanged
            | `Ok (tr, subnode) -> `Ok (j::tr, set_subnode n j subnode)
            end
        end
    | `Found (j, _i) ->
        match n.subnodes with
        | None ->
            let ents = A.replace n.ents j 1 [||] in
            `Ok ([], build n.root ents None)
        | Some subnodes ->
            (*
               e_{j-1}     e_j         e_{j+1}
                       s_j     s_{j+1}

               ->

               e_{j-1}       max(s_j)          e_{j+1}
                       s'_j           s_{j+1}
            *)
            let prev_subnode = A.get subnodes j in
            let rec get_max m =
              match m.subnodes with
              | None ->
                  let ents = A.sub m.ents 0 (A.length m.ents - 1) in
                  let maxe = A.get m.ents (A.length m.ents - 1) in
                  let m = build m.root ents None in
                  let tr = [] in
                  maxe, tr, m
              | Some subnodes ->
                  let j = A.length subnodes - 1 in
                  let maxe, tr, sn = get_max (A.get subnodes j) in
                  let tr = j :: tr in
                  maxe, tr, set_subnode m j sn
            in
            let maxe, tr, prev_subnode = get_max prev_subnode in
            let ents = A.replace n.ents j 1 [|maxe|] in
            let subnodes = A.replace subnodes j 1 [|prev_subnode|] in
            `Ok (j :: tr, build n.root ents (Some subnodes))

  let delete kseg n =
    match delete kseg n with
    | `Unchanged -> n
    | `Ok (tr, n') -> rebalance_from_root n' tr

  let bench n =
    let rec traverse f n =
      f n;
      match n.subnodes with
      | None -> ()
      | Some subnodes -> A.iter (traverse f) subnodes
    in
    let nnodes = ref 0 in
    let pleaves = ref 0 in
    let occupied = ref 0 in
    traverse (fun n ->
        incr nnodes;
        pleaves := !pleaves + A.length n.ents;
        occupied := !occupied + size n
      ) n;
    Format.eprintf "depth: %d nodes: %d bytes: %d pleaves/node: %.2f fill: %.2f@."
      (depth n)
      !nnodes (!nnodes * max_size_in_bytes) (float !pleaves /. float !nnodes)
      (float !occupied /. (float (!nnodes * max_size_in_bytes)))
end

module P = struct
  include Make(struct
      open Plebeia

      module Key = struct
        type t = Segment.t
        let pp ppf s = Format.fprintf ppf "%s" (Segment.to_string s)
        let compare = Segment.compare
        let size s = (Segment.length s + 7) / 8 + 1
      end

      module Value = struct
        type t = Index.t
        let pp = Index.pp
        let size _ = 4
      end

      let max_size_in_bytes = 4096
  end)

  open Plebeia
  open Utils

  let ls kseg bits n =
    let nkseg = Segment.length kseg in
    let lowest =
      Segment.append
        kseg
        @@ Segment.of_sides (List.init bits (fun _ -> Segment.Left))
    in
    let highest =
      Segment.append
        kseg
        @@ Segment.of_sides @@ List.init (Segment.max_length - nkseg) (fun _ -> Segment.Right)
    in
    let rec f n =
      let left =
        match bsearch lowest n with
        | `Found (j, _) -> j * 2 + 1
        | `Not_found j -> j * 2
      in
      let right =
        match bsearch highest n with
        | `Found (j, _) -> j * 2 + 1
        | `Not_found j -> j * 2
      in
      let get_ent j =
        let seg, _ = A.get n.ents j in
        let a, postfix = List.split_at nkseg @@ Segment.to_sides seg in
        assert (Segment.to_sides kseg = a);
        let sides, _ = List.split_at bits postfix in
        if List.length sides = bits then [sides] else []
      in
      match n.subnodes with
      | None ->
          (* Tricky: if right = 0 (left most ss), then the new right must be -1 *)
          let left = left / 2 in
          let right = if right = 0 then -1 else (right - 1) / 2 in
          List.uniq_sorted (=)
          @@ List.flatten @@ List.init (right - left + 1) (fun i ->
              get_ent (left + i))
      | Some ss ->
          let targets =
            List.init (right - left + 1) (fun i ->
                let pos = i + left in
                let pos' = pos / 2 in
                if pos mod 2 = 0 then
                  `Sub (A.get ss pos')
                else `Ent (get_ent pos'))
          in
          (* skip searching subnodes if possible *)
          let rec loop = function
            | `Sub n :: xs -> `Sub (f n) :: loop xs
            | `Ent [sides1] :: `Sub _ :: `Ent [sides2] :: xs when sides1 = sides2 ->
                (* we can skip the sub *)
                (* XXX we can also skip if sides1 + 1 = sides... *)
                `Ent [sides1] :: loop xs
            | `Ent sidess :: `Sub n :: xs ->
                `Ent sidess :: `Sub (f n) :: loop xs
            | [] -> []
            | _ -> assert false
          in
          List.uniq_sorted (=)
          @@ List.concat_map (function
              | `Sub xs -> xs
              | `Ent xs -> xs)
          @@ loop targets
    in
    f n


  (* { (seg,v) | (src++seg,v) \in n } *)
  let find_under src n =
    let nsrc = Segment.length src in
    let lowest = src in
    let highest =
      Segment.append
        src
        @@ Segment.of_sides @@ List.init (Segment.max_length - nsrc) (fun _ -> Segment.Right)
    in
    let rec f n =
      let left =
        match bsearch lowest n with
        | `Found (j, _) -> j * 2 + 1
        | `Not_found j -> j * 2
      in
      let right =
        match bsearch highest n with
        | `Found (j, _) -> j * 2 + 1
        | `Not_found j -> j * 2
      in
      let get_ent j =
        let seg, v = A.get n.ents j in
        let a, postfix = List.split_at nsrc @@ Segment.to_sides seg in
        assert (Segment.to_sides src = a);
        [postfix, v]
      in
      match n.subnodes with
      | None ->
          (* Tricky: if right = 0 (left most ss), then the new right must be -1 *)
          let left = left / 2 in
          let right = if right = 0 then -1 else (right - 1) / 2 in
          List.flatten @@ List.init (right - left + 1) (fun i ->
              get_ent (left + i))
      | Some ss ->
          List.concat
          @@ List.init (right - left + 1) (fun i ->
              let pos = i + left in
              let pos' = pos / 2 in
              if pos mod 2 = 0 then f (A.get ss pos')
              else get_ent pos')
    in
    f n

  let copy srcseg dstseg n =
    let src = find_under srcseg n in
    List.fold_left (fun n (sides, v) ->
        let seg = Segment.append dstseg @@ Segment.of_sides sides in
        add seg v n) n src

  let rm srcseg n =
    let src = find_under srcseg n in
    List.fold_left (fun n (sides, _) ->
        let seg = Segment.append srcseg @@ Segment.of_sides sides in
        delete seg n) n src
end

let copy_plebeia ctxt pn =
  let open Plebeia in
  let pn = match Node.view ctxt pn with
    | Bud (Some pn, _, _) -> pn
    | _ -> assert false
  in

  let leaves = Hashtbl.create 1001 in
  let rec f rev_sides pn =
    match Node.view ctxt pn with
    | Bud _ -> assert false
    | Leaf (_, Indexed i, _) ->
        Hashtbl.replace leaves (Segment.normalize @@ Segment.of_sides @@ List.rev rev_sides) i
    | Leaf _ -> assert false
    | Internal (pnl, pnr, _, _) ->
        f (Segment.Left::rev_sides) pnl;
        f (Segment.Right::rev_sides) pnr
    | Extender (seg, pn, _, _) ->
        f (List.rev_append (Segment.to_sides seg) rev_sides) pn
  in
  f [] pn;
  Format.eprintf "%d pleaves@." @@ Hashtbl.length leaves;

  let cntr = ref 0 in
  let lasttime = ref (Unix.gettimeofday ()) in

  let t =
    Hashtbl.fold (fun seg i n ->
        let n = P.add seg i n in
        incr cntr;
        let now = Unix.gettimeofday () in
        if !cntr mod 10000 = 0 || now -. !lasttime > 60.0 then begin
          Format.eprintf "Added %d leaves@." !cntr;
          lasttime := now
        end;
        n) leaves P.empty
  in
  prerr_endline "done";

  let cntr = ref 0 in
  let lasttime = ref (Unix.gettimeofday ()) in
  Hashtbl.iter (fun seg i ->
      assert (P.find seg t = Some i);
      incr cntr;
      let now = Unix.gettimeofday () in
      if !cntr mod 10000 = 0 || now -. !lasttime > 60.0 then begin
        Format.eprintf "Checked %d leaves@." !cntr;
        lasttime := now
      end) leaves;
  prerr_endline "done";

  (* must check *)
  P.bench t

let test_copy_plebeia () =
  let open Plebeia in
  let path = Sys.argv.(1) in
  let vc = Vc.open_ ~mode:Storage.Reader path in
  let roots = Vc.roots vc in (* XXX silly to load all *)

  let last_root =
    match Commit.read_the_latest roots with
    | Some root -> root
    | None -> assert false
  in

  Format.eprintf "last root index= %a@." Index.pp last_root.Commit.index;

  let Cursor.Cursor (_, n, ctxt,_) = from_Some @@ Vc.checkout vc last_root.Commit.hash in
  copy_plebeia ctxt n
