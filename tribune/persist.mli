open Plebeia

module TStorage : sig
  type t

  val create :
    ?pos:int64 ->
    ?length:int ->
    ?resize_step:Plebeia.Index.t -> ?version:int -> string -> t

  val close : t -> unit
end

module Make
    (P : sig
       val storage : TStorage.t
       val max_size_in_bytes : int
     end)
: sig

  type key = Segment.t
  type value = Index.t

  type t =
    { root : bool

    ; mutable desc : desc
      (* physically shared nodes are loaded/forgot
         at the same time *)
    }

  and desc =
    | Disk of Index.t
    | View of view

  and view =
    { ents : (key * value) array
    ; subnodes : t array (* leaves+1 *) option
    ; mutable index : Index.t option
    ; mutable size : [`Exactly of int | `Overflow] option
      (** Size on disk using Compressed_ents in bytes.
          The size needs not be exact for overflown nodes.
      *)
    }

  val empty : t
  val is_empty : t -> bool

  val build : bool -> (key * value) array -> t array option -> t

  val init : (key * value) list -> t
  val adds : (key * value) list -> t -> t
  val bulk_adds : (key * value) list -> t -> t
  val deletes : key list -> t -> t
  val find : key -> t -> value option
  val ls : key -> int -> t -> Segment.side list list
  val copy : key -> key -> t -> t
  val rm : key -> t -> t * int
  val find_under : key -> t -> (Segment.side list * value) list

  val save : t -> unit
  val may_forget : t -> unit

  val saved : int ref
  val bench : int -> t -> unit
  val elements : t -> (key * value) list
  val pp : Format.formatter -> t -> unit
end
