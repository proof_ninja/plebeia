(** Reference implementation of Tribune B-tree *)

module type Printable = sig
  type t
  val pp : Format.formatter -> t -> unit
end

module type Comparable = sig
  type t
  val compare : t -> t -> int
end

module type Serializable = sig
  type t
  val size : t -> int
end

module type Prefix = sig
  type t
  val empty : t
  val is_empty : t -> bool
  val append : t -> t -> t
  val remove_prefix : t -> t -> (t * t * t)
  (* remove_prefix "ABC" "ABCDE" = ("ABC", "", "DE")
     remove_prefix "ABC" "AX" = ("A", "BC", "X")
  *)
  val remove_prefix_opt : t -> t -> t option
  val common_prefix : t -> t -> t
end

module type S = sig
  type t
  type key
  type value

  val empty : t

  val is_empty : t -> bool

  val depth : t -> int

  val nelems : t -> int

  val find : key -> t -> value option

  val add : key -> value -> t -> t

  val delete : key -> t -> t

  val bench : t -> unit

  val saved_length : t -> int

  val pp : Format.formatter -> t -> unit

  val elements : t -> (key * value) list

  val check_maximumly_prefixed_full : t -> unit
end

module Make(M : sig
    module Key : sig
      type t
      include Printable with type t := t
      include Comparable with type t := t
      include Serializable with type t := t
      include Prefix with type t := t
    end

    module Value : sig
      type t
      include Printable with type t := t
      include Serializable with type t := t
    end

    val max_size_in_bytes : int
  end) : S with type key = M.Key.t and type value = M.Value.t

module P : S with type key = Plebeia.Segment.t and type value = Plebeia.Index.t

val copy_plebeia : Plebeia.Context.t -> Plebeia.Node.node -> unit

val test_copy_plebeia : unit -> unit
