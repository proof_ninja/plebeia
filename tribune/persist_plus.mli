open Plebeia

module TStorage : sig
  type t

  val create :
    ?pos:int64 ->
    ?length:int ->
    ?resize_step:Plebeia.Index.t -> ?version:int -> string -> t

  val open_ :
    ?pos:int64 ->
    ?resize_step:Plebeia.Index.t ->
    ?ignore_version:bool ->
    mode:Plebeia.Gstorage.mode
    -> string
    -> t

  val close : t -> unit

  val commit : t -> unit
end

module Key : sig
  val test : unit -> unit
end

module Value : sig
  val test : unit -> unit
end

module SS (P: sig val check : bool end) : sig
end

module Make
    (P : sig
       val storage : TStorage.t
       val max_size_in_bytes : int
       val check : bool (** check invariants and asserts *)
     end)
: sig

  type key = Segment.t
  type value = Index.t

  type t

  val index : t -> Index.t option

  val of_index : Index.t -> t
  (** Create a root node which points to the given [Index.t] *)

  val empty : t
  val is_empty : t -> bool

  val init' : Segment_set.t -> value array -> t
  val init : (key * value) list -> t
  (** Initiialize Tribune tree with the given *sorted* kv pairs.
      The tree is rebalanced.
  *)

  val add : key -> value -> t -> t
  (** Add or update the given kv pair.  The result is *not* rebalanced. *)

  val adds : (key * value) list -> t -> t

  val bulk_adds : (key * value) list -> t -> t
  (** Add the given kv pairs optimized.  The result is *not* rebalanced.

      It only works under the following conditions:

      - kv pairs are sorted
      - no bindings between the smallest and the larget keys of kvs (inclusive)
        exist in the original tree
  *)

  val delete : key -> t -> t
  (** Remove the given kv pair.  The result is *not* rebalanced.
      If a kv pair is not found in the source, it is ignored.
  *)

  val deletes : key list -> t -> t

  val rebalance : t -> t
  (** Rebalance a tree *)

  val find : key -> t -> value option
  val copy : key -> key -> t -> t
  val ls : key -> int -> t -> Segment.side list list
  val find_under : key -> t -> (key * value) list
  val delete_under : key -> t -> t

  val save : t -> unit
  (** Save a tree.  The tree MUST be already rebalanced. *)

  val saved : int ref
  (** Nodes ever saved *)

  val may_forget : t -> unit
  (** Forget the tree, if it is already persisted to the storage *)

  val elements : t -> (key * value) list
  val elems : t -> (key * value) list
  val nelems : t -> int

  val has_bindings : t -> (key * value) list -> bool
  (** [has_bindings t kvs] checks whether [n] has [kvs].
      Not only by comparing [elements n] and [kvs] it also checks
      [kvs] are really found by [find]. *)

  val bench : int -> t -> unit

  val pp : Format.formatter -> t -> unit
  val depth : t -> int

  val loaded : int ref
end
