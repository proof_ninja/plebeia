(** Reference implementation of Tribune B-tree *)

module type Printable = sig
  type t
  val pp : Format.formatter -> t -> unit
end

module type Comparable = sig
  type t
  val compare : t -> t -> int
end

module type Serializable = sig
  type t
  val size : t -> int
end

module type S = sig
  type t
  type key
  type value

  val empty : t

  val is_empty : t -> bool

  val depth : t -> int

  val find : key -> t -> value option

  val bsearch : key -> t -> [> `Found of int * value | `Not_found of int ]
  (** Binary search within a node.
      `Found (j, v) : key just found at j-th entry with value v
      `Not_found j : key is not found in the entries.  It may be found
                     in j-th subnode, if t is not a leaf.  If it is a leaf,
                     there is no key bound in the leaf.
  *)

  val add : key -> value -> t -> t

  val delete : key -> t -> t

  val bench : t -> unit

  val pp : Format.formatter -> t -> unit

  val elements : t -> (key * value) list
end

module Make(M : sig
    module Key : sig
      type t
      include Printable with type t := t
      include Comparable with type t := t
      include Serializable with type t := t
    end

    module Value : sig
      type t
      include Printable with type t := t
      include Serializable with type t := t
    end

    val max_size_in_bytes : int
  end) : S with type key = M.Key.t and type value = M.Value.t

module P : sig
  include S with type key = Plebeia.Segment.t and type value = Plebeia.Index.t

  val ls : Plebeia.Segment.t -> int -> t -> Plebeia.Segment.side list list

  val find_under : Plebeia.Segment.t -> t -> (Plebeia.Segment.side list * Plebeia.Index.t) list
  (** [find_under src t] lists all the bindings of [t] under [src].
      [{ (seg,v) | (src++seg,v) \in n }]
  *)

  val copy : Plebeia.Segment.t -> Plebeia.Segment.t -> t -> t
  (** [copy src dst t] copies all the bindings [(kseg,v)] where [kseg = src ++ seg]
      of [t] to [dst ++ seg].  Existing bindings of [t] may be overwritten.

     This is implemented by calling [add] one-bye-one.  Incredibly inefficient.
  *)

  val rm : Plebeia.Segment.t -> t -> t
  (** [rm seg t] removes alll the bindings [(kseg,v)] s.t. [kseg = seg ++ seg']
      of [t] for some [seg'].

     This is implemented by calling [delete] one-bye-one.  Incredibly inefficient.
  *)
end

val copy_plebeia : Plebeia.Context.t -> Plebeia.Node.node -> unit

val test_copy_plebeia : unit -> unit
