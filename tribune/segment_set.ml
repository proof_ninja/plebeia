open Plebeia
module Option = Stdlib.Option
module Key = Segment

module Seq = struct
  include Seq

  (* in Stdlib since 4.11 *)
  let cons x xs = fun () -> Seq.Cons (x, xs)

  (* in Stdlib since 4.11 *)
  let rec append xs ys = fun () ->
    match xs () with
    | Seq.Nil -> ys ()
    | Seq.Cons (x, xs) -> Seq.Cons (x, append xs ys)
end

type key = Key.t

(* non empty key *)
type nkey = { hd : Key.side; tail : key }
module NKey = struct
  let make hd tail = { hd; tail }
  let of_key k = Option.map (fun (hd, tail) -> {hd; tail}) @@ Key.cut k
  let of_key_exn k = Option.get @@ of_key k
  let to_key ne = Key.cons ne.hd ne.tail
  let equal k1 k2 = k1.hd = k2.hd && Key.equal k1.tail k2.tail
  let common_prefix k1 k2 = Key.common_prefix (to_key k1) (to_key k2)
  let append k1 k2 = of_key_exn @@ Key.append (to_key k1) (to_key k2)
end

module Node : sig
  type node = private
    | Leaf (* Singleton. The only member is the empty key *)
    | Extender (* Extender has the empty key as a member. *) of nkey * snode (* key is the common prefix of the subnode *)
    | Internal of
        bool (* True iff. the empty key is a member of this node *)
        * key (* left_prefix *)
        * snode
          (* (cons L (left_prefix)) is the common prefix of the left subnode *)
        * key (* right_prefix *)
        * snode

  and snode = { length : int; node : node }

  type t = private
    | EmptySet (* empty *)
    | Root of
        (* not empty *)
        key (* prefix for the entire patricia tree *) * snode

  type snode' = Empty | Node of snode | Prefixed of nkey * snode

  val make_leaf : snode

  val make_internal : bool -> key -> snode -> key -> snode -> snode

  val add_empty : snode -> snode

  val add_empty_opt : snode -> snode option (* return Some only of changed *)

  val make_extender : nkey -> snode -> snode

  val may_prefixed : key -> snode -> snode'

  val force_prefixed_exn : snode' -> key * snode

  val make_internal' : ?first_side:Key.side -> bool -> snode' -> snode' -> snode'

  val make_extender' : snode' -> snode

  val append_prefixed : key -> snode' -> snode'

  val make_root : key -> snode -> t

  val make_root' : snode' -> t

  val empty : t

  val normalize : t -> t
end = struct
  type node =
    | Leaf
    | Extender of nkey * snode
    | Internal of bool * key * snode * key * snode

  and snode = { length : int; node : node }

  type t = EmptySet | Root of key * snode

  type snode' = Empty | Node of snode | Prefixed of nkey * snode

  let make_snode length node = { length; node }

  let make_leaf = make_snode 1 Leaf

  let compact_key k = if Key.is_empty k then Key.empty else Key.normalize k
  let compact_nkey nk = { nk with tail = compact_key nk.tail }

  let make_internal p ls ln rs rn =
    let sz = ln.length + rn.length in
    let sz = if p then sz + 1 else sz in
    make_snode sz @@ Internal (p, compact_key ls, ln, compact_key rs, rn)

  let add_empty sn =
    match sn.node with
    | Internal (_, lseg, ln, rseg, rn) -> make_internal true lseg ln rseg rn
    | _ -> sn

  let add_empty_opt sn =
    match sn.node with
    | Internal (false, lseg, ln, rseg, rn) ->
        Some (make_internal true lseg ln rseg rn)
    | _ -> None

  let make_extender nk n =
    make_snode (n.length + 1) @@ Extender (nk, n)

  let may_prefixed k sn =
    match NKey.of_key k with
    | None -> Node sn
    | Some nk -> Prefixed (nk, sn)


  let force_prefixed_exn = function
    | Empty -> assert false
    | Node n -> (Key.empty, n)
    | Prefixed (nk, n) -> (NKey.to_key nk, n)


  let rec make_internal' ?(first_side = Key.Left) p lsn rsn =
    let force_prefixed_opt = function
      | Empty -> None
      | Node n -> Some (Key.empty, n)
      | Prefixed (nk, n) -> Some (NKey.to_key nk, n)
    in
    if first_side = Key.Right then make_internal' p rsn lsn
    else
      let half p ext n =
        if p then Node (make_extender ext n)
        else Prefixed (ext, n)
      in
      match (force_prefixed_opt lsn, force_prefixed_opt rsn) with
      | None, None -> if p then Node make_leaf else Empty
      | Some (lseg, ln), None ->
          half p (NKey.make Key.Left lseg) ln
      | None, Some (rseg, rn) ->
          half p (NKey.make Key.Right rseg) rn
      | Some (lseg, ln), Some (rseg, rn) ->
          Node (make_internal p lseg ln rseg rn)


  let make_extender' = function
    | Empty -> make_leaf
    | Node n -> add_empty n
    | Prefixed (ext, n) -> make_extender ext n

  let append_prefixed prefix = function
    | Empty -> Empty
    | Node n ->
        begin match NKey.of_key prefix with
          | None -> Node n
          | Some prefix -> Prefixed (prefix, n)
        end
    | Prefixed (prefix', n) ->
        let prefix' = NKey.to_key prefix' in
        Prefixed (NKey.of_key_exn @@ Key.append prefix prefix', n)

  let make_root prefix snode = Root (compact_key prefix, snode)

  let make_root' = function
    | Empty -> EmptySet
    | Node snode -> make_root Key.empty snode
    | Prefixed (prefix, snode) -> make_root (NKey.to_key prefix) snode

  let empty = EmptySet

  let normalize t =
    let rec normalize { length; node } =
      let node =
        match node with
        | Leaf -> Leaf
        | Extender (seg, n') -> Extender (compact_nkey seg, normalize n')
        | Internal (p, lseg, ln, rseg, rn) ->
            Internal
              ( p,
                Segment.normalize lseg,
                normalize ln,
                Segment.normalize rseg,
                normalize rn )
      in
      { length; node }
    in
    match t with
    | EmptySet -> EmptySet
    | Root (p, n) -> Root (Segment.normalize p, normalize n)
end

include Node

let iter f t =
  let rec iter prefix = function
    | Leaf -> f prefix
    | Extender (seg, n) ->
        f prefix;
        iter (Key.append prefix @@ NKey.to_key seg) n.node
    | Internal (p, lseg, l, rseg, r) ->
        let lprefix = Key.append prefix (Key.cons Key.Left lseg) in
        let rprefix = Key.append prefix (Key.cons Key.Right rseg) in
        if p then f prefix;
        iter lprefix l.node;
        iter rprefix r.node
  in
  match t with EmptySet -> () | Root (prefix, n) -> iter prefix n.node

let rec equal_node n m =
  match (n, m) with
  | Leaf, Leaf -> true
  | Extender (segn, n), Extender (segm, m) ->
      NKey.equal segn segm && equal_node n.node m.node
  | Internal (pn, lsegn, ln, rsegn, rn), Internal (pm, lsegm, lm, rsegm, rm) ->
      pn == pm && Key.equal lsegn lsegm && Key.equal rsegn rsegm
      && equal_node ln.node lm.node && equal_node rn.node rm.node
  | _, _ -> false

let equal n m =
  match (n, m) with
  | EmptySet, EmptySet -> true
  | Root (segn, n), Root (segm, m) ->
      Key.equal segn segm && equal_node n.node m.node
  | _, _ -> false

let is_empty = function EmptySet -> true | _ -> false

let length = function EmptySet -> 0 | Root (_, n) -> n.length

let add k t =
  let rec add' k prefix n =
    let map_node = Option.map (fun n -> Node n) in
    match NKey.of_key k, NKey.of_key prefix with
    | Some k, Some prefix -> add_into_prefixed k prefix n
    | Some k, None -> map_node @@ add_non_empty k n
    | None, None -> map_node @@ add_empty_opt n
    | None, Some p -> Some (Node (make_extender p n))
  and add_into_prefixed k prefix n =
    let prefix, k, extend =
      Key.common_prefix (NKey.to_key k) (NKey.to_key prefix)
    in
    Option.map (may_prefixed prefix) @@
      match (NKey.of_key k, NKey.of_key extend) with
      | None, None -> add_empty_opt n
      | None, Some extend -> Some (make_extender extend n)
      | Some k, None -> add_non_empty k n
      | Some { hd= side0; tail = seg0 }, Some { hd = side1; tail = seg1 } ->
          begin
            assert (side0 <> side1);
            let n0 = make_leaf in
            let n1 = n in
            match side0 with
            | Segment.Left -> Some (make_internal false seg0 n0 seg1 n1)
            | Segment.Right -> Some (make_internal false seg1 n1 seg0 n0)
          end
  and add_non_empty k n =
    match n.node with
    | Leaf -> Some (make_extender k make_leaf)
    | Extender (ext, n) ->
        Option.map make_extender' @@ add_into_prefixed k ext n
    | Internal (p, lseg, ln, rseg, rn) -> begin
        match k.hd with
        | Segment.Left ->
            Option.map
              (fun r ->
                 let lseg, ln = force_prefixed_exn r in
                 make_internal p lseg ln rseg rn)
            @@ add' k.tail lseg ln
        | Segment.Right ->
            Option.map
              (fun r ->
                 let rseg, rn = force_prefixed_exn r in
                 make_internal p lseg ln rseg rn)
            @@ add' k.tail rseg rn
      end
  in

  match t with
  | EmptySet -> make_root k make_leaf
  | Root (p, n) ->
      match add' k p n with
      | None -> t
      | Some r -> make_root' r

let union n m =
  let rec merge seg0 n0 seg1 n1 =
    match NKey.of_key seg0, NKey.of_key seg1 with
    | None, None -> Node (merge_node n0 n1)
    | Some seg0, None -> Node (merge_node_ext n1 seg0 n0)
    | None, Some seg1 -> Node (merge_node_ext n0 seg1 n1)
    | Some seg0, Some seg1 -> merge_ext seg0 n0 seg1 n1
  and merge_ext seg0 n0 seg1 n1 =
    let prefix, seg0', seg1' = NKey.common_prefix seg0 seg1 in
    may_prefixed prefix @@
    match (NKey.of_key seg0', NKey.of_key seg1') with
    | None, None -> merge_node n0 n1
    | None, Some seg1 -> merge_node_ext n0 seg1 n1
    | Some seg0, None -> merge_node_ext n1 seg0 n0
    | Some { hd = hd0; tail = seg0 }, Some { hd = hd1; tail = seg1 } -> (
        assert (hd0 <> hd1);
        match hd0 with
        | Key.Left -> make_internal false seg0 n0 seg1 n1
        | Key.Right -> make_internal false seg1 n1 seg0 n0
      )
  and merge_node n0 n1 : snode =
    match n0.node with
    | Leaf -> add_empty n1
    | Extender (ext, n) ->
        add_empty @@ merge_node_ext n1 ext n
    | Internal (p0, lseg0, ln0, rseg0, rn0) -> (
        match n1.node with
        | Internal (p1, lseg1, ln1, rseg1, rn1) ->
            let lseg, ln = force_prefixed_exn @@ merge lseg0 ln0 lseg1 ln1 in
            let rseg, rn = force_prefixed_exn @@ merge rseg0 rn0 rseg1 rn1 in
            make_internal (p0 || p1) lseg ln rseg rn
        | _ -> merge_node n1 n0 )
  and merge_node_ext n0 seg1 n1 : snode =
    match n0.node with
    | Leaf -> make_extender seg1 n1
    | Extender (seg0, n0) ->
        make_extender' @@ merge_ext seg0 n0 seg1 n1
    | Internal (p, lseg0, ln0, rseg0, rn0) -> (
        match seg1.hd with
        | Key.Left ->
            let lseg, ln = force_prefixed_exn @@ merge lseg0 ln0 seg1.tail n1 in
            make_internal p lseg ln rseg0 rn0
        | Key.Right ->
            let rseg, rn = force_prefixed_exn @@ merge rseg0 rn0 seg1.tail n1 in
            make_internal p lseg0 ln0 rseg rn )
  in
  match (n, m) with
  | EmptySet, _ -> m
  | _, EmptySet -> n
  | Root (nseg, nn), Root (mseg, mn) ->
      make_root' @@ merge nseg nn mseg mn

module Builder = struct
  type builder = t * (int * t) list

  let empty_builder = (empty, [])

  let add k (s, ss) =
    let rec union_list (level, s) ss =
      match ss with
      | [] -> [ (level, s) ]
      | (level', s') :: ss' ->
          if level < level' then (level, s) :: ss
          else union_list (level + 1, union s s') ss'
    in
    let chunk_size = 1024 in
    let s = add k s in
    if length s < chunk_size then (s, ss) else (empty, union_list (0, s) ss)

  let build (s, ss) = List.fold_left (fun s (_, s') -> union s s') s ss
end

let of_seq seq =
  Builder.build
  @@ Seq.fold_left (fun b k -> Builder.add k b) Builder.empty_builder seq

let of_list ks =
  Builder.build
  @@ List.fold_left (fun b k -> Builder.add k b) Builder.empty_builder ks

let append_prefix prefix = function
  | EmptySet -> empty
  | Root (p, n) -> make_root (Key.append prefix p) n

let remove_prefix_exn prefix = function
  | EmptySet -> empty
  | Root (p, n) ->
      let _, remain, p = Key.common_prefix prefix p in
      if Key.is_empty remain then make_root p n
      else raise (Invalid_argument "Given segment is not a prefix")

let sorted_seq t =
  let rec sorted_seq prefix n =
    match n.node with
    | Leaf -> Seq.return prefix
    | Extender (seg, n) ->
        Seq.cons prefix @@ sorted_seq (Key.append prefix @@ NKey.to_key seg) n
    | Internal (p, lseg, l, rseg, r) ->
        let lprefix = Key.append prefix (Key.cons Key.Left lseg) in
        let rprefix = Key.append prefix (Key.cons Key.Right rseg) in
        let top = if p then Seq.cons prefix else fun x -> x in
        top @@ Seq.append (sorted_seq lprefix l) (sorted_seq rprefix r)
  in
  match t with EmptySet -> Seq.empty | Root (prefix, n) -> sorted_seq prefix n

let sorted_list t = List.of_seq @@ sorted_seq t

let search k t =
  let has_empty n =
    match n.node with Internal (p, _, _, _, _) -> p | _ -> true
  in
  let rec position accum k prefix n =
    let _, k, extend = Key.common_prefix k prefix in
    match (Key.cut k, Key.cut extend) with
    | None, None -> if has_empty n then `Exact accum else `Before accum
    | None, _ -> `Before accum
    | Some (khd, ktl), None -> position_non_empty accum khd ktl n
    | Some (khd, _), _ -> (
        match khd with
        | Segment.Left -> `Before accum
        | Segment.Right -> `Before (accum + n.length) )
  and position_non_empty accum khd ktl n =
    let k = Key.cons khd ktl in
    match n.node with
    | Leaf -> `Before (accum + 1)
    | Extender (seg, n) -> position (accum + 1) k (NKey.to_key seg) n
    | Internal (p, lseg, ln, rseg, rn) ->
        let accum = if p then accum + 1 else accum in
        let accum, seg, n =
          match khd with
          | Segment.Left -> (accum, lseg, ln)
          | Segment.Right ->
              (accum + ln.length (* FIXME: inefficient *), rseg, rn)
        in
        position accum ktl seg n
  in
  match t with EmptySet -> `Before 0 | Root (prefix, n) -> position 0 k prefix n

let find k t = match search k t with `Exact n -> Some n | _ -> None

let delete_if_found k n =
  let rec delete' k ext n =
    match NKey.of_key k, NKey.of_key ext with
    | None, None -> delete_empty n
    | None, Some _ -> None
    | Some k, None -> delete_non_empty k n
    | Some k, Some ext -> delete_ext k ext n
  and delete_ext k extend n =
    let _, k, extend' = NKey.common_prefix k extend in
    if not (Key.is_empty extend') then None
    else
      let n = match NKey.of_key k with
        | None -> delete_empty n
        | Some k -> delete_non_empty k n
      in
      match n with
      | None -> None
      | Some (i, Empty) -> Some (i, Empty)
      | Some (i, Node n) -> Some (i, Prefixed (extend, n))
      | Some (i, Prefixed (seg, n)) ->
          Some (i, Prefixed (NKey.append extend seg, n))
  and delete_empty n =
    match n.node with
    | Leaf -> Some (0, Empty)
    | Extender (ext, n) -> Some (0, Prefixed (ext, n))
    | Internal (_, lseg, ln, rseg, rn) ->
        let n = make_internal false lseg ln rseg rn in
        Some (0, Node n)
  and delete_non_empty k n =
    match n.node with
    | Leaf -> None
    | Extender (ext, n) -> (
        match delete_ext k ext n with
        | None -> None
        | Some (i, n) -> Some (i + 1, Node (make_extender' @@ n)) )
    | Internal (p, lseg, ln, rseg, rn) -> (
        let nskip = if p then 1 else 0 in
        let seg_rec, n_rec, seg_opp, n_opp, nskip =
          match k.hd with
          | Key.Left -> (lseg, ln, rseg, rn, nskip)
          | Key.Right -> (rseg, rn, lseg, ln, nskip + ln.length)
        in
        let opp = may_prefixed seg_opp n_opp in
        match delete' k.tail seg_rec n_rec with
        | None -> None
        | Some (i, n) ->
            Some
              ( i + nskip,
                make_internal' ~first_side:k.hd p n opp
              ))
  in
  match n with
  | EmptySet -> None
  | Root (p, n) -> (
      match delete' k p n with
      | None -> None
      | Some (ix, n) -> Some (ix, make_root' n) )

let delete k n = match delete_if_found k n with None -> n | Some (_, n) -> n

let nth i t =
  let rec nth i = function
    | Leaf ->
        assert (i = 0);
        Key.empty
    | Extender (ext, n) ->
        if i = 0 then
          Key.empty
        else
          Key.append (NKey.to_key ext) @@ nth (i - 1) n.node
    | Internal (p, lseg, ln, rseg, rn) ->
        if i = 0 && p then Key.empty
        else
          let i = if p then i - 1 else i in
          if i < ln.length then
            Key.cons Key.Left @@ Key.append lseg @@ nth i ln.node
          else
            Key.cons Key.Right @@ Key.append rseg @@ nth (i - ln.length) rn.node
  in
  if i < 0 then raise (Invalid_argument "negative index");
  match t with
  | EmptySet -> None
  | Root (p, n) ->
      if n.length <= i then None else Some (Key.append p @@ nth i n.node)

let nth_exn i t = Option.get @@ nth i t

let pop_nth i t =
  let rec pop_nth i :
    node -> key * snode'
    = function
    | Leaf ->
        assert (i = 0);
        (Key.empty, Empty)
    | Extender (ext, n) ->
        if i = 0 then (Key.empty, Prefixed (ext, n))
        else
          let k, n = pop_nth (i - 1) n.node in
          let k = Segment.append (NKey.to_key ext) k in
          let n = append_prefixed (NKey.to_key ext) n in
          (k, Node (make_extender' n))
    | Internal (p, lseg, ln, rseg, rn) ->
        if i = 0 && p then
          ( Key.empty,
            Node (make_internal false lseg ln rseg rn) )
        else
          let i = if p then i - 1 else i in
          let i, side, seg_rec, n_rec, opp =
            if i < ln.length then (i, Key.Left, lseg, ln, may_prefixed rseg rn)
            else (i - ln.length, Key.Right, rseg, rn, may_prefixed lseg ln)
          in
          let k, n_rec = pop_nth i n_rec.node in
          let n_rec = append_prefixed seg_rec n_rec in
          let k = Key.cons side @@ Key.append seg_rec k in
          let n = make_internal' ~first_side:side p n_rec opp in
          (k, n)
  in
  if i < 0 then raise (Invalid_argument "negative index");
  match t with
  | EmptySet -> None
  | Root (p, n) ->
      if n.length <= i then None
      else
        let k, n = pop_nth i n.node in
        let k = Key.append p k in
        Some (k, make_root' (append_prefixed p n))

let delete_nth_exn i t = snd @@ Option.get @@ pop_nth i t

let split_at i t =
  let rec split_at i n =
    if i = 0 then (Empty, Node n)
    else
      match n.node with
      | Leaf ->
          assert (i = 1);
          (Node make_leaf, Empty)
      | Extender (ext, n) ->
          let ext = NKey.to_key ext in
          let l, g = split_at (i - 1) n in
          let l =
            Node (make_extender' (append_prefixed ext l))
          in
          let g = append_prefixed ext g in
          (l, g)
      | Internal (p, lseg, ln, rseg, rn) ->
          let i = if p then i - 1 else i in
          if i < ln.length then
            let lt, ge = split_at i ln in
            let lt = make_internal' p (append_prefixed lseg lt) Empty in
            let ge =
              make_internal' false (append_prefixed lseg ge)
                (may_prefixed rseg rn)
            in
            (lt, ge)
          else
            let lt, ge = split_at (i - ln.length) rn in
            let lt =
              make_internal' p (may_prefixed lseg ln) (append_prefixed rseg lt)
            in
            let ge = make_internal' false Empty (append_prefixed rseg ge) in
            (lt, ge)
  in
  if i < 0 then raise (Invalid_argument "negative size");
  if length t < i then raise (Invalid_argument "size too large");
  match t with
  | EmptySet -> (empty, empty)
  | Root (p, n) ->
      let lt, ge = split_at i n in
      (make_root' (append_prefixed p lt), make_root' (append_prefixed p ge))


let range_of_zero f t =
  let offset o (i, j) = (o + i, o + j) in
  let rec range_of_zero prefix n =
    match n.node with
    | Leaf ->
        let fk = f prefix in
        if fk < 0 then (1, 1) else if fk = 0 then (0, 1) else (0 ,0)
    | Extender (ext, n) ->
        let ext = NKey.to_key ext in
        let fk = f prefix in
        if fk < 0 then
          offset 1 @@ range_of_zero (Key.append prefix ext) n
        else if fk = 0 then
          (0, 1 + find_ub (Key.append prefix ext) n)
        else
          (0, 0)
    | Internal (p, lseg, ln, rseg, rn) ->
        if not p then
          range_of_zero_split prefix lseg ln rseg rn
        else
          let fk = f prefix in
          if fk < 0 then
            offset 1 @@ range_of_zero_split prefix lseg ln rseg rn
          else if fk = 0 then
            (0, 1 + find_ub_split prefix lseg ln rseg rn)
          else
            (0, 0)
  and range_of_zero_split prefix lseg ln rseg rn =
    let left_prefix = Key.append prefix(Key.cons Key.Left lseg) in
    let (l_begin, l_end) = range_of_zero left_prefix ln in
    if l_end < ln.length then (* left subtree has potive key *)
      (l_begin, l_end)
    else
      let right_prefix = Key.append prefix (Key.cons Key.Right rseg) in
      if l_begin < ln.length then
        let r_end = find_ub right_prefix rn in
        (l_begin, ln.length + r_end)
      else (* left subtree only has negative keys *)
        offset ln.length @@ range_of_zero right_prefix rn
  and find_ub prefix n =
    match n.node with
    | Leaf ->
        let fk = f prefix in if fk = 0 then 1 else 0
    | Extender (ext, n) ->
        let ext = NKey.to_key ext in
        if f prefix = 0 then 1 + find_ub (Key.append prefix ext) n
        else 0
    | Internal (p, lseg, ln, rseg, rn) ->
        if not p then
          find_ub_split prefix lseg ln rseg rn
        else if f prefix = 0 then
          1 + find_ub_split prefix lseg ln rseg rn
        else
          0
  and find_ub_split prefix lseg ln rseg rn =
    let left_prefix = Key.append prefix (Key.cons Key.Left lseg) in
    let l_end = find_ub left_prefix ln in
    if l_end < ln.length then (* left subtree has postive key *)
      l_end
    else
      let right_prefix = Key.append prefix (Key.cons Key.Right rseg) in
      ln.length + find_ub right_prefix rn
  in
  match t with EmptySet -> (0, 0) | Root (prefix, n) -> range_of_zero prefix n

module C = Plebeia.Xcstruct

  type ctor = LEAF | EXTENDER | INTERNAL_E | INTERNAL_F
  let ctor_to_int = function
    | LEAF -> 0 | EXTENDER -> 1 | INTERNAL_E -> 2 | INTERNAL_F -> 3
  let int_to_ctor = function
    | 0 -> LEAF | 1 -> EXTENDER | 2 -> INTERNAL_E | 3 -> INTERNAL_F
    | _ -> assert false

  let pack_ctors (ct0, ct1, ct2, ct3) =
    Char.unsafe_chr @@
    ctor_to_int ct0 * 64 + ctor_to_int ct1 * 16 +
    ctor_to_int ct2 * 4 + ctor_to_int ct3

  let unpack_ctors_table = Array.init 256 @@ fun b ->
    let ct3 = int_to_ctor @@ b mod 4 in
    let b = b / 4 in
    let ct2 = int_to_ctor @@ b mod 4 in
    let b = b / 4 in
    let ct1 = int_to_ctor @@ b mod 4 in
    let b = b / 4 in
    let ct0 = int_to_ctor @@ b mod 4 in
    [ct0; ct1; ct2; ct3]

  let unpack_ctors b = Array.get unpack_ctors_table @@ Char.code b

  let rec unpack_seq seq =
    match seq () with
     | Seq.Nil -> Seq.empty
     | Seq.Cons (b, seq) ->
         Seq.append (List.to_seq (unpack_ctors b)) (unpack_seq seq)

  let rec ctors_seq n () =
    match n.node with
      | Leaf -> Seq.Cons (LEAF, Seq.empty)
      | Extender (_, n) ->
          Seq.Cons (EXTENDER, ctors_seq n)
      | Internal (p, _, ln, _, rn) ->
          let head = if p then INTERNAL_F else INTERNAL_E in
          Seq.Cons (head, Seq.append (ctors_seq ln) (ctors_seq rn))

  let ctor_packed_seq t =
    let rec take_rev n rev seq =
      if n <= 0 then (rev, Some seq)
      else
        match seq () with
         | Seq.Nil -> (rev, None)
         | Seq.Cons (c, seq) -> take_rev (n - 1) (c :: rev) seq
    in
    let take_packed seq =
      match take_rev 4 [] seq with
      | [], None -> None
      | [c0], None -> Some (pack_ctors (c0, LEAF, LEAF, LEAF), None)
      | [c1; c0], None -> Some (pack_ctors (c0, c1, LEAF, LEAF), None)
      | [c2; c1; c0], None -> Some (pack_ctors (c0, c1, c2, LEAF), None)
      | [c3; c2; c1; c0], s -> Some (pack_ctors (c0, c1, c2, c3), s)
      | _ -> assert false
    in
    let rec packed_seq seq () =
      match take_packed seq with
      | None -> Seq.Nil
      | Some (x, None) -> Seq.Cons (x, Seq.empty)
      | Some (x, (Some seq)) -> Seq.Cons (x, packed_seq seq)
    in
    match t with
    | EmptySet -> Seq.return (Char.chr 0)
    | Root (_, n) -> Seq.cons (Char.chr 1) (packed_seq @@ ctors_seq n)

  let save_short_string cstr i str  =
    let l = String.length str in
    assert (l < 256);
    C.set_uint8 cstr i l;
    C.blit_from_string str 0 cstr (i + 1) l;
    i + 1 + l

  let save_string cstr i str =
    let l = String.length str in
    C.set_uint64 cstr i (Int64.of_int l);
    C.blit_from_string str 0 cstr (i + 8) l;
    i + 8 + l

  let save_seg cstr i seg =
    let enc = Key.Serialization.encode seg in
    save_short_string cstr i enc

  let seg_size seg = Key.length seg / 8 + 2

  let save_segs cstr i t =
    let s = save_seg cstr in
    let rec save_segs i n =
      match n.node with
      | Leaf -> i
      | Extender (ext, n) -> save_segs (s i (NKey.to_key ext)) n
      | Internal (_, lseg, ln, rseg, rn) ->
          let i = save_segs (s i lseg) ln in
          save_segs (s i rseg) rn
    in
    match t with
    | EmptySet -> i
    | Root (prefix, n) -> save_segs (s i prefix) n

  let save cstr i set =
    let ctors_string = String.of_seq @@ ctor_packed_seq set in
    let i = save_string cstr i ctors_string in
    save_segs cstr i set


  let size_margin margin set =
    let rec num_ctors_margin margin bits n =
      let (margin, bits) =
        if bits < 2
        then (margin - 1, bits + 8 - 2)
        else (margin, bits - 2)
      in
      if margin < 0 then None
      else
        match n.node with
        | Leaf -> Some (margin, bits)
        | Extender (_, n) -> num_ctors_margin margin bits n
        | Internal (_, _, ln, _, rn) ->
            Option.bind (num_ctors_margin margin bits ln)
            @@ fun (margin, bits) -> num_ctors_margin margin bits rn
    in
    let rec size_margin margin seg n =
      let margin = margin - seg_size seg in
      if margin < 0 then None
      else
        match n.node with
        | Leaf -> Some margin
        | Extender (seg, n) -> size_margin margin (NKey.to_key seg) n
        | Internal (_, lseg, ln, rseg, rn) ->
            Option.bind (size_margin margin lseg ln) @@ fun margin ->
            size_margin margin rseg rn
    in
    let margin = margin - 9 (* fixed *) in
    if margin < 0 then None
    else
      match set with
      | EmptySet -> Some margin
      | Root (p, n) ->
          Option.bind (num_ctors_margin margin 0 n) @@ fun (margin, _) ->
          size_margin margin p n

  let size n =
    match size_margin Int.max_int n with
    | None -> Int.max_int
    | Some margin -> Int.max_int - margin

  let load_short_string cstr i =
    let l = C.get_uint8 cstr i in
    let str = C.copy cstr (i + 1) l in
    i + 1 + l, str

  let load_string cstr i =
    let l = Int64.to_int @@ C.get_uint64 cstr i in
    let str = C.copy cstr (i + 8) l in
    i + 8 + l, str

  let load_seg cstr i =
    let i, enc = load_short_string cstr i in
    let seg = Option.get (Key.Serialization.decode enc) in
    i, seg

  let rec load_prefixed ctors cstr i =
    let i, seg = load_seg cstr i in
    let (ctor, ctors) = match ctors () with
      | Seq.Nil -> assert false
      | Seq.Cons (ctor, ctors) -> (ctor, ctors)
    in
    let i, n, ctors = match ctor with
      | LEAF ->
          i, make_leaf, ctors
      | EXTENDER ->
          let i, seg, n, ctors = load_prefixed ctors cstr i in
          i, make_extender (NKey.of_key_exn seg) n, ctors
      | INTERNAL_E | INTERNAL_F ->
          let p = ctor = INTERNAL_F in
          let i, lseg, ln, ctors = load_prefixed ctors cstr i in
          let i, rseg, rn, ctors = load_prefixed ctors cstr i in
          i, make_internal p lseg ln rseg rn, ctors
    in
    i, seg, n, ctors

  let load cstr i =
    let i, ctors_string = load_string cstr i in
    match String.to_seq ctors_string () with
    | Seq.Nil -> assert false
    | Seq.Cons (b, seq) ->
        match Char.code b with
        | 0 -> i, empty
        | 1 ->
            let i, seg, n, _ = load_prefixed (unpack_seq seq) cstr i in
            i, make_root seg n
        | _ -> assert false

module Debug = struct
  let rec length_node_full n =
    match n.node with
    | Leaf -> 1
    | Extender (_, n) -> 1 + length_node_full n
    | Internal (p, _, l, _, r) ->
        (if p then 1 else 0) + length_node_full l + length_node_full r

  let length_full t =
    match t with EmptySet -> 0 | Root (_, n) -> length_node_full n

  type stat = {
    leaves : int;
    extenders : int;
    internals : int;
    sides : int;
    all_segs : key list;
  }

  let add_stat stat1 stat2 =
    {
      leaves = stat1.leaves + stat2.leaves;
      extenders = stat1.extenders + stat2.extenders;
      internals = stat1.internals + stat2.internals;
      sides = stat1.sides + stat2.sides;
      all_segs = stat1.all_segs @ stat2.all_segs;
    }

  let zero_stat =
    { leaves = 0; extenders = 0; internals = 0; sides = 0; all_segs = [] }

  let stat t =
    let rec stat n =
      match n.node with
      | Leaf -> { zero_stat with leaves = 1 }
      | Extender (seg, n) ->
          add_stat (stat n)
            {
              zero_stat with
              extenders = 1;
              sides = Key.length seg.tail;
              all_segs = [ seg.tail ];
            }
      | Internal (_, lseg, ln, rseg, rn) ->
          let lstat = stat ln in
          let rstat = stat rn in
          add_stat (add_stat lstat rstat)
            {
              zero_stat with
              internals = 1;
              sides = Key.length lseg + Key.length rseg;
              all_segs = [ lseg; rseg ];
            }
    in
    match t with
    | EmptySet -> zero_stat
    | Root (seg, n) ->
        add_stat (stat n)
          { zero_stat with sides = Key.length seg; all_segs = [ seg ] }

  let stat_pp fmt stat =
    Format.fprintf fmt " leaves %d\n" stat.leaves;
    Format.fprintf fmt " extenders %d\n" stat.extenders;
    Format.fprintf fmt " internals %d\n" stat.internals;
    Format.fprintf fmt " sides %d\n" stat.sides;
    List.iter
      (fun seg -> Format.fprintf fmt "seg %d\n" (Key.length seg))
      stat.all_segs
end
