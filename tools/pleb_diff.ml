(* input 2 trees

   output diff of buds and leaves, checked their correctness
   by applying them to the original.

   Checked diffs and applied them succcessfully in 63.9 mins for 22G data
   in Jun's machine kurama.
*)

open Plebeia.Internal
open Utils
open Node

let () =
  let path = Sys.argv.(1) in
  let vc = Vc.open_ ~mode:Storage.Reader path in
  let roots = Vc.roots vc in
  let context = Vc.context vc in

  let nroots = Commit.length roots in
  let processed = ref 0 in
  let t0 = Unix.gettimeofday () in

  let rec loop = function
    | [] -> ()
    | root::jobs ->
        let root_index = root.Commit.index in
        incr processed;

        let n1 = match root.Commit.parent with
          | None -> View (_Bud (None, Not_Indexed, Not_Hashed))
          | Some i when i = Index.one -> assert false
          | Some i ->
              let v = Node_storage.load_node context i Not_Extender in
              View v
        in
        let Cursor (_, n2, _, _) = from_Some @@ Vc.checkout vc root.Commit.hash in

        let diffs = Diff.diff context n1 n2 in
        Format.eprintf "checking index %a@." Index.pp root_index;
        Format.eprintf "%s: %d diffs@." (Commit.Hash.to_hex_string root.Commit.hash) (List.length diffs);
        Diff.apply_and_check context n1 n2 diffs;

        let t = Unix.gettimeofday () in
        Format.eprintf "ETC: %.1f mins (Passed %.1f mins)@."
          ((t -. t0) /. float !processed *. float (nroots - !processed) /. 60.0)
          ((t -. t0) /. 60.0);

        loop @@ (Commit.children roots root) @ jobs
  in
  loop @@ Commit.genesis roots
