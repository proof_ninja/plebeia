(*

   Scan the root hashes and prints out branches.

   $ dune exec ./pleb_count_branches.exe plebeia.context
*)
open Plebeia.Internal
open Utils

let (//) = Filename.concat

let () =
  let path = Sys.argv.(1) in
  let ctxt = Vc.open_ ~mode:Storage.Reader path in
  let roots = Vc.roots ctxt in
  let open Commit in
  match genesis roots with
  | [] ->
      Format.eprintf "Roots has no genesis hashes@.";
      assert false
  | (_::_::_ as hs) ->
      Format.eprintf "Roots have %d genesis hashes@." (List.length hs);
      assert false
  | [genesis] ->
      (* XXX calls children twice. not optimal *)
      let rec loop f = function
        | [] -> ()
        | e::es ->
            f e;
            let es' = Commit.children roots e in
            loop f (es @ es')
      in
      loop (fun e ->
          match Commit.children roots e with
          | [] -> ()
          | [_] -> ()
          | children  ->
              let { Commit.hash = h ; _ } = from_Some @@ Commit.find_by_index roots e.index in
              Format.eprintf "%d children: %S@."
                (List.length children)
                (Commit.Hash.to_hex_string h)
        ) [genesis]
