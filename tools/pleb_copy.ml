open Plebeia

let () =
  let path1 = Sys.argv.(1) in
  let vc1 = Vc.open_ ~mode:Storage.Reader path1 in

  let path2 = Sys.argv.(2) in
  let vc2 = Vc.create ~node_cache:Node_cache.(create default_config) path2 in

  let commits = Commit.to_list @@ Vc.roots vc1 in

  match Copy.copy vc1 commits vc2 with
  | Error e ->
      Log.fatal "Error: %a" Error.pp e;
      Error.raise e
  | Ok _ ->
      Log.notice "done"
