open Plebeia
open Test_utils

module TP = Plebeia_tribune.Persist_plus
module S = Segment

let prepare_data
    plebeia_context_path (* $HOME/.tezos-node/plebeia   w/o ".context" *)
    tribune_context_path (* $HOME/.tezos-node/tribune.context  w/ ".context"! *)
    dat_path (* $HOME/.tezos-node/dat.dat *)
  =
  if Sys.file_exists tribune_context_path then
    Unix.unlink tribune_context_path;

  let storage = TP.TStorage.create tribune_context_path in

  let vc = Vc.open_ ~mode:Storage.Reader plebeia_context_path in

  let last_root =
    let roots = Vc.roots vc in
    match Commit.read_the_latest roots with
    | Some root -> root
    | None -> assert false
  in

  let idxs = ref [] in

  let dump c =
    Format.eprintf "Dumping Plebeia tree %a@." Commit.Hash.pp c.Commit.hash;
    let Cursor.Cursor (_, n, ctxt,_) as cur = from_Some @@ Vc.checkout vc c.Commit.hash in

    let n = match Node.view ctxt n with
      | Bud (Some n, _, _) -> n
      | _ -> assert false
    in

    let rec f st rev_sides n =
      let v = Node.view ctxt n in
      let idx = from_Some @@ Node.index_of_view v in
      idxs := idx :: !idxs;
      match v with
      | Bud _ -> assert false
      | Leaf (_, Indexed i, _) ->
          ((S.normalize @@ S.of_sides @@ List.rev rev_sides), i) :: st
      | Leaf _ -> assert false
      | Internal (nl, nr, _, _) ->
          let st = f st (S.Right::rev_sides) nr in
          f st (S.Left::rev_sides) nl
      | Extender (seg, n, _, _) ->
          f st (List.rev_append (S.to_sides seg) rev_sides) n
    in
    let kvs = f [] [] n in
    Format.eprintf "%d plebeia leaves@." @@ List.length kvs;

    kvs, cur
  in

  let kvs, cur = dump last_root in

  let module T = TP.Make(struct
      let storage = storage
      let max_size_in_bytes = 4096
      let check = false
    end) in

  let tribune = T.init kvs in
  Format.eprintf "saving the tribune tree with %d kvs...@." @@ List.length kvs;
  T.save tribune;
  TP.TStorage.commit storage;
  Format.eprintf "saved the tribune tree@.";
  TP.TStorage.close storage;

  Format.eprintf "saving the data...@.";
  let ks = List.map fst kvs in
  let oc = open_out_bin dat_path in
  Format.eprintf "tribune depth %d  index %a@." (T.depth tribune) Index.pp
    (from_Some @@ T.index tribune);
  (* beware of the type *)
  output_value oc (from_Some @@ Cursor.index cur,
                   from_Some @@ T.index tribune, ks);
  close_out oc;
  Format.eprintf "saved the keys@."

let test_tribune
    plebeia_context_path (* $HOME/.tezos-node/plebeia  w/o ".context"! *)
    tribune_context_path (* $HOME/.tezos-node/tribune.context  w/ ".context"! *)
    tribune_index
    keys
  =
  let plebeia_ctxt = Context.open_ ~mode:Reader (plebeia_context_path ^ ".context") in

  let storage = TP.TStorage.open_ ~mode:Reader tribune_context_path in
  let module T = TP.Make(struct
      let storage = storage
      let max_size_in_bytes = 4096
      let check = false
    end)
  in

  prerr_endline "test starts!";

  let tribune = T.of_index tribune_index in

  let rec read_tribune n keys =
    if n = 0 then keys
    else match keys with
      | [] -> []
      | l::ls ->
          match T.find l tribune with
          | Some i ->
              ignore @@ Node_storage.load_node plebeia_ctxt i Node.Not_Extender;
              read_tribune (n-1) ls
          | None ->
              Format.eprintf "??? %a@." Segment.pp l;
              Format.eprintf "keys %d@." @@ List.length @@ T.elements tribune;
              Format.eprintf "depth %d@." (T.depth tribune);
              assert false
  in

  let sz = 10000 in

  let rec loop n keys =
    let cur_loaded = !T.loaded in
    let keys, tsecs = with_time @@ fun () -> read_tribune sz keys in
    let n = n + sz in
    let new_loaded = !T.loaded - cur_loaded in
    Format.eprintf "%d tribune_time: %.3f newly_loaded_blocks: %d@." n tsecs new_loaded;
    match keys with
    | [] -> ()
    | _ -> loop n keys
  in
  loop 0 keys;

  (* Run again with disk cache *)
  Format.eprintf "Run again with disk cache...@.";
  T.may_forget tribune;
  loop 0 keys;
  Format.eprintf "Run again with disk cache...@.";
  T.may_forget tribune;
  loop 0 keys

let test_plebeia
    plebeia_context_path (* $HOME/.tezos-node/plebeia  w/o ".context"! *)
    plebeia_index
    keys
  =
  let ctxt = Context.open_ ~mode:Reader (plebeia_context_path ^ ".context") in

  let cur0 = Cursor.(_Cursor (_Top, Node.Disk (plebeia_index, Node.Not_Extender), ctxt, Info.empty)) in

  let rec read_plebeia n leaves cur =
    if n = 0 then cur, leaves
    else match leaves with
      | [] -> cur, leaves
      | l::ls ->
          let cur, _ = Result.from_Ok @@ Cursor.get cur l in
          read_plebeia (n-1) ls cur
  in

  let sz = 10000 in

  let module I64Set = Set.Make(Int64) in

  let rec loop loaded_blocks n cur leaves =
    let r = ref [] in
    Storage.set_accessed_indices (Some r);
    let (cur, leaves), psecs = with_time @@ fun () -> read_plebeia sz leaves cur in
    let r' = List.sort_uniq compare @@ List.map Index.to_int64 !r in
    let blks = List.sort_uniq compare @@ List.map (fun i -> Int64.div i 128L) r' in
    let new_blocks = ref 0 in
    let loaded_blocks = List.fold_left (fun loaded b ->
        if not @@ I64Set.mem b loaded then incr new_blocks;
        I64Set.add b loaded) loaded_blocks blks
    in
    let n = n + sz in
    Format.eprintf "%d plebeia %.3f traversed_cells: %d newly_loaded_blocks: %d @." n psecs
      (List.length r') !new_blocks;
    match leaves with
    | [] -> ()
    | _ -> loop loaded_blocks n cur leaves
  in
  loop I64Set.empty 0 cur0 keys;
  Format.eprintf "Rerun the same test possibly with disk cache@.";
  loop I64Set.empty 0 cur0 keys;
  Format.eprintf "Rerun the same test possibly with disk cache@.";
  loop I64Set.empty 0 cur0 keys

let () =
  match Sys.argv.(1) with
  | "prepare" ->
      prepare_data
        Sys.argv.(2)
        Sys.argv.(3)
        Sys.argv.(4)

  | "test_plebeia" ->
      let ic = open_in Sys.argv.(4) in
      let (plebeia_index, _tribune_index, ks) = input_value ic in
      close_in ic;
      with_random @@ fun rng ->
      let ks = Gen.shuffle ks rng in
      (* test_tribune Sys.argv.(3) tribune_index ks; *)
      test_plebeia Sys.argv.(2) plebeia_index ks

  | "test_tribune" ->
      let ic = open_in Sys.argv.(4) in
      let (_plebeia_index, tribune_index, ks) = input_value ic in
      close_in ic;
      Format.eprintf "index: %a  keys: %d@." Index.pp tribune_index (List.length ks);
      with_random @@ fun rng ->
      let ks = Gen.shuffle ks rng in
      test_tribune Sys.argv.(2) Sys.argv.(3) tribune_index ks

  | _ -> assert false
