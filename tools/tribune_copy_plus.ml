(* make a Tribune copy of 5 cycles:

   20480 blocks: 4096 * 5
*)

open Plebeia
open Utils

let test () =
  let module SS = Plebeia_tribune.Segment_set in
  let module TP = Plebeia_tribune.Persist_plus in
  let storage = TP.TStorage.create "tribune.context" in

  (* commits uses 32byte/cell *)
  let storage_commits = Storage.create "tribune.commits" in
  let troots = Commit.create'
      ~storage_context: storage_commits
      ~storage_roots: None
  in

  prerr_endline "tribune.context opened";
  let module T = TP.Make(struct
      let storage = storage
      let max_size_in_bytes = 4096
      let check = true
    end) in
  prerr_endline "TP.Make";

  let path = Sys.argv.(1) in
  let vc = Vc.open_ ~mode:Storage.Reader path in
  let roots = Vc.roots vc in (* XXX silly to load all *)

  let last_root =
    match Commit.read_the_latest roots with
    | Some root -> root
    | None -> assert false
  in

  let commits =
    let rec f st c n =
      if n = 1 then c::st
      else
        let c' =
          from_Some @@ Result.from_Ok @@ Commit.parent roots c
        in
        f (c::st) c' (n-1)
    in
    f [] last_root (4096 * 5)
  in

  prerr_endline "commits got";

  let module S = Segment in

  let dump c =
    Format.eprintf "Dumping %a@." Commit.Hash.pp c.Commit.hash;
    let Cursor.Cursor (_, n, ctxt,_) = from_Some @@ Vc.checkout vc c.Commit.hash in

    let n = match Node.view ctxt n with
      | Bud (Some n, _, _) -> n
      | _ -> assert false
    in

    let rec g ixs n =
      match Node.view ctxt n with
      | Bud _ -> assert false
      | Leaf (_, Indexed i, _) ->
          SS.of_list [S.empty], i :: ixs
      | Leaf _ -> assert false
      | Internal (nl, nr, _, _) ->
          let sr, ixs = g ixs nr in
          let sr = SS.append_prefix (S.of_sides [S.Right]) sr in
          let sl, ixs = g ixs nl in
          let sl = SS.append_prefix (S.of_sides [S.Left]) sl in
          SS.union sl sr, ixs
      | Extender (seg, n, _, _) ->
          let s, ixs = g ixs n in
          let s = SS.append_prefix seg s in
          s, ixs
    in
    let ((ss, vs), time) = with_time @@ fun () ->
      let (ss, vs) = g [] n in
      Format.eprintf "%d pleaves@." @@ SS.length ss;
      (ss, vs)
    in
    Format.eprintf "Collect leaves : %f secs\n" time;
    let (n, time) = with_time @@ fun () -> T.init' ss (Array.of_list vs) in
    Format.eprintf "Init' : %f secs\n" time;
    n
  in

  let rec f n prev parent_commit_i i = function
    | [] -> T.bench i n
    | c::cs ->
        match prev with
        | None ->
            let n = dump c in
            T.save n;
            let idx = from_Some @@ T.index n in
            let _ent, commit_i = Commit.add troots ~parent:parent_commit_i c.Commit.hash idx ~info:None in
            Commit.sync troots;
            T.bench i n;
            f n (Some c) (Some commit_i) (i+1) cs
        | Some prev ->
            Format.eprintf "App diff %a@." Commit.Hash.pp c.Commit.hash;
            let Cursor.Cursor (_, pn, ctxt, _) = from_Some @@ Vc.checkout vc c.Commit.hash in
            let Cursor.Cursor (_, prevpn, _, _) = from_Some @@ Vc.checkout vc prev.Commit.hash in
            let diff = Diff.diff ctxt prevpn pn in
            let (mods, adds, dels) = List.fold_left (fun (mods, adds, dels) -> function
                | Diff.Add (pn, segs) ->
                    let bindings =
                      List.map (fun (seg, pn) ->
                          match Node.view ctxt pn with
                          | Leaf (_, Indexed i, _) ->
                              (Segment.Segs.append_seg segs seg, i)
                          | _ -> assert false)
                      @@ Node_tools.ls ctxt pn
                    in
                    if bindings = [] then begin
                      (* This strongly indicates Diff or ls is buggy *)
                      Format.eprintf "DIFF Add 0!?!? @[%a@]@." Node.pp pn
                    end;
                    (mods, bindings :: adds, dels)
                | Del segs ->
                    begin match Segment.Segs.finalize segs with
                      | [seg] ->
                          (mods, adds, seg :: dels)
                      | _ -> assert false
                    end
                | CleanBud _segs -> assert false
                | ModLeaf (pn, _v, segs) ->
                    begin match Node.view ctxt pn with
                      | Leaf (_, Indexed i, _) ->
                          ((segs,i) :: mods, adds, dels)
                      | _ -> assert false
                    end) ([], [], []) diff
            in
            (* XXX number of deletions in total can be huge like 1352343
               rather than list all the deletions at the beginning,
               find_under should be called on demand
            *)
            Format.eprintf "%d mods, %d adds, %d dels ...@."
              (List.length mods)
              (List.length adds)
              (List.length dels);

            let n, secs =
              with_time @@ fun () ->

              let n, secs =
                with_time @@ fun () ->
                List.fold_left (fun n (segs, i) ->
                    T.adds
                      [ (match Segment.Segs.finalize segs with
                            | [seg] -> seg
                            | _ -> assert false),
                        i ] n) n mods
              in
              Format.eprintf "Mods %d in %.2f ses@."
                (List.length mods)
                secs;

              let n = List.fold_left (fun n seg -> T.delete_under seg n) n dels in

              let n =
                List.fold_left (fun n add ->
                    let kvs = List.map (fun (segs,i) ->
                        (match Segment.Segs.finalize segs with
                         | [seg] -> seg
                         | _ -> assert false), i) add
                    in
                    T.bulk_adds kvs n) n adds
              in

              T.rebalance n
            in
            Format.eprintf "%d mods, %d adds, %d dels in %.2f secs@."
              (List.length mods)
              (List.length adds)
              (List.length dels)
              secs;
            let orig_saved = !T.saved in
            T.save n;
            let idx = from_Some @@ T.index n in
            let _ent, commit_i = Commit.add troots ~parent:parent_commit_i c.Commit.hash idx ~info:None in
            Commit.sync troots;
            let new_saved = !T.saved in
            Format.eprintf "%d mods, %d adds, %d dels, %d nodes saved@."
              (List.length mods)
              (List.length adds)
              (List.length dels)
              (new_saved - orig_saved);

            if i mod 128 = 0 then T.bench i n;
            T.may_forget n;
            f n (Some c) (Some commit_i) (i+1) cs

  in
  f T.empty None None 0 commits;

  TP.TStorage.close storage;
  Commit.close troots;
  Storage.close storage_commits

let () = test ()
