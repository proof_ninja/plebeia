(* This uses Persist, B-tree implementation.
   We no longer use B-tree but B+, Persist_plus.
   Use tribune_copy_plus.ml instead.
*)
(* make a Tribune copy of 5 cycles:

   20480 blocks: 4096 * 5
*)

open Plebeia
open Utils

let test () =
  let module TP = Plebeia_tribune.Persist in
  let storage = TP.TStorage.create "tribune.context" in
  prerr_endline "tribune.context opened";
  let module T = TP.Make(struct let storage = storage let max_size_in_bytes = 4096 end) in
  prerr_endline "TP.Make";

  let path = Sys.argv.(1) in
  let vc = Vc.open_ ~mode:Storage.Reader path in
  let roots = Vc.roots vc in (* XXX silly to load all *)

  let last_root =
    match Commit.read_the_latest roots with
    | Some root -> root
    | None -> assert false
  in

  let commits =
    let rec f st c n =
      if n = 1 then c::st
      else
        let c' =
          from_Some @@ Result.from_Ok @@ Commit.parent roots c
        in
        f (c::st) c' (n-1)
    in
    f [] last_root (4096 * 5)
  in

  prerr_endline "commits got";

  let module S = Segment in

  let dump c =
    Format.eprintf "Dumping %a@." Commit.Hash.pp c.Commit.hash;
    let Cursor.Cursor (_, n, ctxt,_) = from_Some @@ Vc.checkout vc c.Commit.hash in

    let n = match Node.view ctxt n with
      | Bud (Some n, _, _) -> n
      | _ -> assert false
    in

    let rec f st rev_sides n =
      match Node.view ctxt n with
      | Bud _ -> assert false
      | Leaf (_, Indexed i, _) ->
          ((S.normalize @@ S.of_sides @@ List.rev rev_sides), i) :: st
      | Leaf _ -> assert false
      | Internal (nl, nr, _, _) ->
          let st = f st (S.Right::rev_sides) nr in
          f st (S.Left::rev_sides) nl
      | Extender (seg, n, _, _) ->
          f st (List.rev_append (S.to_sides seg) rev_sides) n
    in
    let kvs = f [] [] n in
    Format.eprintf "%d pleaves@." @@ List.length kvs;

    T.init kvs
  in

  let rec f n prev i = function
    | [] -> T.bench i n
    | c::cs ->
        match prev with
        | None ->
            let n = dump c in
            T.save n;
            T.bench i n;
            f n (Some c) (i+1) cs
        | Some prev ->
            Format.eprintf "App diff %a@." Commit.Hash.pp c.Commit.hash;
            let Cursor.Cursor (_, pn, ctxt, _) = from_Some @@ Vc.checkout vc c.Commit.hash in
            let Cursor.Cursor (_, prevpn, _, _) = from_Some @@ Vc.checkout vc prev.Commit.hash in
            let diff = Diff.diff ctxt prevpn pn in
            let (mods, adds, dels) = List.fold_left (fun (mods, adds, dels) -> function
                | Diff.Add (pn, segs) ->
                    let bindings =
                      List.map (fun (seg, pn) ->
                          match Node.view ctxt pn with
                          | Leaf (_, Indexed i, _) ->
                              (Segment.Segs.append_seg segs seg, i)
                          | _ -> assert false)
                      @@ Node_tools.ls ctxt pn
                    in
                    (mods, bindings @ adds, dels)
                | Del segs ->
                    begin match Segment.Segs.finalize segs with
                      | [seg] ->
Format.eprintf "find under...@.";
                          let bindings = T.find_under seg n in
Format.eprintf "find under: found %d@." (List.length bindings);
                          let segs =
                            List.map (fun (sides, _) ->
                                Segment.append seg
                                  (Segment.of_sides sides)) bindings
                          in
Format.eprintf "del %d@." (List.length segs);
                          (mods, adds, segs @ dels)
                      | _ -> assert false
                    end
                | CleanBud _segs -> assert false
                | ModLeaf (pn, _v, segs) ->
                    begin match Node.view ctxt pn with
                      | Leaf (_, Indexed i, _) ->
                          ((segs,i) :: mods, adds, dels)
                      | _ -> assert false
                    end) ([], [], []) diff
            in
            (* XXX number of deletions in total can be huge like 1352343
               rather than list all the deletions at the beginning,
               find_under should be called on demand
            *)
            Format.eprintf "%d mods, %d adds, %d dels ...@."
              (List.length mods)
              (List.length adds)
              (List.length dels);
            let n =
              T.adds (List.map (fun (segs,i) ->
                  (match Segment.Segs.finalize segs with
                   | [seg] -> seg
                   | _ -> assert false), i) (mods @ adds)) n
            in

            let n = T.deletes dels n in
            let orig_saved = !T.saved in
            T.save n;
            let new_saved = !T.saved in
            Format.eprintf "%d mods, %d adds, %d dels, %d nodes saved@."
              (List.length mods)
              (List.length adds)
              (List.length dels)
              (new_saved - orig_saved);
            if i mod 256 = 0 then T.bench i n;
            T.may_forget n;
            f n (Some c) (i+1) cs
  in
  f T.empty None 0 commits;

  TP.TStorage.close storage

let () = test ()
