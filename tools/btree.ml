open Plebeia
open Utils

module S = Segment

module A = struct
  include Array
  let (++) = Array.append
end

let get = A.unsafe_get
let (++) = A.(++)

let max_size_in_bytes = 4096

module Model : sig
  type t =
    { root : bool
    ; ents : (S.t * Index.t) array
    ; subnodes : t array (* leaves+1 *) option
    }

  val empty : t
  val is_empty : t -> bool
  val is_leaf : t -> bool
  val validate : t -> t
  val build : bool (* root ? *) -> (S.t * Index.t) array -> t array option -> t

(*
  val upsert : t -> int -> Index.t -> t
  val inject : t -> int -> (S.t * Index.t) -> (t * t) option -> t
  val set_subnode : t -> int -> t -> t
*)

end = struct
  (* Without optimization *)
  type t =
    { root : bool
    ; ents : (S.t * Index.t) array
    ; subnodes : t array (* leaves+1 *) option
    }

  let empty = { root= true; ents= [||]; subnodes= None }
  let is_empty n = n = empty

  let validate n =
    let nents = A.length n.ents in
    if is_empty n then () else assert (nents > 0);
    for i = 0 to nents - 2 do
      let seg , _ = get n.ents i in
      let seg', _ = get n.ents (i+1) in
      assert (S.compare seg seg' = -1)
    done;
    begin match n.subnodes with
    | None -> assert (A.length n.ents > 0)
    | Some subnodes ->
        let nsubnodes = A.length subnodes in
        assert (nents + 1 = nsubnodes)
    end;
    n

  let is_leaf n = n.subnodes = None

  let build root ents subnodes = validate { root; ents; subnodes }
end

module SS : sig
  type t = private S.t
  val of_segment : S.t -> t
  val to_segment : t -> S.t
  val compare : t -> t -> int
  val is_empty : t -> bool
  val common_prefix : t -> t -> t * t * t
  val equal : t -> t -> bool
  val append : t -> t -> t
  val length : t -> int
  val normalize : t -> t
end = struct
  include S
  let of_segment t = t
  let to_segment t = t
end

module Raw_node : sig
  type t = private
    { root : bool
    ; prefix : S.t
    ; ents : (SS.t * Index.t) array
    ; subnodes : t array (* leaves+1 *) option
    }

  val is_leaf : t -> bool

  val update : t -> int -> Index.t -> t

  val inject : t -> int -> (S.t * Index.t) -> (t * t) option -> t

  val set_subnode : t -> int -> t -> t

  val empty : t

  val is_empty : t -> bool

  val build : bool (* root ? *) -> S.t -> (SS.t * Index.t) array -> t array option -> t

end = struct
  type t =
    { root : bool
    ; prefix : S.t
    ; ents : (SS.t * Index.t) array
    ; subnodes : t array (* leaves+1 *) option
    }

  let is_leaf rn = rn.subnodes = None

  let empty =
    { root = true
    ; prefix = S.empty
    ; ents = [||]
    ; subnodes = None
    }

  let is_empty rn = rn = empty

  (* - size is not checked
     - balancing is not checked
  *)
  let _validate rn =
    let nents = A.length rn.ents in
    if is_empty rn then () else assert (nents > 0);
    (* XXX Uniqueness of prefix *)
    if S.is_empty rn.prefix then
      A.iter (fun (seg, _) -> assert (not @@ SS.is_empty seg)) rn.ents;
    for i = 0 to nents - 2 do
      let seg , _ = get rn.ents i in
      let seg', _ = get rn.ents (i+1) in
      assert (SS.compare seg seg' = -1)
    done;
    begin match rn.subnodes with
    | None -> assert (A.length rn.ents > 0)
    | Some subnodes ->
        let nsubnodes = A.length subnodes in
        assert (nents + 1 = nsubnodes)
    end;
    rn

  let validate rn = rn

  let recompute_prefix rn =
    let nents = A.length rn.ents in
    assert (nents > 0);
    let segl, _ = get rn.ents 0 in
    let segr, _ = get rn.ents (nents - 1) in
    let common, segl_postfix, segr_postfix = SS.common_prefix segl segr in
    if SS.is_empty common then rn
    else
      { root= rn.root
      ; prefix= S.normalize @@ S.append rn.prefix (SS.to_segment common)
      ; ents= A.mapi (fun i (seg, v) ->
            let seg' =
              if i = 0 then segl_postfix
              else if i = nents - 1 then segr_postfix
              else
                let _, common_postfix, seg_postfix = SS.common_prefix common seg in
                assert SS.(equal (append common seg_postfix) seg);
                assert (SS.is_empty common_postfix);
                seg_postfix
            in
            (SS.normalize seg', v)) rn.ents
      ; subnodes= rn.subnodes }

  let copy rn =
    { rn with ents = A.copy rn.ents
    ; subnodes=
        match rn.subnodes with
        | None -> None
        | Some subnodes -> Some (A.copy subnodes) }

  let update rn i v =
    assert (0 <= i && i < A.length rn.ents);
    let rn = copy rn in
    let seg, _ = get rn.ents i in
    A.unsafe_set rn.ents i (seg, v);
    (* invariants are kept *)
    rn

  (* mutation. *)
  let set_subnode rn j subnode =
    match rn.subnodes with
    | None -> assert false
    | Some subnodes ->
        let subnodes = A.copy subnodes in
        A.set subnodes j subnode;
        (* invariants are kept *)
        { rn with subnodes = Some subnodes }

  let inject rn j (kseg, v) lro =
    let nents = A.length rn.ents in
    match lro, rn.subnodes with
    | None, None ->
        let prefix = S.empty (* XXX *) in
        let ents =
          A.(init (nents + 1) (fun i ->
              if i < j then
                let (seg, v) = get rn.ents i in
                SS.of_segment (Segment.append rn.prefix (SS.to_segment seg)), v
              else if i = j then (SS.of_segment kseg, v)
              else
                let (seg, v) = get rn.ents (i-1) in
                SS.of_segment (Segment.append rn.prefix (SS.to_segment seg)), v))
        in
        validate @@ recompute_prefix { root= rn.root; prefix; ents; subnodes= None }
    | Some (left, right), Some subnodes ->
        let prefix = S.empty in (* XXX *)
        let ents = A.init (nents + 1) (fun i ->
            if i < j then
              let (seg, v) = get rn.ents i in
              SS.of_segment (Segment.append rn.prefix (SS.to_segment seg)), v
            else if i = j then (SS.of_segment kseg, v)
            else
              let (seg, v) = get rn.ents (i-1) in
              SS.of_segment (Segment.append rn.prefix (SS.to_segment seg)), v)
        in
        let nsubnodes = A.length subnodes in
        let subnodes =
          A.init (nsubnodes+1) (fun i ->
              if i < j then get subnodes i
              else if i = j then left
              else if i = j+1 then right
              else get subnodes (i-1))
        in
        validate @@ recompute_prefix { root= rn.root; prefix; ents; subnodes= Some subnodes }
    | _ -> assert false

  let build root prefix ents subnodes =
    validate @@ recompute_prefix { root; prefix; ents; subnodes }
end

open Raw_node

let segment_size_in_bytes seg = (S.length seg + 7) / 8 + 1

let size rn =
  segment_size_in_bytes rn.prefix
  + 2 (* #ents *)
  + A.fold_left (fun sum (seg, _i) ->
      sum + segment_size_in_bytes (SS.to_segment seg) + 4 (* index *)) 0 rn.ents
  + match rn.subnodes with
    | None -> 4
    | Some subnodes -> A.length subnodes * 4

let rec depth rn =
  match rn.subnodes with
  | None -> 1
  | Some subnodes -> depth (get subnodes 0) + 1

let is_too_big rn =
  let sz = size rn in
  let b = sz > max_size_in_bytes in
  if b then
    Format.eprintf "too big: %d prefix=%d segavg=%.2f sn=%d %d@."
      (A.length rn.ents)
      (segment_size_in_bytes rn.prefix)
      (float
         (A.fold_left (fun sum (seg, _) ->
              segment_size_in_bytes (SS.to_segment seg) + sum) 0 rn.ents)
       /. float (A.length rn.ents))
      (match rn.subnodes with None -> 0 | Some xs -> A.length xs)
      sz;
  b

let bsearch kseg rn =
  let nleaves = A.length rn.ents in
  let get = get rn.ents in
  let check j =
    let seg, i = get j in
    let seg = S.append rn.prefix (SS.to_segment seg) in
    let comp = S.compare kseg seg in
    i, comp
  in
  let rec bsearch (l,r) =
    let j = (l + r) / 2 in
    match check j with
    | i, 0 -> `Found (j, i)
    | _, -1 ->
        if j = l then `Not_found l
        else bsearch (l,j-1)
    | _, 1 ->
        if j = r then `Not_found (l+1)
        else bsearch (j+1,r)
    | _ -> assert false
  in
  bsearch (0, nleaves-1)

let rec find kseg rn =
  if is_empty rn then None
  else
    match bsearch kseg rn with
    | `Found (_, v) -> Some v
    | `Not_found j ->
        match rn.subnodes with
        | None -> None
        | Some subnodes ->
            find kseg (get subnodes j)

let array_split at xs =
  let left = A.sub xs 0 at in
  let mid = get xs at in
  let right = A.sub xs (at+1) (A.length xs - at - 1) in
  assert (left ++ [|mid|] ++ right = xs);
  left, mid, right

let split rn =
  let nents = A.length rn.ents in
  let mid = nents / 2 in
  assert (mid > 0);
  let lents, (midseg, midt), rents = array_split mid rn.ents in
  let mident = S.append rn.prefix (SS.to_segment midseg), midt in
  assert (A.length lents = mid);
  let left =
    let subnodes = match rn.subnodes with
      | None -> None
      | Some subnodes -> Some (A.sub subnodes 0 (mid+1))
    in
    build false rn.prefix lents subnodes
  in
  let right =
    let subnodes = match rn.subnodes with
      | None -> None
      | Some subnodes -> Some (A.sub subnodes (mid+1) (A.length subnodes - mid - 1))
    in
    build false rn.prefix rents subnodes
  in
  left, mident, right

let rec add kseg v rn =
  if is_empty rn then
    `Updated (build true S.empty [| (SS.of_segment kseg, v) |] None)
  else
    match bsearch kseg rn with
    | `Found (_, v') when v = v' -> `NoChange
    | `Found (i, _) ->
        (* overwrite *)
        let rn = update rn i v in
        `Updated rn
    | `Not_found j ->
        match rn.subnodes with
        | None ->
            let rn = inject rn j (kseg, v) None in
            if is_too_big rn then
              let left, mid, right = split rn in
              `Split (left, mid, right)
            else `Updated rn
        | Some subnodes ->
            match add kseg v (get subnodes j) with
            | `NoChange -> `NoChange
            | `Updated subnode ->
                (* XXX may exceeds the size *)
                `Updated (set_subnode rn j subnode)
            | `Split (left, mid, right) ->
                let rn = inject rn j mid (Some (left, right)) in
                if is_too_big rn then
                  let left, mid, right = split rn in
                  `Split (left, mid, right)
                else `Updated rn

let add kseg v rn =
  match add kseg v rn with
  | `Updated rn -> rn
  | `NoChange -> rn
  | `Split (left, mid, right) ->
      prerr_endline "root split";
      let mid =
        let midseg, midt = mid in
        SS.of_segment midseg, midt
      in
      build true Segment.empty [| mid |] (Some  [| left; right |])

let bench rn =
  let rec traverse f rn =
    f rn;
    match rn.subnodes with
    | None -> ()
    | Some subnodes -> A.iter (traverse f) subnodes
  in
  let n = ref 0 in
  let pleaves = ref 0 in
  let occupied = ref 0 in
  traverse (fun rn ->
      incr n;
      pleaves := !pleaves + A.length rn.ents;
      occupied := !occupied + size rn
    ) rn;
  Format.eprintf "depth: %d nodes: %d bytes: %d pleaves/node: %.2f fill: %.2f@."
    (depth rn)
    !n (!n * 4096) (float !pleaves /. float !n)
    (float !occupied /. (float (!n * 4096)))

let test () =
  let rng = Random.State.make_self_init () in
  let added = Hashtbl.create 1000 in
  let add rn =
    let kseg = Gen.(segment (int_range (1,200))) rng in
    let v = Gen.index rng in
    Hashtbl.replace added kseg v;
    add kseg v rn
  in
  let rec f rn = function
    | 0 -> rn
    | i -> f (add rn) (i-1)
  in
  let rn = f empty 100000 in
  Hashtbl.iter (fun kseg v ->
      match find kseg rn with
      | Some v' when v = v' -> ()
      | Some _ -> assert false
      | _ -> assert false) added;
  bench rn

(*
let () = test ()
*)

let test2 () =
  let path = Sys.argv.(1) in
  let vc = Vc.open_ ~mode:Storage.Reader path in
  let roots = Vc.roots vc in (* XXX silly to load all *)

  let last_root =
    match Commit.read_the_latest roots with
    | Some root -> root
    | None -> assert false
  in

  Format.eprintf "last root index= %a@." Index.pp last_root.Commit.index;

  let Cursor.Cursor (_, n, ctxt,_) = from_Some @@ Vc.checkout vc last_root.Commit.hash in

  let n = match Node.view ctxt n with
    | Bud (Some n, _, _) -> n
    | _ -> assert false
  in

  let leaves = Hashtbl.create 1001 in
  let rec f rev_sides n =
    match Node.view ctxt n with
    | Bud _ -> assert false
    | Leaf (_, Indexed i, _) ->
        Hashtbl.replace leaves (S.normalize @@ S.of_sides @@ List.rev rev_sides) i
    | Leaf _ -> assert false
    | Internal (nl, nr, _, _) ->
        f (S.Left::rev_sides) nl;
        f (S.Right::rev_sides) nr
    | Extender (seg, n, _, _) ->
        f (List.rev_append (S.to_sides seg) rev_sides) n
  in
  f [] n;
  Format.eprintf "%d pleaves@." @@ Hashtbl.length leaves;

  let cntr = ref 0 in
  let lasttime = ref (Unix.gettimeofday ()) in

  let rn =
    Hashtbl.fold (fun seg i rn ->
        let rn = add seg i rn in
        incr cntr;
        let now = Unix.gettimeofday () in
        if !cntr mod 10000 = 0 || now -. !lasttime > 60.0 then begin
          Format.eprintf "Added %d leaves@." !cntr;
          lasttime := now
        end;
        rn) leaves empty
  in
  prerr_endline "done";

  let cntr = ref 0 in
  let lasttime = ref (Unix.gettimeofday ()) in
  Hashtbl.iter (fun seg i ->
      assert (find seg rn = Some i);
      incr cntr;
      let now = Unix.gettimeofday () in
      if !cntr mod 10000 = 0 || now -. !lasttime > 60.0 then begin
        Format.eprintf "Checked %d leaves@." !cntr;
        lasttime := now
      end) leaves;
  prerr_endline "done";

  (* must check *)
  bench rn

let () = test2 ()
