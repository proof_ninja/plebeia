# 1.1.0

## Module renaming

`Hash_prefix`
:    `Hash` is renamed as `Hash_prefix`, since `Hash.t` was not the hash of the nodes but the 28 byte prefix.

## New modules

`Snapshot`
:    Making a snapshot

`Data_encoding_tools`
:    Pretty printer using encoding.  Reader.

## Deprecation

Old modules `Search`, `Ediff` and `Traverse` are removed.
